appCtrl.controller('registerCtrl', function($scope, $ionicPopup, $ionicActionSheet, $ionicLoading, TimeService, RequestService, ionicDatePicker) {
    //if(typeof analytics !== undefined) { analytics.trackView("Register"); }

    $scope.init = function() {
        $scope.profile = [];
        $scope.user = [];
        $scope.login = [];

        $scope.userCheck();

        var dpObj = {
            callback: function (val) {  //Mandatory
                var date = new Date(val);
                $scope.profile.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
            },
            from: new Date(1920, 01, 01), //Optional
            inputDate: new Date(2017, 10, 30),      //Optional
            mondayFirst: true,          //Optional
            closeOnSelect: false,       //Optional
            templateType: 'popup'       //Optional
        };

        $scope.openDatePicker = function() {
            ionicDatePicker.openDatePicker(dpObj);
        };
    }

    $scope.userCheck = function() {
        // var deviceInfo = ionic.Platform.device();

        // // data = { uuid: deviceInfo.uuid };
        // data = { uuid: '123123' };

        // RequestService.getby(data, 'userdevice', 'uuid').then(function(response) {
        //     $('#profile-loading-progress').hide();

        //     if (response.length > 0) {
        //         $scope.user = response[0];

        //         $('#profile-info').show();
        //     } else {
        //         $('#profile-register').show();
        //     }
        // });

        if (localStorage.user) {
            $scope.user = JSON.parse(localStorage.user);

            $('#profile-info').show();
        } else {
            $scope.user = [];

            $('#profile-info').hide();

            $('#profile-login').show();
        }
    }

    $scope.submitProfile = function() {
        if ($scope.profile.username && $scope.profile.password && $scope.profile.repassword && $scope.profile.name && $scope.profile.phone && $scope.profile.email && $scope.profile.address && $scope.profile.birthday) {
            if ($scope.profile.password != $scope.profile.repassword) {
                $scope.showAlert('warning2');
            } else {
                if ($scope.profile.username.match(/^[a-zA-Z0-9.\-_]+$/)) {
                    $ionicLoading.show({
                        template: 'Registering..'
                    });

                    var deviceInfo = ionic.Platform.device();

                    var data = {
                        username: $scope.profile.username,
                        pass: $scope.profile.password,
                        name: $scope.profile.name,
                        phone: $scope.profile.phone,
                        email: $scope.profile.email,
                        city: $scope.profile.city,
                        address: $scope.profile.address,
                        uuid: deviceInfo.uuid,
                        device: deviceInfo.model
                        //uuid: '5b030556347f72ed',
                        //device: 'SM-A520F'
                    }

                    RequestService.save(data, 'userdevice').then(function(response) {
                        $ionicLoading.hide();

                        $scope.showAlert('success', 'Terima Kasih telah melakukan registrasi', $scope.profile);
                    });
                } else {
                    $scope.showAlert('warning4');
                }
            }
        } else {
            $scope.showAlert('warning');
        }
    }

    $scope.showAlert = function(type, msg, user) {
        if (type == 'warning') {
            title = 'Pesan';
            template = 'Harap lengkapi data.';
        } else if (type == 'warning2') {
            title = 'Pesan';
            template = 'Password tidak sama.';
        } else if (type == 'warning3') {
            title = 'Pesan';
            template = msg;
        } else if (type == 'warning4') {
            title = 'Pesan';
            template = 'Username tidak boleh mengandung spasi dan karakter simbol';
        } else {
            title = 'Terima Kasih';
            template = msg;
        }

        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template
        });

        if (type == 'success') {
            alertPopup.then(function(res) {
                $scope.profile = [];

                $('#profile-loading-progress').show();

                $('#profile-register').hide();

                var data = {
                    username: user.username,
                    pass: user.password
                };

                RequestService.login(data).then(function(response) {
                    $scope.user = response;

                    $('#profile-loading-progress').hide();

                    $('#profile-info').show();

                    localStorage.user = JSON.stringify(response);
                });
            });
        } else if (type == 'success2') {
            alertPopup.then(function(res) {
                $scope.login = [];

                $('#profile-loading-progress').show();

                $('#profile-login').hide();

                $scope.user = user;

                $('#profile-loading-progress').hide();

                $('#profile-info').show();

                localStorage.user = JSON.stringify(user);
            });
        }
    }

    $scope.showLogin = function() {
        $('#profile-register').hide();
        $('#profile-login').show();
    }

    $scope.showRegister = function() {
        $('#profile-register').show();
        $('#profile-login').hide();
    }

    $scope.doLogin = function() {
        if ($scope.login.username && $scope.login.password) {
            var data = {
                username: $scope.login.username,
                pass: $scope.login.password
            }

            $ionicLoading.show({
                template: 'Logging in..'
            });
            
            RequestService.login(data).then(function(response) {
                $ionicLoading.hide();

                if (response.status == 'error') {
                    $scope.showAlert('warning3', response.message);
                } else {
                    $scope.showAlert('success2', 'Login berhasil.', response);
                }
            });
        } else {
            $scope.showAlert('warning');
        }
    }

    $scope.showLogoutMenu = function() {
        var hideSheet = $ionicActionSheet.show({
            destructiveText: 'Logout',
            titleText: 'Anda yakin ingin logout?',
            cancelText: 'Cancel',
            cancel: function() {},
            buttonClicked: function(index) {
                return true;
            },
            destructiveButtonClicked: function(){
                $ionicLoading.show({
                    template: 'Logging out..'
                });
                
                // // Google logout
                // window.plugins.googleplus.logout(
                //     function (msg) {
                //         console.log(msg);
                //         $ionicLoading.hide();
                //         $state.go('login');
                //     },
                //     function(fail){
                //         console.log(fail);
                //     }
                // );

                localStorage.user = '';

                $scope.userCheck();

                $ionicLoading.hide();

                return true;
            }
        });
    };

    $scope.init();
});