// appCtrl.controller('propertyCtrl', function($scope, $state, RequestService, TimeService) {
// 	$scope.init = function() {
// 		$scope.properties = [];

//         $scope.getData();
// 	}

// 	$scope.getData = function() {
//         RequestService.getall('property').then(function(properties) {
//             for (var ii = 0; ii < properties.length; ii++) {
//                 var auction_date = new Date(properties[ii].auction_date);
//                 properties[ii].auction_date_ = auction_date.getDate() + ' ' + TimeService.months[auction_date.getMonth()] + ' ' + auction_date.getFullYear();

//                 properties[ii].price_billion = properties[ii].price / 1000000000;
                
//                 $scope.properties.push(properties[ii]);
//             }

//             $('#property-loading-progress').hide();
//             $('#property-content').show();
//         });
//     }

//     $scope.doRefresh = function() {
//         $scope.properties = [];
        
//         RequestService.getall('property').then(function(properties) {
//             for (var ii = 0; ii < properties.length; ii++) {
//                 var auction_date = new Date(properties[ii].auction_date);
//                 properties[ii].auction_date_ = auction_date.getDate() + ' ' + TimeService.months[auction_date.getMonth()] + ' ' + auction_date.getFullYear();

//                 properties[ii].price_billion = properties[ii].price / 1000000000;
                
//                 $scope.properties.push(properties[ii]);
//             }
//         })

//         .finally(function() {
//             $scope.$broadcast('scroll.refreshComplete');
//         });
//     }

//     $scope.navigateTo = function (targetPage, objectData) {
//         $state.go(targetPage, {
//             property: objectData
//         });
//     };

// 	$scope.init();
// });

appCtrl.controller('propertyCtrl', function($scope, $state, TimeService, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Property"); }

    $scope.baseUrl = RequestService.urlImageLink;
    $scope.init = function() {
        $scope.search = [];
        $scope.cities = [{value: '', label: 'Semua'}];
        $scope.results = [];
        $scope.info = '';

        RequestService.getallnopaging('city').then(function(cities) {
            for (var ii = 0; ii < cities.length; ii++) {
                $scope.cities.push({value: cities[ii].name, label: cities[ii].name});
            }
        });
    }

    $scope.searchProperty = function() {
        $scope.results = [];
        
        $('#property-loading-progress').show();

        var data = {
            'location': $scope.search.location,
            'min_price': $scope.search.min_price,
            'max_price': $scope.search.max_price,
            'min_surface_area': $scope.search.min_surface_area,
            'max_surface_area': $scope.search.max_surface_area,
            'min_building_area': $scope.search.min_building_area,
            'min_building_area': $scope.search.min_building_area,
            'rate': $scope.search.rate
        };

        // RequestService.search(data, 'property').then(function(results) {
        //     if (results.length > 0) {
        //         for (var ii = 0; ii < results.length; ii++) {
        //             if (results[ii].auction_date) {
        //                 var auction_date = new Date(results[ii].auction_date);
        //                 results[ii].auction_date_ = auction_date.getDate() + ' ' + TimeService.months[auction_date.getMonth()] + ' ' + auction_date.getFullYear();
        //             } else {
        //                 results[ii].auction_date_ = '';
        //             }

        //             if (results[ii].price >= 1000000000) {
        //                 results[ii].price_billion = results[ii].price / 1000000000;
        //             } else {
        //                 results[ii].price_million = results[ii].price / 1000000;
        //             }
                    
        //             $scope.results.push(results[ii]);
        //         }
                
        //         $scope.info = results.length + ' data ditemukan';
        //     } else {
        //         $scope.info = 'Data belum tersedia';
        //     }

        //     $('#property-loading-progress').hide();
        //     $('#property-result').show();
        // });

        RequestService.getcount(data, 'property').then(function(result) {
            if (result > 0) {
                $state.go('app.property-results', { search: data, total: result });
            } else {
                window.plugins.toast.showShortCenter("Data Belum Tersedia");
            }

            $('#property-loading-progress').hide();
            $('#property-result').show();
        });
    }

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            property: objectData
        });
    };

    $scope.init();
});

appCtrl.controller('propertyResultsCtrl', function($scope, $state, $stateParams, TimeService, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Property Result"); }

    $scope.baseUrl = RequestService.urlImageLink;
    $scope.init = function() {
        // window.plugins.toast.showShortCenter($stateParams.total + " Data Ditemukan");

        $scope.properties = [];

        var search = $stateParams.search;

        $scope.noMoreProperty = false;

        search.page = 1;
        search.limit = 2;

        $scope.loadMoreProperties = function() {
            RequestService.search(search, 'property').then(function(properties) {
                for (var ii = 0; ii < properties.length; ii++) {
                    if (properties[ii].auction_date) {
                        var auction_date = new Date(properties[ii].auction_date);
                        properties[ii].auction_date_ = auction_date.getDate() + ' ' + TimeService.months[auction_date.getMonth()] + ' ' + auction_date.getFullYear();
                    } else {
                        properties[ii].auction_date_ = '';
                    }

                    if (properties[ii].price >= 1000000000) {
                        properties[ii].price_billion = properties[ii].price / 1000000000;
                    } else {
                        properties[ii].price_million = properties[ii].price / 1000000;
                    }
                    
                    $scope.properties.push(properties[ii]);
                }

                $scope.$broadcast('scroll.infiniteScrollComplete');

                search.page += 1;

                if (properties.length != search.limit) {
                    $scope.noMoreProperty = true;
                }
            });
        };
    };

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            property: objectData
        });
    };

    $scope.init();
});

appCtrl.controller('propertyDetailCtrl', function($scope, $state, $stateParams, $ionicSlideBoxDelegate, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Property Detail"); }

    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {
		$scope.property = $stateParams.property;

        var data = { 'id': $stateParams.property.id };

        RequestService.getby(data, 'property', 'id').then(function(property) {
            $scope.property = property;

            $scope.property.id = $stateParams.property.id;

            $ionicSlideBoxDelegate.update();

            $('#property-detail-loading-progress').hide();

            $('#property-detail').show();
        });
	}

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            property: objectData
        });
    };

	$scope.init();
});

appCtrl.controller('propertyRegisterCtrl', function($scope, $stateParams, $ionicPopup, $ionicHistory, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Property Register"); }

    $scope.baseUrl = RequestService.urlImageLink;
    $scope.init = function() {
        $scope.property = $stateParams.property;

        $scope.register = [];
        $scope.branchs = [];

        $scope.genders = [{
            value: 'Laki-laki',
            label: 'Laki-laki'
        }, {
            value: 'Perempuan',
            label: 'Perempuan'
        }];

        RequestService.getallnopaging('branch').then(function(branchs) {
            for (var ii = 0; ii < branchs.length; ii++) {
                $scope.branchs.push({ 'value' : branchs[ii].id, 'label': branchs[ii].name });
            }

            $('#property-register-loading-progress').hide();
            $('#register-form').show();
        });
    };

    $scope.submitRegister = function() {
        if ($scope.register.name && $scope.register.address && $scope.register.gender && $scope.register.phone && $scope.register.nearest_branch) {
            var date = new Date();

            var data = {
                date: date,
                type_id: $scope.property.id,
                name: $scope.register.name,
                address: $scope.register.address,
                gender: $scope.register.gender,
                phone: $scope.register.phone,
                nearest_branch_id: $scope.register.nearest_branch
            }

            RequestService.save(data, 'propertyregister').then(function(response) {
                $scope.showAlert('success');
            });
        } else {
            $scope.showAlert('warning');
        }
    };

    $scope.showAlert = function(type, msg) {
        if (type == 'warning') {
            title = 'Pesan';
            template = 'Harap lengkapi data.';
        } else if (type == 'success') {
            title = 'Terima Kasih';
            template = 'Pembelian Anda akan segera kami proses.';
        }

        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template
        });

        if (type == 'success') {
            alertPopup.then(function(res) {
                $scope.filing = [];

                $ionicHistory.goBack(-2);
            });
        }
    }

    $scope.init();
});

appCtrl.controller('propertyFilingCtrl', function($scope, $stateParams, $ionicPopup, $ionicHistory, $ionicLoading, ionicDatePicker, TimeService, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Property Filing"); }

    $scope.baseUrl = RequestService.urlImageLink;
    $scope.init = function() {
        $scope.property = $stateParams.property;

        $scope.filing = [];
        $scope.branchs = [];
        $scope.profile = [];
        $scope.login = [];

        $scope.userCheck();
    }

    $scope.openDatePicker = function(){
        var dpObj = {
            callback: function (val) {
                var date = new Date(val);
                $scope.filing.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
            },
            from: new Date(1920, 01, 01),
            to: new Date(2017, 10, 30),
            inputDate: new Date(),
            mondayFirst: true,
            closeOnSelect: false,
            templateType: 'popup'
        };

        ionicDatePicker.openDatePicker(dpObj);
    };

    $scope.submitFiling = function() {
        if ($scope.filing.facility && $scope.filing.name && $scope.filing.birthday && $scope.filing.gender && $scope.filing.city && $scope.filing.address && $scope.filing.kelurahan && $scope.filing.kecamatan && $scope.filing.rt && $scope.filing.rw && $scope.filing.postal_code && $scope.filing.phone && $scope.filing.email && $scope.filing.nearest_branch && $scope.filing.job && $scope.filing.company_type && $scope.filing.job_position && $scope.filing.business_field && $scope.filing.year_start_work && $scope.filing.service_length && $scope.filing.office_name && $scope.filing.office_phone && $scope.filing.income && $scope.filing.contact_to) {
            if ($scope.filing.term1 && $scope.filing.term2) {
                var data = {
                    address: $scope.filing.address,
                    birthday: $scope.filing.birthday,
                    business_field: $scope.filing.business_field,
                    city: $scope.filing.city,
                    company_type: $scope.filing.company_type,
                    contact_to: $scope.filing.contact_to,
                    email: $scope.filing.email,
                    facility: $scope.filing.facility,
                    gender: $scope.filing.gender,
                    income: $scope.filing.income,
                    job: $scope.filing.job,
                    job_position: $scope.filing.job_position,
                    kecamatan: $scope.filing.kecamatan,
                    kelurahan: $scope.filing.kelurahan,
                    name: $scope.filing.name,
                    nearest_branch_id: $scope.filing.nearest_branch,
                    office_name: $scope.filing.office_name,
                    office_phone: $scope.filing.office_phone,
                    phone: $scope.filing.phone,
                    postal_code: $scope.filing.postal_code,
                    rt: $scope.filing.rt,
                    rw: $scope.filing.rw,
                    service_length: $scope.filing.service_length,
                    type_id: $scope.property.id,
                    year_start_work: $scope.filing.year_start_work
                }

                RequestService.save(data, 'propertyfiling').then(function(response) {
                    $scope.showAlert('success');
                });
            } else {
                $scope.showAlert('warning2');
            }
        } else {
            $scope.showAlert('warning');
        }
    }

    $scope.showAlert = function(type, msg, user) {
        if (type == 'warning') {
            title = 'Pesan';
            template = 'Harap lengkapi data.';
        } else if (type == 'warning2') {
            title = 'Pesan';
            template = 'Harap menyetujui Syarat dan Ketentuan.';
        } else if (type == 'warning3') {
            title = 'Pesan';
            template = 'Password tidak sama';
        } else if (type == 'warning4') {
            title = 'Pesan';
            template = msg;
        } else if (type == 'warning5') {
            title = 'Pesan';
            template = 'Username tidak boleh mengandung spasi dan karakter simbol';
        } else if (type == 'success') {
            title = 'Terima Kasih';
            template = 'Pengajuan Anda akan segera kami proses.';
        } else {
            title = 'Terima Kasih';
            template = msg;
        }

        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template
        });

        if (type == 'success') {
            alertPopup.then(function(res) {
                $scope.filing = [];

                $ionicHistory.goBack(-2);
            });
        } else if (type == 'success2') {
            $scope.profile = [];

            $('#property-profile-loading-progress').show();

            $('#filing-register').hide();

            var data = {
                username: user.username,
                pass: user.password
            };

            RequestService.login(data).then(function(response) {
                $scope.user = response;

                $('#property-profile-loading-progress').hide();

                $scope.showForm();

                localStorage.user = JSON.stringify(response);
            });
        } else if (type == 'success3') {
            alertPopup.then(function(res) {
                $scope.login = [];

                $('#property-profile-loading-progress').show();

                $('#filing-login').hide();

                $scope.user = user;

                $('#property-loading-progress').hide();

                $scope.showForm();

                localStorage.user = JSON.stringify(user);
            });
        }
    }

    $scope.userCheck = function() {
        $('#property-profile-loading-progress').show();

        if (localStorage.user) {
            $('#property-profile-loading-progress').hide();
            
            $scope.showForm();
        } else {
            $('#property-profile-loading-progress').hide();
            var dpObj = {
                callback: function (val) {  //Mandatory
                    var date = new Date(val);
                    $scope.profile.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
                },
                from: new Date(1920, 01, 01), //Optional
                inputDate: new Date(2017, 10, 30),      //Optional
                mondayFirst: true,          //Optional
                closeOnSelect: false,       //Optional
                templateType: 'popup'       //Optional
            };

            $scope.openDatePickerRegister = function() {
                ionicDatePicker.openDatePicker(dpObj);
            };

            $('#filing-form').hide();
            $('#filing-register').show();
        }
    }

    $scope.submitProfile = function() {
        if ($scope.profile.username && $scope.profile.password && $scope.profile.repassword && $scope.profile.name && $scope.profile.phone && $scope.profile.email && $scope.profile.address && $scope.profile.birthday) {
            if ($scope.profile.password != $scope.profile.repassword) {
                $scope.showAlert('warning3');
            } else {
                if ($scope.profile.username.match(/^[a-zA-Z0-9.\-_]+$/)) {
                    $ionicLoading.show({
                        template: 'Registering..'
                    });

                    var deviceInfo = ionic.Platform.device();

                    var data = {
                        username: $scope.profile.username,
                        pass: $scope.profile.password,
                        name: $scope.profile.name,
                        phone: $scope.profile.phone,
                        email: $scope.profile.email,
                        city: $scope.profile.city,
                        address: $scope.profile.address,
                        uuid: deviceInfo.uuid,
                        device: deviceInfo.model
                        //uuid: '5b030556347f72ed',
                        //device: 'SM-A520F'
                    }

                    RequestService.save(data, 'userdevice').then(function(response) {
                        $ionicLoading.hide();

                        $scope.showAlert('success2', 'Terima Kasih telah melakukan registrasi', $scope.profile);
                    });
                } else {
                    $scope.showAlert('warning5');
                }  
            }
        } else {
            $scope.showAlert('warning');
        }
    }

    $scope.doLogin = function() {
        if ($scope.login.username && $scope.login.password) {
            var data = {
                username: $scope.login.username,
                pass: $scope.login.password
            }

            $ionicLoading.show({
                template: 'Logging in..'
            });
            
            RequestService.login(data).then(function(response) {
                $ionicLoading.hide();

                if (response.status == 'error') {
                    $scope.showAlert('warning4', response.message);
                } else {
                    $scope.showAlert('success3', 'Login berhasil.', response);
                }
            });
        } else {
            $scope.showAlert('warning');
        }
    }

    $scope.showForm = function() {
        RequestService.getallnopaging('branch').then(function(branchs) {
            for (var ii = 0; ii < branchs.length; ii++) {
                $scope.branchs.push({ 'value' : branchs[ii].id, 'label': branchs[ii].name });
            }
        });

        $scope.filing.facility = 'Pembelian Property';

        // $scope.facilities = [{
        //     value: 'Pembelian',
        //     label: 'Pembelian'
        // }, {
        //     value: 'Pembangunan',
        //     label: 'Pembangunan'
        // }, {
        //     value: 'Renovasi',
        //     label: 'Renovasi'
        // }, {
        //     value: 'Take Over',
        //     label: 'Take Over'
        // }, {
        //     value: 'Pembelian Kavling',
        //     label: 'Pembelian Kavling'
        // }];

        $scope.genders = [{
            value: 'Laki-laki',
            label: 'Laki-laki'
        }, {
            value: 'Perempuan',
            label: 'Perempuan'
        }];

        $scope.jobs = [{
            value: 'Karyawan',
            label: 'Karyawan'
        }, {
            value: 'Wiraswasta',
            label: 'Wiraswasta'
        }, {
            value: 'Professional',
            label: 'Professional'
        }];

        $scope.company_types = [{
            value: 'Perusahaan Swasta Nasional',
            label: 'Perusahaan Swasta Nasional'
        }, {
            value: 'Perusahaan Swasta Asing',
            label: 'Perusahaan Swasta Asing'
        }, {
            value: 'Pemerintahan',
            label: 'Pemerintahan'
        }, {
            value: 'BUMN',
            label: 'BUMN'
        }, {
            value: 'BUMD',
            label: 'BUMD'
        }, {
            value: 'TNI/Polri',
            label: 'TNI/Polri'
        }, {
            value: 'Perusahaan Perorangan',
            label: 'Perusahaan Perorangan'
        }];

        $scope.job_positions = [{
            value: 'Di Bawah Supervisor',
            label: 'Di Bawah Supervisor'
        }, {
            value: 'Supervisor Ke Atas',
            label: 'Supervisor Ke Atas'
        }, {
            value: 'Dosen/Guru',
            label: 'Dosen/Guru'
        }, {
            value: 'Wiraswasta',
            label: 'Wiraswasta'
        }, {
            value: 'Profesional',
            label: 'Profesional'
        }, {
            value: 'Pensiunan',
            label: 'Pensiunan'
        }, {
            value: 'Tidak Bekerja',
            label: 'Tidak Bekerja'
        }];

        $scope.business_fields = [{
            value: 'Konstruksi',
            label: 'Konstruksi'
        }, {
            value: 'Industri Pengolahan',
            label: 'Industri Pengolahan'
        }, {
            value: 'Jasa-jasa Dunia Usaha',
            label: 'Jasa-jasa Dunia Usaha'
        }, {
            value: 'Jasa-jasa Sosial Masyarakat',
            label: 'Jasa-jasa Sosial Masyarakat'
        }, {
            value: 'Listrik, Gas dan Air',
            label: 'Listrik, Gas dan Air'
        }, {
            value: 'Pengangkutan, Pergudangan dan Komunikasi',
            label: 'Pengangkutan, Pergudangan dan Komunikasi'
        }, {
            value: 'Perdagangan, Restoran dan Hotel',
            label: 'Perdagangan, Restoran dan Hotel'
        }, {
            value: 'Pertambangan',
            label: 'Pertambangan'
        }, {
            value: 'Pertanian, Perburuan dan Sarana Pertanian',
            label: 'Pertanian, Perburuan dan Sarana Pertanian'
        }];

        $scope.contact_options = [{
            value: 'No. Handphone',
            label: 'No. Handphone'
        }, {
            value: 'Telp. Kantor',
            label: 'Telp. Kantor'
        }];

        $('#filing-form').show();
    }

    $scope.doRefresh = function() {
        $scope.userCheck();

        $scope.$broadcast('scroll.refreshComplete');
    }

    $scope.showLogin = function() {
        $('#filing-register').hide();
        $('#filing-login').show();
    }

    $scope.showRegister = function() {
        $('#filing-register').show();
        $('#filing-login').hide();
    }

    $scope.init();
});