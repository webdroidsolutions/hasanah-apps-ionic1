// appCtrl.controller('productCtrl', function($scope, $state, RequestService) {
// 	$scope.init = function() {
// 		$scope.products = [];

//         $scope.getData();
// 	}

// 	$scope.getData = function() {
//         RequestService.getall('product').then(function(products) {
//             for (var ii = 0; ii < products.length; ii++) {
//                 $scope.products.push(products[ii]);
//             }

//             $('#product-loading-progress').hide();
//             $('#product-content').show();
//         });
//     }

//     $scope.navigateTo = function (targetPage, objectData) {
//         $state.go(targetPage, {
//             product: objectData
//         });
//     };

// 	$scope.init();
// });

appCtrl.controller('productCtrl', function($scope, $state) {
    //if(typeof analytics !== undefined) { analytics.trackView("Product"); }

    $scope.init = function() {
        $scope.productCategories = [{"name": "Produk Dana"}, {"name": "Produk Pembiayaan"}]
    };

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            category: objectData
        });
    };

    $scope.init();
});

appCtrl.controller('productListCtrl', function($scope, $state, $stateParams, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Product List"); }
    $scope.baseUrl = RequestService.urlImageLink;
    $scope.init = function() {
        $scope.category = $stateParams.category;

        $scope.products = [];

        $scope.getData();
    }

    $scope.getData = function() {
        console.log($scope.category);
        var data = {'category': $scope.category.name};

        RequestService.getby(data, 'product', 'category').then(function(products) {
            console.log(products);
            for (var ii = 0; ii < products.length; ii++) {
                $scope.products.push(products[ii]);
            }

            $('#product-loading-progress').hide();
            $('#product-content').show();
        });
    }

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            product: objectData
        });
    };

    $scope.init();
});

appCtrl.controller('productDetailCtrl', function($scope, $stateParams, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Product Detail"); }
    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {
		$scope.product = $stateParams.product;
	}

	$scope.init();
});
