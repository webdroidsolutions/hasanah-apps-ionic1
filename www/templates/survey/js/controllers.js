appCtrl.controller('surveyCtrl', function($scope, $state, RequestService, TimeService) {
	//if(typeof analytics !== undefined) { analytics.trackView("Survey"); }

    $scope.init = function() {
		$scope.surveys = [];

        $scope.getData();
	}

	$scope.getData = function() {
        RequestService.getall('survey').then(function(surveys) {
            for (var ii = 0; ii < surveys.length; ii++) {
                var departure = new Date(surveys[ii].departure);
                surveys[ii].departure_month = TimeService.months[departure.getMonth()] + ' ' + departure.getFullYear();

                surveys[ii].price_million = surveys[ii].price / 1000000;
                
                $scope.surveys.push(surveys[ii]);
            }

            $('#survey-loading-progress').hide();
            $('#survey-content').show();
        });
    }

    $scope.doRefresh = function() {
        $scope.surveys = [];
        
        RequestService.getall('survey').then(function(surveys) {
            for (var ii = 0; ii < surveys.length; ii++) {
                var departure = new Date(surveys[ii].departure);
                surveys[ii].departure_month = TimeService.months[departure.getMonth()] + ' ' + departure.getFullYear();

                surveys[ii].price_million = surveys[ii].price / 1000000;
                
                $scope.surveys.push(surveys[ii]);
            }
        })

        .finally(function() {
            $scope.$broadcast('scroll.refreshComplete');
        });
    }

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            survey: objectData
        });
    };

	$scope.init();
});

appCtrl.controller('surveyDetailCtrl', function($scope, $stateParams, $ionicPopup, $ionicHistory, RequestService) {
	//if(typeof analytics !== undefined) { analytics.trackView("Survey Detail"); }

    $scope.init = function() {
        $scope.data = { choice: '' };
        $scope.survey = $stateParams.survey;

        $scope.rating = {};
        $scope.rating.rate = $scope.survey.default;
        $scope.rating.max  = $scope.survey.max;
	}

    $scope.submitQuestionnaire = function() {
        if (!$scope.data.choice) {
            $scope.showAlert('warning', 'questionnaire');
        } else {
            var deviceInfo = ionic.Platform.device();

            var data = {
                id_question: $scope.survey.id,
                answer: $scope.data.choice,
                        uuid: deviceInfo.uuid,
                        device: deviceInfo.model
                        //uuid: '5b030556347f72ed',
                        //device: 'SM-A520F'
            }

            RequestService.save(data, 'surveyresult').then(function(response) {
                $scope.showAlert('success', 'questionnaire', $scope.data.choice);
            });
        }
    }

    $scope.submitRate = function() {
        var deviceInfo = ionic.Platform.device();

        var data = {
            id_question: $scope.survey.id,
            answer: $scope.rating.rate,
                        uuid: deviceInfo.uuid,
                        device: deviceInfo.model
                        //uuid: '5b030556347f72ed',
                        //device: 'SM-A520F'
        }

        RequestService.save(data, 'surveyresult').then(function(response) {
            $scope.showAlert('success', 'rate', $scope.rating.rate);
        });
    }

    $scope.showAlert = function(type, survey, choice) {
        if (type == 'warning') {
            title = 'Pesan';
            template = 'Harap pilih salah satu.';
        } else if (type == 'success') {
            title = 'Terima Kasih';

            if (survey == 'questionnaire') {
                template = 'Anda telah memilih ' + choice + '.<br>Terima Kasih.';
            } else if (survey == 'rate') {
                template = 'Anda memberi menilai ' + choice + '.<br>Terima Kasih.';
            }
        }

        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template
        });

        if (type == 'success') {
            alertPopup.then(function(res) {
                $ionicHistory.goBack();
            });
        }
    }

	$scope.init();
});