appCtrl.controller('calculatorCtrl', function($scope, $ionicPopup, $timeout, $state, RequestService) {
	//if(typeof analytics !== undefined) { analytics.trackView("Calculator"); }

    $scope.init = function() {
		$scope.calculator = [];
		$scope.types = [];
		$scope.jobs = [];
		$scope.dp;
		$scope.periods = [];
		$scope.period;
		$scope.margin;

		RequestService.getall('finance').then(function(types) {
			for (var ii = 0; ii < types.length; ii++) {
				$scope.types.push({value: types[ii], label: types[ii].name});
			}
		});
	}

	$scope.selectType = function(type) {
		if (type) {
			$scope.jobs = [];

			for (var ii = 0; ii < type.rate.length; ii++) {
				$scope.jobs.push({value: type.rate[ii], label: type.rate[ii].job})
			}
		}
	}

	$scope.selectJob = function(job) {
		if (job) {
			$scope.periods = [];

			$scope.dp = job.dp;

			for (var ii = 0; ii < job.margin.length; ii++) {
				$scope.periods.push({value: job.margin[ii], label: job.margin[ii].period})
			}
		}
	}

	$scope.selectPeriod = function(period) {
		if (period) {
			$scope.period = period.period;
			$scope.margin = period.margin
		}
	}

	$scope.formatCurrency = function() {
		var selection = window.getSelection().toString();
		console.log(selection);
		if (selection !== '') {
			return;
		}

		if ($.inArray(event.keyCode, [38,40,37,39]) !== -1) {
			return;
		}

		var input = $scope.calculator.amount.replace(/[\D\s\._\-]+/g, "");
		input = input ? parseInt(input) : 0;
		$scope.calculator.amount = (input === 0) ? "" : input.toLocaleString("en-US");
    }

    $scope.submitCalculator = function() {
    	if ($scope.calculator.type && $scope.calculator.job && $scope.calculator.period && $scope.calculator.amount) {
    		$('#calculator-loading-progress').show();

    		$('#calculator-result').hide();

    		$scope.doCalculate();
    	} else {
    		$scope.showAlert('warning');
    	}
    }

    $scope.showAlert = function(type) {
    	if (type == 'warning') {
    		title = 'Pesan';
    		template = 'Harap lengkapi data.';
    	}

    	var alertPopup = $ionicPopup.alert({
    		title: title,
    		template: template
    	});
    }

    $scope.doCalculate = function() {
    	$scope.result = [];

    	$timeout(function() {
    		var Amount = $scope.calculator.amount.replace(/,/g,'');
    		$scope.result.dp = Amount * $scope.dp / 100;
    		$scope.result.financing_bank = Amount - $scope.result.dp;
            // $scope.result.financing_total = parseInt($scope.result.financing_bank + ($scope.result.financing_bank * ($scope.margin / 100)));
            $scope.result.financing_total = parseInt($scope.result.financing_bank + ($scope.result.financing_bank * ($scope.margin / 100) * ($scope.period / 12)));
            $scope.result.installment_payment = parseInt($scope.result.financing_total / $scope.period);
            $scope.result.min_salary = parseInt($scope.result.installment_payment / .4);
            console.log($scope.result);
            
            $('#calculator-loading-progress').hide();
            $('#calculator-result').show();
        }, 2000);
    }

    $scope.navigateTo = function (targetPage) {
    	$state.go(targetPage);
    };

    $scope.init();
});