appCtrl.controller('menuCtrl', function($scope, UserService, $ionicActionSheet, $state, $ionicLoading, $cordovaGoogleAnalytics){
	//if(typeof analytics !== undefined) { $cordovaGoogleAnalytics.trackView("Menu"); }

    $scope.showLogOutMenu = function() {
		var hideSheet = $ionicActionSheet.show({
			destructiveText: 'Logout',
			titleText: 'Anda yakin ingin logout?',
			cancelText: 'Cancel',
			cancel: function() {},
			buttonClicked: function(index) {
				return true;
			},
			destructiveButtonClicked: function(){
				$ionicLoading.show({
					template: 'Logging out...'
				});
				// Google logout
				window.plugins.googleplus.logout(
					function (msg) {
						console.log(msg);
						$ionicLoading.hide();
						$state.go('login');
					},
					function(fail){
						console.log(fail);
					}
				);
			}
		});
	};
});
