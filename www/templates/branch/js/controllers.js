appCtrl.controller('branchCtrl', function($scope, $state, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Branch"); }

    $scope.baseUrl = RequestService.urlImageLink;
    $scope.init = function() {
        // RequestService.getcount('branch').then(function(response) {
        //     $scope.total = response[0].$1;
        // });

        $scope.branchs = [];

        $scope.page = 1;
        $scope.limit = 3;

        $scope.noMoreBranch = false;

        // $scope.getData();
    }

	// $scope.getData = function() {
     //        RequestService.getlimit('branch', 1, 2).then(function(branchs) {
     //            for (var ii = 0; ii < branchs.length; ii++) {
     //                $scope.branchs.push(branchs[ii]);
     //            }

     //            $('#branch-loading-progress').hide();
     //            $('#branch-content').show();
     //        });
    //    }

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            branch: objectData
        });
    };

    $scope.loadMoreBranchs = function() {
        RequestService.getlimit('branch', $scope.page, $scope.limit).then(function(branchs) {
            for (var ii = 0; ii < branchs.length; ii++) {
                $scope.branchs.push(branchs[ii]);
            }

            $scope.$broadcast('scroll.infiniteScrollComplete');

            $scope.page += 1;

            // if ($scope.branchs.length == $scope.total) {
            //     $scope.noMoreBranch = true;
            // }

            if (branchs.length != $scope.limit) {
                $scope.noMoreBranch = true;
            }
        });
    };

    $scope.init();
});

appCtrl.controller('branchDetailCtrl', function($scope, $stateParams, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Branch Detail"); }

    $scope.baseUrl = RequestService.urlImageLink;
    $scope.init = function() {
      $scope.branch = $stateParams.branch;
  }

  $scope.init();
});

appCtrl.controller('branchSearchCtrl', function($scope, $state, $ionicPopup, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Branch Search"); }

    $scope.baseUrl = RequestService.urlImageLink;
    $scope.init = function() {
        $scope.search = [];
        $scope.cities = [{value: '', label: 'Semua'}];
        $scope.results = [];
        $scope.info = '';

        RequestService.getallnopaging('city').then(function(cities) {
            for (var ii = 0; ii < cities.length; ii++) {
                $scope.cities.push({value: cities[ii].name, label: cities[ii].name});
            }
        });
    }

    $scope.searchBranch = function() {
        // if ($scope.search.location) {
            $scope.results = [];

            $('#branch-search-loading-progress').show();

            var data = {
                'key': $scope.search.location,
            };
            
            RequestService.search(data, 'branch').then(function(results) {
                if (results.length > 0) {
                    for (var ii = 0; ii < results.length; ii++) {
                        // var departure = new Date(results[ii].departure);
                        // results[ii].departure_month = TimeService.months[departure.getMonth()] + ' ' + departure.getFullYear();

                        // results[ii].price_million = results[ii].price / 1000000;
                        
                        $scope.results.push(results[ii]);
                    }

                    $scope.info = results.length + ' data ditemukan';
                } else {
                    $scope.info = 'Data belum tersedia';
                }
                
                $('#branch-search-loading-progress').hide();
                $('#branch-result').show();
            });
        // } else {
        //     $scope.showAlert();
        // }
    }

    $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'Pesan',
            template: 'Harap lengkapi data.'
        });
    }

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            branch: objectData
        });
    };

    $scope.init();
});