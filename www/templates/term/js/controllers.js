appCtrl.controller('termCtrl', function($scope, $state, RequestService, TimeService) {
	//if(typeof analytics !== undefined) { analytics.trackView("Term"); }

    $scope.init = function() {
		$scope.terms = [];

        $scope.getData();
	}

	$scope.getData = function() {
        RequestService.getall('term').then(function(terms) {
            for (var ii = 0; ii < terms.length; ii++) {
                $scope.terms.push(terms[ii]);
            }

            $('#term-loading-progress').hide();
            $('#term-content').show();
        });
    }

    $scope.doRefresh = function() {
        $scope.terms = [];
        
        RequestService.getall('term').then(function(terms) {
            for (var ii = 0; ii < terms.length; ii++) {
                $scope.terms.push(terms[ii]);
            }
        })

        .finally(function() {
            $scope.$broadcast('scroll.refreshComplete');
        });
    }

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            term: objectData
        });
    };

	$scope.init();
});

appCtrl.controller('termDetailCtrl', function($scope, $stateParams) {
	//if(typeof analytics !== undefined) { analytics.trackView("Term Detail"); }

    $scope.init = function() {
		$scope.term = $stateParams.term;
	}

	$scope.init();
});