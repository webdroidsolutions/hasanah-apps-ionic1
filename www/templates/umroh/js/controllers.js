// appCtrl.controller('umrohCtrl', function($scope, $state, RequestService, TimeService) {
// 	$scope.init = function() {
// 		$scope.umrohs = [];

//         $scope.getData();
// 	}

// 	$scope.getData = function() {
//         RequestService.getall('umroh').then(function(umrohs) {
//             for (var ii = 0; ii < umrohs.length; ii++) {
//                 var departure = new Date(umrohs[ii].departure);
//                 umrohs[ii].departure_month = TimeService.months[departure.getMonth()] + ' ' + departure.getFullYear();

//                 umrohs[ii].price_million = umrohs[ii].price / 1000000;

//                 $scope.umrohs.push(umrohs[ii]);
//             }

//             $('#umroh-loading-progress').hide();
//             $('#umroh-content').show();
//         });
//     }

//     $scope.doRefresh = function() {
//         $scope.umrohs = [];

//         RequestService.getall('umroh').then(function(umrohs) {
//             for (var ii = 0; ii < umrohs.length; ii++) {
//                 var departure = new Date(umrohs[ii].departure);
//                 umrohs[ii].departure_month = TimeService.months[departure.getMonth()] + ' ' + departure.getFullYear();

//                 umrohs[ii].price_million = umrohs[ii].price / 1000000;

//                 $scope.umrohs.push(umrohs[ii]);
//             }
//         })

//         .finally(function() {
//             $scope.$broadcast('scroll.refreshComplete');
//         });
//     }

//     $scope.navigateTo = function (targetPage, objectData) {
//         $state.go(targetPage, {
//             umroh: objectData
//         });
//     };

// 	$scope.init();
// });



appCtrl.controller('umrohCtrl', function($scope, $state, RequestService, TimeService, ionicDatePicker, $ionicPopup, $cordovaActionSheet) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh"); }

    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {

		$scope.checkClass = {};
		$scope.search = [];
        $scope.start_month = [];
        $scope.end_month = [];
        $scope.search.date_start = null;
        $scope.search.date_end = null;
        $scope.cities = [{value: '', label: 'Semua'}];
        // $scope.search.hotel = 3;
        $scope.planes = [{value: '', label: 'Semua'}];
        $scope.types = [];
        $scope.results = [];
        $scope.info = '';
        $scope.stars = [{
        	value: null,
        	label: 'Semua'
        }, {
        	value: 1,
        	label: 'Bintang 1'
        }, {
        	value: 2,
        	label: 'Bintang 2'
        }, {
        	value: 3,
        	label: 'Bintang 3'
        }, {
        	value: 4,
        	label: 'Bintang 4'
        }, {
        	value: 5,
        	label: 'Bintang 5'
        }]

        $scope.packets = [{
        	value: '',
        	label: 'Semua'
        }, {
        	value: 'Umroh Reguler',
        	label: 'Umroh Reguler'
        }, {
        	value: 'Umroh Plus',
        	label: 'Umroh Plus'
        }, {
        	value: 'Wisata Muslim',
        	label: 'Wisata Muslim'
        }];

        

        RequestService.getDepartureCity().then(function(cities) {
        	for (var ii = 0; ii < cities.length; ii++) {
        		$scope.cities.push({value: cities[ii].city, label: cities[ii].city});
        	}
        });


        RequestService.getall('product').then(function(types) {
        	for (var ii = 0; ii < types.length; ii++) {
        		$scope.types.push({value: types[ii].id, label: types[ii].name});
        	}
        });

        RequestService.getvar('umroh', 'plane').then(function(planes) {
        	for (var ii = 0; ii < planes.length; ii++) {
        		$scope.planes.push({value: planes[ii].plane, label: planes[ii].plane});
        	}
        });

        // $scope.planes = [{
        //     value: 'Garuda Indonesia',
        //     label: 'Garuda Indonesia'
        // }, {
        //     value: 'AirAsia',
        //     label: 'AirAsia'
        // }, {
        //     value: 'Citilink',
        //     label: 'Citilink'
        // }, {
        //     value: 'Fly Emirates',
        //     label: 'Fly Emirates'
        // }];
        // var dpObjStart = {
        //     callback: function (val) {  //Mandatory
        //     	var date = new Date(val);
        //     	date = new Date(date.getFullYear(), date.getMonth());
        //     	$scope.date_start_ = date;
        //     	$scope.search.date_start = TimeService.months[date.getMonth()] + ' ' + date.getFullYear();

        //     	$scope.dpObjEnd = {
        //             callback: function (val) {  //Mandatory
        //             	var date = new Date(val);
        //             	date = new Date(date.getFullYear(), date.getMonth());
        //             	$scope.date_end_ = date;
        //             	$scope.search.date_end = TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
        //             },
        //             from: new Date($scope.date_start_.getFullYear(), $scope.date_start_.getMonth(), 1), //Optional
        //             to: new Date(new Date().getFullYear()+2, new Date().getMonth(), 1), //Optional
        //             inputDate: new Date(),      //Optional
        //             mondayFirst: true,          //Optional
        //             closeOnSelect: false,       //Optional
        //             templateType: 'popup'       //Optional
        //         };
        //     },
        //     from: new Date(new Date().getFullYear(), new Date().getMonth(), 1), //Optional
        //     to: new Date(new Date().getFullYear()+2, new Date().getMonth(), 1), //Optional
        //     inputDate: new Date(),      //Optional
        //     mondayFirst: true,          //Optional
        //     closeOnSelect: false,       //Optional
        //     templateType: 'popup'       //Optional
        // };

        // $scope.openDatePickerStart = function() {
        // 	ionicDatePicker.openDatePicker(dpObjStart);
        // };

        // $scope.openDatePickerEnd = function() {
        // 	if ($scope.search.date_start) {
        // 		ionicDatePicker.openDatePicker($scope.dpObjEnd);
        // 	}
        // }

        var current_date = new Date();
        var current_month = current_date.getMonth();

        $scope.changeEndMonth = function() {
            if ($scope.search.date_end)
            {
                var selected_end_year = parseInt($scope.search.date_end.split("-")[0])
                var selected_end_month = parseInt($scope.search.date_end.split("-")[1]);
            }
            $scope.end_month = [];
            for (var m=0;m<12;m++)
            {
                var selected_start_year = parseInt($scope.search.date_start.split("-")[0])
                var selected_start_month = parseInt($scope.search.date_start.split("-")[1]);
                if (m+1<selected_start_month)
                    $scope.end_month.push({value: (selected_start_year+1)+"-"+(m+1), label: TimeService.months[m] + ' ' + (selected_start_year+1)});
                else
                    $scope.end_month.push({value: selected_start_year+"-"+(m+1), label: TimeService.months[m] + ' ' + selected_start_year});
            }

            if ($scope.search.date_end)
            {
                $scope.search.date_end = $scope.end_month[selected_end_month-1].value;
            }
        }

        $scope.start_month = [];
        for (var m=0;m<12;m++)
        {
            if (m<current_month)
                $scope.start_month.push({value: (current_date.getFullYear()+1)+"-"+(m+1), label: TimeService.months[m] + ' ' + (current_date.getFullYear()+1)});
            else
                $scope.start_month.push({value: current_date.getFullYear()+"-"+(m+1), label: TimeService.months[m] + ' ' + current_date.getFullYear()});
        }

        $scope.search.date_start = $scope.start_month[current_month].value;
        $scope.changeEndMonth();
        $scope.search.date_end = $scope.end_month[current_month].value;
    }

    $scope.searchUmroh = function() {
    	$scope.results = [];

    	var hotelClass;

    	$('#umroh-loading-progress').show();

    	if ($scope.checkClass.all) {
    		hotelClass = '';
    	} else {
    		hotelClass = $scope.search.hotel;
    	}

    	var data = {
    		'city': $scope.search.city,
    		'date_start': $scope.search.date_start,
    		'date_end': $scope.search.date_end,
    		'packet': $scope.search.packet,
    		'plane': $scope.search.plane,
    		'min_price': $scope.search.min_price,
    		'max_price': $scope.search.max_price,
    		'hotel': hotelClass
    	};

        // RequestService.search(data, 'umroh').then(function(results) {
        //     if (results.length > 0) {
        //         for (var ii = 0; ii < results.length; ii++) {
        //             var departure = new Date(results[ii].departure);
        //             results[ii].departure_month = TimeService.months[departure.getMonth()] + ' ' + departure.getFullYear();

        //             results[ii].price_million = results[ii].price / 1000000;

        //             $scope.results.push(results[ii]);
        //         }

        //         // $scope.info = results.length + ' data ditemukan';
        //         // window.plugins.toast.showShortCenter(results.length + " Data Ditemukan");
        //         $state.go('app.umroh-results', { umrohs: $scope.results });
        //     } else {
        //         // $scope.info = 'Data belum tersedia';
        //         // window.plugins.toast.showShortCenter("Data Belum Tersedia");
        //     }

        //     $('#umroh-loading-progress').hide();
        //     $('#umroh-result').show();
        // });

        RequestService.getcount(data, 'umroh').then(function(result) {
        	if (result > 0) {
                // $scope.info = results.length + ' data ditemukan';
                // window.plugins.toast.showShortCenter(results.length + " Data Ditemukan");

                $state.go('app.umroh-results', { search: data, total: result });
            } else {
                // $scope.info = 'Data belum tersedia';
                //window.plugins.toast.showShortCenter("Data Belum Tersedia");
                $scope.showAlert2('Warning', 'Data Pencarian Umroh Tidak Ditemukan');
            }

            $('#umroh-loading-progress').hide();
            $('#umroh-result').show();
        });
    }

    $scope.showAlert2 = function(title, msg) {
    	var alertPopup = $ionicPopup.alert({
    		title: title,
    		template: msg
    	});
    };

    $scope.navigateTo = function (targetPage, objectData) {
    	$state.go(targetPage, {
    		umroh: objectData
    	});
    };

    $scope.init();
});

appCtrl.controller('umrohResultsCtrl', function($scope, $state, $stateParams, TimeService, RequestService) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh Result"); }

    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {
        //window.plugins.toast.showShortCenter($stateParams.total + " Data Ditemukan");

        $scope.umrohs = [];

        var search = $stateParams.search;

        $scope.noMoreUmroh = false;

        search.page = 1;
        search.limit = 2;

        $scope.loadMoreUmrohs = function() {
        	RequestService.search(search, 'umroh').then(function(umrohs) {
        		for (var ii = 0; ii < umrohs.length; ii++) {
        			var departure = new Date(umrohs[ii].departure);
        			umrohs[ii].departure_month = TimeService.months[departure.getMonth()] + ' ' + departure.getFullYear();
        			umrohs[ii].departure_string = moment(umrohs[ii].departure).format('DD MMMM YYYY');
        			umrohs[ii].price_million = umrohs[ii].price / 1000000;
                    //umrohs[ii].img = RequestService.getUrlImg() + umrohs[ii].img;
                    //Hardcode Beng

                    // umrohs[ii].quad_price = 28000000;
                    // umrohs[ii].triple_price = 29000000;
                    // umrohs[ii].double_price = 30000000;
                    // umrohs[ii].handling_price = 175000;
                    // umrohs[ii].infant_price = 7000000;
                    
                    // umrohs[ii].mahram_price = 500000;

                    // if(umrohs[ii].child_price) {
                    //     umrohs[ii].child_price = 0;
                    // }

                    // if(umrohs[ii].quad_price) {
                    //     umrohs[ii].quad_price = 0;
                    // }

                    // if(umrohs[ii].triple_price) {
                    //     umrohs[ii].triple_price = 0;
                    // }

                    // if(umrohs[ii].double_price) {
                    //     umrohs[ii].double_price = 0;
                    // }

                    // if(umrohs[ii].handling_price){
                    //     umrohs[ii].handling_price = 0;
                    // }

                    // if(umrohs[ii].mahram_price) {
                    //     umrohs[ii].mahram_price = 0;
                    // }

                    // if(umrohs[ii].infant_price) {
                    //     umrohs[ii].infant_price = 0;
                    // }

                    // if(umrohs[ii].travelagent_id) {
                    //     umrohs[ii].travelagent_id = null;
                    // }


                    $scope.umrohs.push(umrohs[ii]);
                }

                $scope.$broadcast('scroll.infiniteScrollComplete');

                search.page += 1;

                if (umrohs.length != search.limit) {
                	$scope.noMoreUmroh = true;
                }
            });
        };
    };

    $scope.navigateTo = function (targetPage, objectData) {
    	$state.go(targetPage, {
    		umroh: objectData
    	});
    };

    $scope.init();
});

appCtrl.controller('umrohDetailCtrl', function(RequestService, $scope, $state, $stateParams, $ionicPopup) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh Detail"); }

    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {
		$scope.umroh = [];
		$scope.umroh.content = null;
		$scope.umroh = $stateParams.umroh;
		$scope.umroh.mecca_pilgrimage_list_obj = [];
		if($scope.umroh.mecca_pilgrimage_list !== undefined && $scope.umroh.mecca_pilgrimage_list !== null) {
			i = 0;
			arrSplit = $scope.umroh.mecca_pilgrimage_list.split(',');
			while (i < arrSplit.length) {
				j = i + 1;
				$scope.umroh.mecca_pilgrimage_list_obj.push({
					'number':j, 'listname':arrSplit[i].trim()
				});
				i++;
			}
		}

		$scope.umroh.madina_pilgrimage_list_obj = [];
		console.log($scope.umroh);
		if($scope.umroh.madina_pilgrimage_list !== undefined && $scope.umroh.madina_pilgrimage_list !== null) {
			i = 0;
			arrSplit = $scope.umroh.madina_pilgrimage_list.split(',');
			while (i < arrSplit.length) {
				j = i + 1;
				$scope.umroh.madina_pilgrimage_list_obj.push({
					'number':j, 'listname':arrSplit[i].trim()
				});
				i++;
			}
		}

		console.log($scope.umroh);

		var bintangHotel = $stateParams.umroh.hotel;
		$scope.lists = [];
		$scope.var = [];
		$scope.bintang = [];
		$scope.sentItem = [];
		$scope.user = [];
		$scope.var.jmlInbox = 0;
		$scope.user.id = null;
		$scope.messages = [];
		$scope.var.totalPesan = 0;
		if(bintangHotel != null) {
			var i = 0;
			while (i < bintangHotel )
			{
				$scope.bintang.push(i);
				i++;
			}
		}

		$scope.userCheck();


		var data = {
			"limit": 2,
			"page": 1,
			"umroh_id": $scope.umroh.id
		}



		RequestService.getRatingReview(data).then(function(response) {
			$scope.var.rating_avg = Math.round(response.rating_avg);
			$scope.lists = response.list;
            //getCountRatingRevie
            var data = {
            	"umroh_id": $scope.umroh.id
            }
            RequestService.getCountRatingReview(data).then(function(response) {
            	$scope.var.totalCount = response[0].total;

            	var data = {
            		"umroh_id": $scope.umroh.id,
            		"limit": 2,
            		"page": 1
            	}
            	RequestService.getMessageUmroh(data).then(function(response) {
            		$scope.messages = response;
            		i = 0;
            		while (i < response.length) {
            			$scope.messages[i].create_date = moment($scope.messages.create_date).format('DD MMM YYYY - HH:mm');
            			if($scope.messages[i].reply_date != null) {
            				$scope.messages[i].reply_date = moment($scope.messages[i].reply_date).format('DD MMM YYYY - HH:mm');
            			}
            			i++;
            		}

            		var data = {
            			"umroh_id": $scope.umroh.id
            		}

            		RequestService.getCountByUmroh(data).then(function(response) {
            			$scope.var.totalPesan = response[0].total;
            		});

            	});

            });
        });

	}

	$scope.sendMessage = function() {

		if(localStorage.user)
		{
			var data = {
				'umroh_id' : $scope.umroh.id,
				'travelagent': $scope.umroh.travel
			};
			$scope.navigateTo('app.message-send', data);
		}
		else
		{
			setTimeout(function(){
				$scope.navigateTo('app.profile', $scope.umroh);
			}, 800);

		}
	}

	$scope.userCheck = function() {
		if (localStorage.user) {
			$scope.user = JSON.parse(localStorage.user);

		} else {
			$scope.user = [];
		}
	}

    // $scope.interested = function() {
    //     $scope.interest = {};

    //     var alertPopup = $ionicPopup.show({
    //         title: 'Permohonan',
    //         subTitle: 'Harap masukkan data Anda',
    //         template: '<input type="text" placeholder="Nama" ng-model="interest.name"><input type="tel" placeholder="No. Handphone" ng-model="interest.phone">',
    //         scope: $scope,
    //         buttons: [
    //             { text: 'Batal' },
    //             {
    //                 text: '<b>Submit</b>',
    //                 type: 'button-positive',
    //                 onTap: function(e) {
    //                     if ($scope.interest.name && $scope.interest.phone) {
    //                         return $scope.interest;
    //                     } else {
    //                         e.preventDefault();
    //                     }
    //                 }
    //             }
    //         ]
    //     });

    //     alertPopup.then(function(res) {
    //         if (res) {
    //             var alertPopup = $ionicPopup.alert({
    //                 title: 'Pesan',
    //                 template: 'Terima Kasih, Bpk/Ibu ' + res.name + '.<br>Permohonan Anda akan segera kami proses.'
    //             });
    //         }
    //     });
    // }


    $scope.submitDaftar = function() {
    	if(localStorage.user)
    	{
    		$scope.navigateTo('app.umroh-register', $scope.umroh);
    	}
    	else
    	{
    		setTimeout(function(){
    			$scope.navigateTo('app.profile', $scope.umroh);
    		}, 800);

    	}

    }

    $scope.submitFilling = function() {
    	if(localStorage.user)
    	{
    		$scope.navigateTo('app.umroh-filing', $scope.umroh);
    	}
    	else
    	{
    		$scope.navigateTo('app.profile', $scope.umroh);
    	}
    }

    $scope.toRatingReview = function() {
    	var data = {
    		'umroh_id' : $scope.umroh.id,
    		'totalCount' : $scope.var.totalCount,
    		'rating_avg' : $scope.var.rating_avg
    	}

    	$scope.navigateTo('app.umroh-rating', data);
    }

    $scope.toAllMessage = function() {
    	var data = {
    		'umroh_id' : $scope.umroh.id,
    		'totalCount' : $scope.var.totalPesan,
    		'travel' : $scope.umroh.travel
    	}

    	$scope.navigateTo('app.umroh-pesan', data);
    }
    

    $scope.navigateTo = function (targetPage, objectData) {
    	$state.go(targetPage, {
    		umroh: objectData
    	});
    };

    $scope.init();
});


appCtrl.controller('umrohRatingCtrl', function(RequestService, $scope, $state, $stateParams, $ionicPopup) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh Rating"); }

    $scope.init = function() {
		$scope.umroh = $stateParams.umroh;
		$scope.lists = [];
		$scope.var = [];

		$scope.var.noMoreUmroh = false;

		$scope.var.page = 1;
		$scope.var.limit = 2;
		$scope.var.jmlTampil = 0;


		$scope.loadMoreRating = function() {
			var data = {
				"limit": $scope.var.limit,
				"page": $scope.var.page,
				"umroh_id": $scope.umroh.umroh_id
			}
			RequestService.getRatingReview(data).then(function(response) {
                // $scope.var.rating_avg = Math.round(response.rating_avg);
                i = 0;
                while (i < response.list.length) {
                	response.list[i].create_date = moment(response.list[i].create_date).format('DD/MM/YY HH:mm');
                	$scope.lists.push(response.list[i]);
                	i++;
                }
                
                //getCountRatingRevie
                $scope.$broadcast('scroll.infiniteScrollComplete');
                
                $scope.var.page += 1;
                $scope.var.jmlTampil += 2;
                if ($scope.var.jmlTampil >= $scope.umroh.totalCount) {
                	$scope.var.noMoreUmroh = true;
                }

            });
		}

	}



	$scope.init();


});


appCtrl.controller('umrohPesanCtrl', function(RequestService, $scope, $state, $stateParams, $ionicPopup) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh Pesan"); }

    $scope.init = function() {
		$scope.umroh = $stateParams.umroh;
		$scope.lists = [];
		$scope.var = [];

		$scope.var.noMoreUmroh = false;

		$scope.var.page = 1;
		$scope.var.limit = 2;
		$scope.var.jmlTampil = 0;


		$scope.loadMoreRating = function() {
			var data = {
				"limit": $scope.var.limit,
				"page": $scope.var.page,
				"umroh_id": $scope.umroh.umroh_id
			}
			RequestService.getMessageUmroh(data).then(function(response) {
                // $scope.var.rating_avg = Math.round(response.rating_avg);
                i = 0;
                while (i < response.length) {
                	response[i].create_date = moment(response[i].create_date).format('DD/MM/YY HH:mm');
                	$scope.lists.push(response[i]);
                	i++;
                }
                
                //getCountRatingRevie
                $scope.$broadcast('scroll.infiniteScrollComplete');
                
                $scope.var.page += 1;
                $scope.var.jmlTampil += 2;
                if ($scope.var.jmlTampil >= $scope.umroh.totalCount) {
                	$scope.var.noMoreUmroh = true;
                }

            });
		}

	}



	$scope.init();


});


appCtrl.controller('umrohRegisterCtrl', function($ionicLoading, ionicDatePicker, $ionicModal, $scope, $state, $stateParams, $ionicHistory, RequestService, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $cordovaActionSheet) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh Register"); }

    $scope.init = function() {
		var a, b;
		$scope.umroh = $stateParams.umroh;
        console.log('--------------');
        console.log(JSON.stringify($scope.umroh));

        $scope.register = [];
        $scope.branchs = [];


        $scope.jamaah = [];
        $scope.var = [];
        $scope.var.tglKeberangkatan = $scope.umroh.departure;
        $scope.var.selisih = [];
        $scope.var.statusMahram = [];
        $scope.var.tglLahir = [];
        $scope.var.namaLengkap = [];
        $scope.var.namaAyahKandung = [];
        $scope.var.hubunganDenganBNI = [];
        $scope.var.ukuranPakaian = [];
        $scope.var.tempatLahir = [];
        $scope.var.passengger = [];

        $scope.var.provinsi = [];
        $scope.var.kotaKabupaten = [];
        $scope.var.kecamatan = [];
        $scope.var.kelurahan = [];
        $scope.var.rt = [];
        $scope.var.rw = [];
        $scope.var.alamat = [];
        $scope.var.nomorTelpRumah = [];

        $scope.var.namaMahram = [];
        $scope.var.nomorAkta = [];
        $scope.var.nomorHP = [];
        $scope.var.nomorKTP = [];
        $scope.var.nomorHPOrtu = [];
        $scope.var.rekeningBNISyariah = [];

        $scope.var.jenisKelamin = [];
        $scope.var.foto = [];
        $scope.var.ktp = [];
        $scope.var.kk = [];
        $scope.var.passport = [];
        $scope.var.RekeningBNISyariah = [];
        $scope.var.jmljamaah = '1';

        var j;
        j = 0;
        while (j < 4)
        {
           $scope.var.statusMahram[j] = 'sendiri';
           $scope.var.tglLahir[j] = moment().format('DD MMMM YYYY');
           $scope.var.hubunganDenganBNI[j] = 'umum';
           $scope.var.ukuranPakaian[j] = 'm';
           $scope.var.jenisKelamin[j] = 'pria';
           $scope.var.RekeningBNISyariah[j] = '';
           a = moment($scope.var.tglKeberangkatan);
           b = moment();
           $scope.var.selisih[j] = a.diff(b, 'years', true);
           j++;
       }


       $scope.var.harga = [];

       $scope.var.jamaahDewasa = 0;
       $scope.var.jamaahInfant = 0;
       $scope.var.jamaahChild = 0;
       $scope.var.handling = 0;
       $scope.var.mahram = 0;
       $scope.var.totalPrice = 0;
       $scope.var.order_type = 'send';
       $scope.var.uri = RequestService.getUrlUpload();
       $scope.user = JSON.parse(localStorage.user);
       $scope.var.hargaPilihan = 0;
       $scope.provinsi = [];
       $scope.kabupaten = [];
       $scope.kecamatan = [];
       $scope.kelurahan = [];
       if($scope.umroh != null) {
           if(!$scope.umroh.quad_price || $scope.umroh.quad_price != null)
           {
            $scope.var.harga.push({
             'price_name' : 'Harga Quad',
             'price' : $scope.umroh.quad_price
         });
        }
    }

    RequestService.getallnopaging('branch').then(function(branchs) {
       for (var ii = 0; ii < branchs.length; ii++) {
        $scope.branchs.push({ 'value' : branchs[ii].id, 'label': branchs[ii].name });
    }
});

    RequestService.getProvinsi().then(function(response) {
       console.log(response);
            //UNCOMMENT FOR LOCALHOST
            //response = {"error":false,"message":"Berhasil mendapatkan data provinsi","semuaprovinsi":[{"id":"11","nama":"Aceh"},{"id":"12","nama":"Sumatera Utara"},{"id":"13","nama":"Sumatera Barat"},{"id":"14","nama":"Riau"},{"id":"15","nama":"Jambi"},{"id":"16","nama":"Sumatera Selatan"},{"id":"17","nama":"Bengkulu"},{"id":"18","nama":"Lampung"},{"id":"19","nama":"Kepulauan Bangka Belitung"},{"id":"21","nama":"Kepulauan Riau"},{"id":"24","nama":"Kalimantan Utara"},{"id":"31","nama":"Dki Jakarta"},{"id":"32","nama":"Jawa Barat"},{"id":"33","nama":"Jawa Tengah"},{"id":"34","nama":"Di Yogyakarta"},{"id":"35","nama":"Jawa Timur"},{"id":"36","nama":"Banten"},{"id":"51","nama":"Bali"},{"id":"52","nama":"Nusa Tenggara Barat"},{"id":"53","nama":"Nusa Tenggara Timur"},{"id":"61","nama":"Kalimantan Barat"},{"id":"62","nama":"Kalimantan Tengah"},{"id":"63","nama":"Kalimantan Selatan"},{"id":"64","nama":"Kalimantan Timur"},{"id":"65","nama":"Kalimantan Utara"},{"id":"71","nama":"Sulawesi Utara"},{"id":"72","nama":"Sulawesi Tengah"},{"id":"73","nama":"Sulawesi Selatan"},{"id":"74","nama":"Sulawesi Tenggara"},{"id":"75","nama":"Gorontalo"},{"id":"76","nama":"Sulawesi Barat"},{"id":"81","nama":"Maluku"},{"id":"82","nama":"Maluku Utara"},{"id":"91","nama":"Papua Barat"},{"id":"94","nama":"Papua"}]}
            
            $scope.provinsi = response.semuaprovinsi;            
        });


};

$scope.changeJmlJamaah = function() {
  $scope.var.harga = [];
  $scope.var.hargaPilihan = 0;
  if($scope.var.jmljamaah == 1) {
   if($scope.umroh != null) {
    if(!$scope.umroh.quad_price || $scope.umroh.quad_price != null)
    {
     $scope.var.harga.push({
      'price_name' : 'Harga Quad',
      'price' : $scope.umroh.quad_price
  });
 }
}
} else {
   if($scope.umroh != null) {
    if(!$scope.umroh.quad_price || $scope.umroh.quad_price != null)
    {
     $scope.var.harga.push({
      'price_name' : 'Harga Quad',
      'price' : $scope.umroh.quad_price
  });
 }

 if(!$scope.umroh.triple_price || $scope.umroh.triple_price != null) {
     $scope.var.harga.push({
      'price_name' : 'Harga Triple',
      'price' : $scope.umroh.triple_price 
  })
 }

 if(!$scope.umroh.double_price || $scope.umroh.double_price != null) {
     $scope.var.harga.push({
      'price_name' : 'Harga Double',
      'price' : $scope.umroh.double_price
  })
 }
}
}
}

$scope.changeProvinsi = function(i) {
  var temp = $scope.var.provinsi[i].split(';');
  RequestService.getKabupaten(temp[0]).then(function(response) {
            //UNCOMMENT FOR LOCALHOST
            //response = {"error":false,"message":"Berhasil mengambil data kabupaten","daftar_kecamatan":[{"id":"1101","id_prov":"11","nama":"Kab. Simeulue"},{"id":"1102","id_prov":"11","nama":"Kab. Aceh Singkil"},{"id":"1103","id_prov":"11","nama":"Kab. Aceh Selatan"},{"id":"1104","id_prov":"11","nama":"Kab. Aceh Tenggara"},{"id":"1105","id_prov":"11","nama":"Kab. Aceh Timur"},{"id":"1106","id_prov":"11","nama":"Kab. Aceh Tengah"},{"id":"1107","id_prov":"11","nama":"Kab. Aceh Barat"},{"id":"1108","id_prov":"11","nama":"Kab. Aceh Besar"},{"id":"1109","id_prov":"11","nama":"Kab. Pidie"},{"id":"1110","id_prov":"11","nama":"Kab. Bireuen"},{"id":"1111","id_prov":"11","nama":"Kab. Aceh Utara"},{"id":"1112","id_prov":"11","nama":"Kab. Aceh Barat Daya"},{"id":"1113","id_prov":"11","nama":"Kab. Gayo Lues"},{"id":"1114","id_prov":"11","nama":"Kab. Aceh Tamiang"},{"id":"1115","id_prov":"11","nama":"Kab. Nagan Raya"},{"id":"1116","id_prov":"11","nama":"Kab. Aceh Jaya"},{"id":"1117","id_prov":"11","nama":"Kab. Bener Meriah"},{"id":"1118","id_prov":"11","nama":"Kab. Pidie Jaya"},{"id":"1171","id_prov":"11","nama":"Kota Banda Aceh"},{"id":"1172","id_prov":"11","nama":"Kota Sabang"},{"id":"1173","id_prov":"11","nama":"Kota Langsa"},{"id":"1174","id_prov":"11","nama":"Kota Lhokseumawe"},{"id":"1175","id_prov":"11","nama":"Kota Subulussalam"}]}
            
            $scope.kabupaten = response.daftar_kecamatan;
            $scope.var.kotaKabupaten[i] = $scope.kabupaten[0].id + ';' + $scope.kabupaten[0].nama;
            $scope.changeKabupaten(i);
        });
}

$scope.changeKabupaten = function(i) {
  var temp = $scope.var.kotaKabupaten[i].split(';');
  RequestService.getKecamatan(temp[0]).then(function(response) {
            //UNCOMMENT FOR LOCALHOST
            //response = {"error":false,"message":"Berhasil mengambil data kecamatan","daftar_kecamatan":[{"id":"1101010","id_kabupaten":"1101","nama":" Teupah Selatan"},{"id":"1101020","id_kabupaten":"1101","nama":" Simeulue Timur"},{"id":"1101021","id_kabupaten":"1101","nama":" Teupah Barat"},{"id":"1101022","id_kabupaten":"1101","nama":" Teupah Tengah"},{"id":"1101030","id_kabupaten":"1101","nama":" Simeulue Tengah"},{"id":"1101031","id_kabupaten":"1101","nama":" Teluk Dalam"},{"id":"1101032","id_kabupaten":"1101","nama":" Simeulue Cut"},{"id":"1101040","id_kabupaten":"1101","nama":" Salang"},{"id":"1101050","id_kabupaten":"1101","nama":" Simeulue Barat"},{"id":"1101051","id_kabupaten":"1101","nama":" Alafan"}]}
            
            $scope.kecamatan = response.daftar_kecamatan;
            $scope.var.kecamatan[i] = $scope.kecamatan[0].id + ';' + $scope.kecamatan[0].nama;
            $scope.changeKecamatan(i);
        });
}

$scope.changeKecamatan = function(i) {
  var temp = $scope.var.kecamatan[i].split(';');
  RequestService.getKelurahan(temp[0]).then(function(response) {
            //UNCOMMENT FOR LOCALHOST
            //response = {"error":false,"message":"Berhasil mengambil data desa","daftar_desa":[{"id":"1101010001","id_kecamatan":"1101010","nama":"Latiung"},{"id":"1101010002","id_kecamatan":"1101010","nama":"Labuhan Bajau"},{"id":"1101010003","id_kecamatan":"1101010","nama":"Suak Lamatan"},{"id":"1101010004","id_kecamatan":"1101010","nama":"Ana Ao"},{"id":"1101010005","id_kecamatan":"1101010","nama":"Lataling"},{"id":"1101010006","id_kecamatan":"1101010","nama":"Pulau Bengkalak"},{"id":"1101010007","id_kecamatan":"1101010","nama":"Badegong"},{"id":"1101010008","id_kecamatan":"1101010","nama":"Kebun Baru"},{"id":"1101010009","id_kecamatan":"1101010","nama":"Ulul Mayang"},{"id":"1101010010","id_kecamatan":"1101010","nama":"Pasir Tinggi"},{"id":"1101010011","id_kecamatan":"1101010","nama":"Labuhan Jaya"},{"id":"1101010012","id_kecamatan":"1101010","nama":"Labuhan Bakti"},{"id":"1101010013","id_kecamatan":"1101010","nama":"Batu Ralang"},{"id":"1101010014","id_kecamatan":"1101010","nama":"Alus Alus"},{"id":"1101010015","id_kecamatan":"1101010","nama":"Seuneubok"},{"id":"1101010016","id_kecamatan":"1101010","nama":"Blang Sebel"},{"id":"1101010017","id_kecamatan":"1101010","nama":"Trans Baru"},{"id":"1101010018","id_kecamatan":"1101010","nama":"Trans Meranti"},{"id":"1101010019","id_kecamatan":"1101010","nama":"Trans Jernge"}]}
            
            $scope.kelurahan = response.daftar_desa;
            $scope.var.kelurahan[i] = $scope.kelurahan[0].id + ';' + $scope.kelurahan[0].nama;
        });
}

$scope.modal1 = $ionicModal.fromTemplate(
  '<ion-modal-view>'
  +'<ion-header-bar class="bg-header no-padding" style="height:auto;">'
  +'<div class="row">'
  +'<div class="col col-25 text-center">'

  +'</div>'
  +'<div class="col col-50 text-center">'
  +'<span class="txt-putih bold margin-top" style="font-size:1.1rem;">Input Data Jamaah 1</span>'
  +'</div>'
  +'<div class="col col-25 text-center">'
  +'<div class="button bg-header" ng-click="modal1.hide()">'
  +'<span class="icon ion-close txt-putih"></span>'
  +'</div>'
  +'</div>'
  +'</div>'
  +'</ion-header-bar>'
  +'<ion-content style="top:10px;">'
  +'<div class="card" style="margin-top:60px;">'
  +'<div class="item item-divider">'
  +'Data Pribadi'
  +'</div>'
  +'<div class="item item-text-wrap no-padding">'
  +'<div class="list">'
  +'<label class="item item-input">'
  +'<input type="text" placeholder="Nama Lengkap" ng-model="var.namaLengkap[0]">'
  +'</label>'
  +'<label class="item item-input">'
  +'<input type="text" placeholder="Nama Ayah Kandung" ng-model="var.namaAyahKandung[0]">'
  +'</label>'
  +'<label class="item item-input">'
  +'<input type="text" placeholder="Tempat Lahir" ng-model="var.tempatLahir[0]">'
  +'</label>'
  +'<label class="item item-input item-select">'
  +'<div class="input-label">'
  +'Hubungan Dengan BNI Syariah'
  +'</div>'
  +'<select ng-model="var.hubunganDenganBNI[0]">'
  +'<option value="nasabah" ng-selected="var.hubunganDenganBNI[0] == \'nasabah\'">Nasabah</option>'
  +'<option value="keluarga nasabah" ng-selected="var.hubunganDenganBNI[0] == \'keluarga nasabah\'">Keluarga Nasabah</option>'
  +'<option value="umum" ng-selected="var.hubunganDenganBNI[0] == \'umum\'">Umum</option>'
  +'<option value="pegawai bni syariah" ng-selected="var.hubunganDenganBNI[0] == \'pegawai bni syariah\'">Pegawai BNI Syariah</option>'
  +'<option value="pegawai bni" ng-selected="var.hubunganDenganBNI[0] == \'pegawai bni\'">Pegawai BNI</option>'
  +'<option value="keluarga bni syariah" ng-selected="var.hubunganDenganBNI[0] == \'keluarga bni syariah\'">Keluarga BNI Syariah</option>'
  +'<option value="keluarga bni" ng-selected="var.hubunganDenganBNI[0] == \'keluarga bni\'">Keluarga BNI</option>'
  +'</select>'
  +'</label>'
  +'<label class="item item-input item-select">'
  +'<div class="input-label">'
  +'Ukuran Pakaian'
  +'</div>'
  +'<select ng-model="var.ukuranPakaian[0]">'
  +'<option value="xs" ng-selected="var.ukuranPakaian[0] == \'xs\'">XS</option>'
  +'<option value="s" ng-seleced="var.ukuranPakaian[0] == \'s\'">S</option>'
  +'<option value="m" ng-selected="var.ukuranPakaian[0] == \'m\'">M</option>'
  +'<option value="l" ng-selected="var.ukuranPakaian[0] == \'l\'">L</option>'
  +'<option value="xl" ng-selected="var.ukuranPakaian[0] == \'xl\'">XL</option>'
  +'<option value="xxl" ng-selected="var.ukuranPakaian[0] == \'xxl\'">XXL</option>'
  +'</select>'
  +'</label>'
  +'</div>'
  +'</div>'
  +'</div>'
  +'<div class="card">'
  +'<div class="item item-divider">'
  +'Alamat'
  +'</div>'
  +'<div class="item item-text-wrap no-padding">'
  +'<div class="list">'
  +'<label class="item item-input item-select">'
  +'<div class="input-label">'
  +'Provinsi'
  +'</div>'
  +'<select ng-model="var.provinsi[0]" ng-change="changeProvinsi(0)">'
  +'<option ng-repeat="prov in provinsi" value="{{prov.id}};{{prov.nama}}" ng-if="prov.id >= 11">{{prov.nama}}</option>'
  +'</select>'
  +'</label>'
  +'<label class="item item-input item-select">'
  +'<div class="input-label">'
  +'Kota / Kabupaten'
  +'</div>'
  +'<select ng-model="var.kotaKabupaten[0]" ng-change="changeKabupaten(0)">'
  +'<option ng-repeat="kab in kabupaten" value="{{kab.id}};{{kab.nama}}">{{kab.nama}}</option>'
  +'</select>'
  +'</label>'
  +'<label class="item item-input item-select">'
  +'<div class="input-label">'
  +'Kecamatan'
  +'</div>'
  +'<select ng-model="var.kecamatan[0]" ng-change="changeKecamatan(0)">'
  +'<option ng-repeat="kec in kecamatan" value="{{kec.id}};{{kec.nama}}">{{kec.nama}}</option>'
  +'</select>'
  +'</label>'
  +'<label class="item item-input item-select">'
  +'<div class="input-label">'
  +'Kelurahan'
  +'</div>'
  +'<select ng-model="var.kelurahan[0]">'
  +'<option ng-repeat="kel in kelurahan" value="{{kel.id}};{{kel.nama}}">{{kel.nama}}</option>'
  +'</select>'
  +'</label>'
  +'<label class="item item-input">'
  +'<input type="number" placeholder="RT" ng-model="var.rt[0]">'
  +'</label>'
  +'<label class="item item-input">'
  +'<input type="number" placeholder="RW" ng-model="var.rw[0]">'
  +'</label>'
  +'<label class="item item-input">'
  +'<input type="text" placeholder="Alamat" ng-model="var.alamat[0]">'
  +'</label>'
  +'<label class="item item-input">'
  +'<input type="number" placeholder="Nomor Telp Rumah" ng-model="var.nomorTelpRumah[0]">'
  +'</label>'
  +'</div>'
  +'</div>'
  +'</div>'
  +'<div class="card">'
  +'<div class="item item-divider">'
  +'Tgl Lahir Dan Jenis Kelamin'
  +'</div>'
  +'<div class="item item-text-wrap no-padding">'
  +'<div class="list">'
  +'<div class="item item-input-inset">'
  +'<label class="item-input-wrapper">'
  +'<input type="text" placeholder="Tanggal Lahir" disabled ng-model="var.tglLahir[0]" ng-click="openDatePickerTanggalLahir(0)">'
  +'</label>'

  +'<button class="button button-small bg-medium-orange" ng-click="openDatePickerTanggalLahir(0)">'
  +'<i class="icon ion-calendar txt-putih"></i>'
  +'</button>'
  +'</div>'
  +'<label class="item item-input item-select">'
  +'<div class="input-label">'
  +'Jenis Kelamin'
  +'</div>'
  +'<select ng-model="var.jenisKelamin[0]">'
  +'<option value="pria">Pria</option>'
  +'<option value="wanita">Wanita</option>'
  +'</select>'
  +'</label>'
  +'</div>'
  +'</div>'
  +'</div>'

  +'<div class="card" ng-if="(var.selisih[0] >=0)">'
  +'<div class="item item-divider">'
  +'Informasi Lain'
  +'</div>'
  +'<div class="item item-text-wrap no-padding">'
  +'<label class="item item-input animate-if" ng-if="var.selisih[0] >= 17">'
  +'<input type="number" placeholder="Nomor KTP" ng-model="var.nomorKTP[0]">'
  +'</label>'
  +'<label class="item item-input item-select animate-if" ng-if="(var.selisih[0] >=3 && var.selisih[0] < 17) || (var.jenisKelamin[0] == \'wanita\' && (var.selisih[0] >=17 && var.selisih[0] <=45))">'
  +'<div class="input-label">'
  +'Status Mahram'
  +'</div>'
  +'<select ng-model="var.statusMahram[0]">'
  +'<option value="suami">Suami</option>'
  +'<option value="ayah">Ayah</option>'
  +'<option value="anak">Anak</option>'
  +'<option value="saudara pria">Saudara Pria</option>'
  +'<option value="paman">Paman</option>'
  +'<option value="kakek">Kakek</option>'
  +'<option value="sendiri">Sendiri</option>'
  +'</select>'
  +'</label>'
  +'<label class="item item-input animate-if" ng-if="var.statusMahram[0] != \'sendiri\'">'
  +'<input type="text" placeholder="Nama Mahram Yang Ikut" ng-model="var.namaMahram[0]">'
  +'</label>'
  +'<label class="item item-input animate-if" ng-if="(var.selisih[0] >=0 && var.selisih[0] < 17)">'
  +'<input type="number" placeholder="Nomor Akta Kelahiran" ng-model="var.nomorAkta[0]">'
  +'</label>'
  +'<label class="item item-input animate-if" ng-if="var.selisih[0] >= 3">'
  +'<input type="number" placeholder="Nomor HP" ng-model="var.nomorHP[0]">'
  +'</label>'
  +'<label class="item item-input animate-if" ng-if="(var.selisih[0] >= 0 && var.selisih[0] < 17)">'
  +'<input type="number" placeholder="Nomor HP Orang tua" ng-model="var.nomorHPOrtu[0]">'
  +'</label>'
  +'<label class="item item-input animate-if" ng-if="var.selisih[0] >= 3">'
  +'<input type="number" placeholder="Rekening BNI Syariah" ng-model="var.RekeningBNISyariah[0]">'
  +'</label>'
  +'</div>'
  +'</div>'
  +'<div class="card">'
  +'<div class="item item-divider">'
  +'Upload Dokumen'
  +'</div>'
  +'<div class="item item-text-wrap no-padding">'
  +'<div class="list">'
  +'<div class="item item-input-inset">'
  +'<label class="item-input-wrapper">'
  +'<input type="text" placeholder="Upload Foto" disabled ng-model="var.foto[0]">'
  +'</label>'

  +'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'foto\', 0)">'
  +'<i class="icon ion-search txt-putih"></i>'
  +'</button>'
  +'</div>'
  +'<div class="item item-input-inset animate-if" ng-if="var.selisih[0] >= 17">'
  +'<label class="item-input-wrapper">'
  +'<input type="text" placeholder="Upload KTP" disabled ng-model="var.ktp[0]">'
  +'</label>'

  +'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'ktp\', 0)">'
  +'<i class="icon ion-search txt-putih"></i>'
  +'</button>'
  +'</div>'
  +'<div class="item item-input-inset animate-if" ng-if="var.selisih[0] >= 3">'
  +'<label class="item-input-wrapper">'
  +'<input type="text" placeholder="Upload Passport" disabled ng-model="var.passport[0]">'
  +'</label>'

  +'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'passport\', 0)">'
  +'<i class="icon ion-search txt-putih"></i>'
  +'</button>'
  +'</div>'
  +'<div class="item item-input-inset">'
  +'<label class="item-input-wrapper">'
  +'<input type="text" placeholder="Upload Kartu Keluarga" disabled ng-model="var.kk[0]">'
  +'</label>'

  +'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'kk\', 0)">'
  +'<i class="icon ion-search txt-putih"></i>'
  +'</button>'
  +'</div>'
  +'</div>'
  +'</div>'
  +'</div>'
  +'<div class="btn-wrapper padding">'
  +'<button class="button button-block txt-putih bg-medium-orange" ng-click="confirmDataJamaah1(0)">'
  +'Simpan'
  +'</button>'
  +'<button class="button button-block txt-putih bg-medium-orange"  ng-click="modal1.hide()">'
  +'Cancel'
  +'</button>'
  +'</div>'
  +'</ion-content>'
  +'</ion-modal-view>', {
   scope: $scope,
   animation: 'slide-in-up'
});

$scope.modal2 = $ionicModal.fromTemplate(
	'<ion-modal-view>'
	+'<ion-header-bar class="bg-header no-padding" style="height:auto;">'
	+'<div class="row">'
	+'<div class="col col-25 text-center">'

	+'</div>'
	+'<div class="col col-50 text-center">'
	+'<span class="txt-putih bold margin-top" style="font-size:1.1rem;">Input Data Jamaah 2</span>'
	+'</div>'
	+'<div class="col col-25 text-center">'
	+'<div class="button bg-header" ng-click="modal2.hide()">'
	+'<span class="icon ion-close txt-putih"></span>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'</ion-header-bar>'
	+'<ion-content style="top:10px;">'
	+'<div class="card" style="margin-top:60px;">'
	+'<div class="item item-divider">'
	+'Data Pribadi'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Nama Lengkap" ng-model="var.namaLengkap[1]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Nama Ayah Kandung" ng-model="var.namaAyahKandung[1]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Tempat Lahir" ng-model="var.tempatLahir[1]">'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Hubungan Dengan BNI Syariah'
	+'</div>'
	+'<select ng-model="var.hubunganDenganBNI[1]">'
	+'<option value="nasabah" ng-selected="var.hubunganDenganBNI[1] == \'nasabah\'">Nasabah</option>'
	+'<option value="keluarga nasabah" ng-selected="var.hubunganDenganBNI[1] == \'keluarga nasabah\'">Keluarga Nasabah</option>'
	+'<option value="umum" ng-selected="var.hubunganDenganBNI[1] == \'umum\'">Umum</option>'
	+'<option value="pegawai bni syariah" ng-selected="var.hubunganDenganBNI[1] == \'pegawai bni syariah\'">Pegawai BNI Syariah</option>'
	+'<option value="pegawai bni" ng-selected="var.hubunganDenganBNI[1] == \'pegawai bni\'">Pegawai BNI</option>'
	+'<option value="keluarga bni syariah" ng-selected="var.hubunganDenganBNI[1] == \'keluarga bni syariah\'">Keluarga BNI Syariah</option>'
	+'<option value="keluarga bni" ng-selected="var.hubunganDenganBNI[1] == \'keluarga bni\'">Keluarga BNI</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Ukuran Pakaian'
	+'</div>'
	+'<select ng-model="var.ukuranPakaian[1]">'
	+'<option value="xs" ng-selected="var.ukuranPakaian[1] == \'xs\'">XS</option>'
	+'<option value="s" ng-seleced="var.ukuranPakaian[1] == \'s\'">S</option>'
	+'<option value="m" ng-selected="var.ukuranPakaian[1] == \'m\'">M</option>'
	+'<option value="l" ng-selected="var.ukuranPakaian[1] == \'l\'">L</option>'
	+'<option value="xl" ng-selected="var.ukuranPakaian[1] == \'xl\'">XL</option>'
	+'<option value="xxl" ng-selected="var.ukuranPakaian[1] == \'xxl\'">XXL</option>'
	+'</select>'
	+'</label>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'<div class="card">'
	+'<div class="item item-divider">'
	+'Alamat'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Provinsi'
	+'</div>'
	+'<select ng-model="var.provinsi[1]" ng-change="changeProvinsi(1)">'
	+'<option ng-repeat="prov in provinsi" value="{{prov.id}};{{prov.nama}}" ng-if="prov.id >= 11">{{prov.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Kota / Kabupaten'
	+'</div>'
	+'<select ng-model="var.kotaKabupaten[1]" ng-change="changeKabupaten(1)">'
	+'<option ng-repeat="kab in kabupaten" value="{{kab.id}};{{kab.nama}}">{{kab.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Kecamatan'
	+'</div>'
	+'<select ng-model="var.kecamatan[1]" ng-change="changeKecamatan(1)">'
	+'<option ng-repeat="kec in kecamatan" value="{{kec.id}};{{kec.nama}}">{{kec.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Kelurahan'
	+'</div>'
	+'<select ng-model="var.kelurahan[1]">'
	+'<option ng-repeat="kel in kelurahan" value="{{kel.id}};{{kel.nama}}">{{kel.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="number" placeholder="RT" ng-model="var.rt[1]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="number" placeholder="RW" ng-model="var.rw[1]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Alamat" ng-model="var.alamat[1]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="number" placeholder="Nomor Telp Rumah" ng-model="var.nomorTelpRumah[1]">'
	+'</label>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'<div class="card">'
	+'<div class="item item-divider">'
	+'Tgl Lahir Dan Jenis Kelamin'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<div class="item item-input-inset">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Tanggal Lahir" disabled ng-model="var.tglLahir[1]" ng-click="openDatePickerTanggalLahir(1)">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="openDatePickerTanggalLahir(1)">'
	+'<i class="icon ion-calendar txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Jenis Kelamin'
	+'</div>'
	+'<select ng-model="var.jenisKelamin[1]">'
	+'<option value="pria">Pria</option>'
	+'<option value="wanita">Wanita</option>'
	+'</select>'
	+'</label>'
	+'</div>'
	+'</div>'
	+'</div>'

	+'<div class="card" ng-if="(var.selisih[1] >=0)">'
	+'<div class="item item-divider">'
	+'Informasi Lain'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<label class="item item-input animate-if" ng-if="var.selisih[1] >= 17">'
	+'<input type="number" placeholder="Nomor KTP" ng-model="var.nomorKTP[1]">'
	+'</label>'
	+'<label class="item item-input item-select animate-if" ng-if="(var.selisih[1] >=3 && var.selisih[1] < 17) || (var.jenisKelamin[1] == \'wanita\' && (var.selisih[1] >=17 && var.selisih[1] <=45))">'
	+'<div class="input-label">'
	+'Status Mahram'
	+'</div>'
	+'<select ng-model="var.statusMahram[1]">'
	+'<option value="suami">Suami</option>'
	+'<option value="ayah">Ayah</option>'
	+'<option value="anak">Anak</option>'
	+'<option value="saudara pria">Saudara Pria</option>'
	+'<option value="paman">Paman</option>'
	+'<option value="kakek">Kakek</option>'
	+'<option value="sendiri">Sendiri</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="var.statusMahram[1] != \'sendiri\'">'
	+'<input type="text" placeholder="Nama Mahram Yang Ikut" ng-model="var.namaMahram[1]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="(var.selisih[1] >=0 && var.selisih[1] < 17)">'
	+'<input type="number" placeholder="Nomor Akta Kelahiran" ng-model="var.nomorAkta[1]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="var.selisih[1] >= 3">'
	+'<input type="number" placeholder="Nomor HP" ng-model="var.nomorHP[1]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="(var.selisih[1] >= 0 && var.selisih[1] < 17)">'
	+'<input type="number" placeholder="Nomor HP Orang tua" ng-model="var.nomorHPOrtu[1]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="var.selisih[1] >= 3">'
	+'<input type="number" placeholder="Rekening BNI Syariah" ng-model="var.RekeningBNISyariah[1]">'
	+'</label>'
	+'</div>'
	+'</div>'
	+'<div class="card">'
	+'<div class="item item-divider">'
	+'Upload Dokumen'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<div class="item item-input-inset">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload Foto" disabled ng-model="var.foto[1]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'foto\', 1)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<div class="item item-input-inset animate-if" ng-if="var.selisih[1] >= 17">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload KTP" disabled ng-model="var.ktp[1]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'ktp\', 1)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<div class="item item-input-inset animate-if" ng-if="var.selisih[1] >= 3">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload Passport" disabled ng-model="var.passport[1]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'passport\', 1)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<div class="item item-input-inset">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload Kartu Keluarga" disabled ng-model="var.kk[1]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'kk\', 1)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'<div class="btn-wrapper padding">'
	+'<button class="button button-block txt-putih bg-medium-orange" ng-click="confirmDataJamaah1(1)">'
	+'Simpan'
	+'</button>'
	+'<button class="button button-block txt-putih bg-medium-orange"  ng-click="modal2.hide()">'
	+'Cancel'
	+'</button>'
	+'</div>'
	+'</ion-content>'
	+'</ion-modal-view>'

	, {
		scope: $scope,
		animation: 'slide-in-up'
	});


$scope.modal3 = $ionicModal.fromTemplate(
	'<ion-modal-view>'
	+'<ion-header-bar class="bg-header no-padding" style="height:auto;">'
	+'<div class="row">'
	+'<div class="col col-25 text-center">'

	+'</div>'
	+'<div class="col col-50 text-center">'
	+'<span class="txt-putih bold margin-top" style="font-size:1.1rem;">Input Data Jamaah 3</span>'
	+'</div>'
	+'<div class="col col-25 text-center">'
	+'<div class="button bg-header" ng-click="modal3.hide()">'
	+'<span class="icon ion-close txt-putih"></span>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'</ion-header-bar>'
	+'<ion-content style="top:10px;">'
	+'<div class="card" style="margin-top:60px;">'
	+'<div class="item item-divider">'
	+'Data Pribadi'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Nama Lengkap" ng-model="var.namaLengkap[2]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Nama Ayah Kandung" ng-model="var.namaAyahKandung[2]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Tempat Lahir" ng-model="var.tempatLahir[2]">'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Hubungan Dengan BNI Syariah'
	+'</div>'
	+'<select ng-model="var.hubunganDenganBNI[2]">'
	+'<option value="nasabah" ng-selected="var.hubunganDenganBNI[2] == \'nasabah\'">Nasabah</option>'
	+'<option value="keluarga nasabah" ng-selected="var.hubunganDenganBNI[2] == \'keluarga nasabah\'">Keluarga Nasabah</option>'
	+'<option value="umum" ng-selected="var.hubunganDenganBNI[2] == \'umum\'">Umum</option>'
	+'<option value="pegawai bni syariah" ng-selected="var.hubunganDenganBNI[2] == \'pegawai bni syariah\'">Pegawai BNI Syariah</option>'
	+'<option value="pegawai bni" ng-selected="var.hubunganDenganBNI[2] == \'pegawai bni\'">Pegawai BNI</option>'
	+'<option value="keluarga bni syariah" ng-selected="var.hubunganDenganBNI[2] == \'keluarga bni syariah\'">Keluarga BNI Syariah</option>'
	+'<option value="keluarga bni" ng-selected="var.hubunganDenganBNI[2] == \'keluarga bni\'">Keluarga BNI</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Ukuran Pakaian'
	+'</div>'
	+'<select ng-model="var.ukuranPakaian[2]">'
	+'<option value="xs" ng-selected="var.ukuranPakaian[2] == \'xs\'">XS</option>'
	+'<option value="s" ng-seleced="var.ukuranPakaian[2] == \'s\'">S</option>'
	+'<option value="m" ng-selected="var.ukuranPakaian[2] == \'m\'">M</option>'
	+'<option value="l" ng-selected="var.ukuranPakaian[2] == \'l\'">L</option>'
	+'<option value="xl" ng-selected="var.ukuranPakaian[2] == \'xl\'">XL</option>'
	+'<option value="xxl" ng-selected="var.ukuranPakaian[2] == \'xxl\'">XXL</option>'
	+'</select>'
	+'</label>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'<div class="card">'
	+'<div class="item item-divider">'
	+'Alamat'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Provinsi'
	+'</div>'
	+'<select ng-model="var.provinsi[2]" ng-change="changeProvinsi(2)">'
	+'<option ng-repeat="prov in provinsi" value="{{prov.id}};{{prov.nama}}" ng-if="prov.id >= 11">{{prov.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Kota / Kabupaten'
	+'</div>'
	+'<select ng-model="var.kotaKabupaten[2]" ng-change="changeKabupaten(2)">'
	+'<option ng-repeat="kab in kabupaten" value="{{kab.id}};{{kab.nama}}">{{kab.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Kecamatan'
	+'</div>'
	+'<select ng-model="var.kecamatan[2]" ng-change="changeKecamatan(2)">'
	+'<option ng-repeat="kec in kecamatan" value="{{kec.id}};{{kec.nama}}">{{kec.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Kelurahan'
	+'</div>'
	+'<select ng-model="var.kelurahan[2]">'
	+'<option ng-repeat="kel in kelurahan" value="{{kel.id}};{{kel.nama}}">{{kel.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="number" placeholder="RT" ng-model="var.rt[2]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="number" placeholder="RW" ng-model="var.rw[2]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Alamat" ng-model="var.alamat[2]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="number" placeholder="Nomor Telp Rumah" ng-model="var.nomorTelpRumah[2]">'
	+'</label>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'<div class="card">'
	+'<div class="item item-divider">'
	+'Tgl Lahir Dan Jenis Kelamin'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<div class="item item-input-inset">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Tanggal Lahir" disabled ng-model="var.tglLahir[2]" ng-click="openDatePickerTanggalLahir(2)">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="openDatePickerTanggalLahir(2)">'
	+'<i class="icon ion-calendar txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Jenis Kelamin'
	+'</div>'
	+'<select ng-model="var.jenisKelamin[2]">'
	+'<option value="pria">Pria</option>'
	+'<option value="wanita">Wanita</option>'
	+'</select>'
	+'</label>'
	+'</div>'
	+'</div>'
	+'</div>'

	+'<div class="card" ng-if="(var.selisih[2] >=0)">'
	+'<div class="item item-divider">'
	+'Informasi Lain'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<label class="item item-input animate-if" ng-if="var.selisih[2] >= 17">'
	+'<input type="number" placeholder="Nomor KTP" ng-model="var.nomorKTP[2]">'
	+'</label>'
	+'<label class="item item-input item-select animate-if" ng-if="(var.selisih[2] >=3 && var.selisih[2] < 17) || (var.jenisKelamin[2] == \'wanita\' && (var.selisih[2] >=17 && var.selisih[2] <=45))">'
	+'<div class="input-label">'
	+'Status Mahram'
	+'</div>'
	+'<select ng-model="var.statusMahram[2]">'
	+'<option value="suami">Suami</option>'
	+'<option value="ayah">Ayah</option>'
	+'<option value="anak">Anak</option>'
	+'<option value="saudara pria">Saudara Pria</option>'
	+'<option value="paman">Paman</option>'
	+'<option value="kakek">Kakek</option>'
	+'<option value="sendiri">Sendiri</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="var.statusMahram[2] != \'sendiri\'">'
	+'<input type="text" placeholder="Nama Mahram Yang Ikut" ng-model="var.namaMahram[2]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="(var.selisih[2] >=0 && var.selisih[2] < 17)">'
	+'<input type="number" placeholder="Nomor Akta Kelahiran" ng-model="var.nomorAkta[2]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="var.selisih[2] >= 3">'
	+'<input type="number" placeholder="Nomor HP" ng-model="var.nomorHP[2]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="(var.selisih[2] >= 0 && var.selisih[2] < 17)">'
	+'<input type="number" placeholder="Nomor HP Orang tua" ng-model="var.nomorHPOrtu[2]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="var.selisih[2] >= 3">'
	+'<input type="number" placeholder="Rekening BNI Syariah" ng-model="var.RekeningBNISyariah[2]">'
	+'</label>'
	+'</div>'
	+'</div>'
	+'<div class="card">'
	+'<div class="item item-divider">'
	+'Upload Dokumen'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<div class="item item-input-inset">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload Foto" disabled ng-model="var.foto[2]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'foto\', 2)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<div class="item item-input-inset animate-if" ng-if="var.selisih[2] >= 17">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload KTP" disabled ng-model="var.ktp[2]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'ktp\', 2)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<div class="item item-input-inset animate-if" ng-if="var.selisih[2] >= 3">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload Passport" disabled ng-model="var.passport[2]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'passport\', 2)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<div class="item item-input-inset">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload Kartu Keluarga" disabled ng-model="var.kk[2]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'kk\', 2)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'<div class="btn-wrapper padding">'
	+'<button class="button button-block txt-putih bg-medium-orange" ng-click="confirmDataJamaah1(2)">'
	+'Simpan'
	+'</button>'
	+'<button class="button button-block txt-putih bg-medium-orange"  ng-click="modal3.hide()">'
	+'Cancel'
	+'</button>'
	+'</div>'
	+'</ion-content>'
	+'</ion-modal-view>'

	, {
		scope: $scope,
		animation: 'slide-in-up'
	});


$scope.modal4 = $ionicModal.fromTemplate(
	'<ion-modal-view>'
	+'<ion-header-bar class="bg-header no-padding" style="height:auto;">'
	+'<div class="row">'
	+'<div class="col col-25 text-center">'

	+'</div>'
	+'<div class="col col-50 text-center">'
	+'<span class="txt-putih bold margin-top" style="font-size:1.1rem;">Input Data Jamaah 4</span>'
	+'</div>'
	+'<div class="col col-25 text-center">'
	+'<div class="button bg-header" ng-click="modal4.hide()">'
	+'<span class="icon ion-close txt-putih"></span>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'</ion-header-bar>'
	+'<ion-content style="top:10px;">'
	+'<div class="card" style="margin-top:60px;">'
	+'<div class="item item-divider">'
	+'Data Pribadi'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Nama Lengkap" ng-model="var.namaLengkap[3]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Nama Ayah Kandung" ng-model="var.namaAyahKandung[3]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Tempat Lahir" ng-model="var.tempatLahir[3]">'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Hubungan Dengan BNI Syariah'
	+'</div>'
	+'<select ng-model="var.hubunganDenganBNI[3]">'
	+'<option value="nasabah" ng-selected="var.hubunganDenganBNI[3] == \'nasabah\'">Nasabah</option>'
	+'<option value="keluarga nasabah" ng-selected="var.hubunganDenganBNI[3] == \'keluarga nasabah\'">Keluarga Nasabah</option>'
	+'<option value="umum" ng-selected="var.hubunganDenganBNI[3] == \'umum\'">Umum</option>'
	+'<option value="pegawai bni syariah" ng-selected="var.hubunganDenganBNI[3] == \'pegawai bni syariah\'">Pegawai BNI Syariah</option>'
	+'<option value="pegawai bni" ng-selected="var.hubunganDenganBNI[3] == \'pegawai bni\'">Pegawai BNI</option>'
	+'<option value="keluarga bni syariah" ng-selected="var.hubunganDenganBNI[3] == \'keluarga bni syariah\'">Keluarga BNI Syariah</option>'
	+'<option value="keluarga bni" ng-selected="var.hubunganDenganBNI[3] == \'keluarga bni\'">Keluarga BNI</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Ukuran Pakaian'
	+'</div>'
	+'<select ng-model="var.ukuranPakaian[3]">'
	+'<option value="xs" ng-selected="var.ukuranPakaian[3] == \'xs\'">XS</option>'
	+'<option value="s" ng-seleced="var.ukuranPakaian[3] == \'s\'">S</option>'
	+'<option value="m" ng-selected="var.ukuranPakaian[3] == \'m\'">M</option>'
	+'<option value="l" ng-selected="var.ukuranPakaian[3] == \'l\'">L</option>'
	+'<option value="xl" ng-selected="var.ukuranPakaian[3] == \'xl\'">XL</option>'
	+'<option value="xxl" ng-selected="var.ukuranPakaian[3] == \'xxl\'">XXL</option>'
	+'</select>'
	+'</label>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'<div class="card">'
	+'<div class="item item-divider">'
	+'Alamat'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Provinsi'
	+'</div>'
	+'<select ng-model="var.provinsi[3]" ng-change="changeProvinsi(3)">'
	+'<option ng-repeat="prov in provinsi" value="{{prov.id}};{{prov.nama}}" ng-if="prov.id >= 11">{{prov.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Kota / Kabupaten'
	+'</div>'
	+'<select ng-model="var.kotaKabupaten[3]" ng-change="changeKabupaten(3)">'
	+'<option ng-repeat="kab in kabupaten" value="{{kab.id}};{{kab.nama}}">{{kab.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Kecamatan'
	+'</div>'
	+'<select ng-model="var.kecamatan[3]" ng-change="changeKecamatan(3)">'
	+'<option ng-repeat="kec in kecamatan" value="{{kec.id}};{{kec.nama}}">{{kec.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Kelurahan'
	+'</div>'
	+'<select ng-model="var.kelurahan[3]">'
	+'<option ng-repeat="kel in kelurahan" value="{{kel.id}};{{kel.nama}}">{{kel.nama}}</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="number" placeholder="RT" ng-model="var.rt[3]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="number" placeholder="RW" ng-model="var.rw[3]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="text" placeholder="Alamat" ng-model="var.alamat[3]">'
	+'</label>'
	+'<label class="item item-input">'
	+'<input type="number" placeholder="Nomor Telp Rumah" ng-model="var.nomorTelpRumah[3]">'
	+'</label>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'<div class="card">'
	+'<div class="item item-divider">'
	+'Tgl Lahir Dan Jenis Kelamin'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<div class="item item-input-inset">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Tanggal Lahir" disabled ng-model="var.tglLahir[3]" ng-click="openDatePickerTanggalLahir(3)">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="openDatePickerTanggalLahir(3)">'
	+'<i class="icon ion-calendar txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<label class="item item-input item-select">'
	+'<div class="input-label">'
	+'Jenis Kelamin'
	+'</div>'
	+'<select ng-model="var.jenisKelamin[3]">'
	+'<option value="pria">Pria</option>'
	+'<option value="wanita">Wanita</option>'
	+'</select>'
	+'</label>'
	+'</div>'
	+'</div>'
	+'</div>'

	+'<div class="card" ng-if="(var.selisih[3] >=0)">'
	+'<div class="item item-divider">'
	+'Informasi Lain'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<label class="item item-input animate-if" ng-if="var.selisih[3] >= 17">'
	+'<input type="number" placeholder="Nomor KTP" ng-model="var.nomorKTP[2]">'
	+'</label>'
	+'<label class="item item-input item-select animate-if" ng-if="(var.selisih[3] >=3 && var.selisih[3] < 17) || (var.jenisKelamin[3] == \'wanita\' && (var.selisih[3] >=17 && var.selisih[3] <=45))">'
	+'<div class="input-label">'
	+'Status Mahram'
	+'</div>'
	+'<select ng-model="var.statusMahram[3]">'
	+'<option value="suami">Suami</option>'
	+'<option value="ayah">Ayah</option>'
	+'<option value="anak">Anak</option>'
	+'<option value="saudara pria">Saudara Pria</option>'
	+'<option value="paman">Paman</option>'
	+'<option value="kakek">Kakek</option>'
	+'<option value="sendiri">Sendiri</option>'
	+'</select>'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="var.statusMahram[3] != \'sendiri\'">'
	+'<input type="text" placeholder="Nama Mahram Yang Ikut" ng-model="var.namaMahram[3]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="(var.selisih[3] >=0 && var.selisih[3] < 17)">'
	+'<input type="number" placeholder="Nomor Akta Kelahiran" ng-model="var.nomorAkta[3]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="var.selisih[3] >= 3">'
	+'<input type="number" placeholder="Nomor HP" ng-model="var.nomorHP[3]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="(var.selisih[2] >= 0 && var.selisih[3] < 17)">'
	+'<input type="number" placeholder="Nomor HP Orang tua" ng-model="var.nomorHPOrtu[3]">'
	+'</label>'
	+'<label class="item item-input animate-if" ng-if="var.selisih[3] >= 3">'
	+'<input type="number" placeholder="Rekening BNI Syariah" ng-model="var.RekeningBNISyariah[3]">'
	+'</label>'
	+'</div>'
	+'</div>'
	+'<div class="card">'
	+'<div class="item item-divider">'
	+'Upload Dokumen'
	+'</div>'
	+'<div class="item item-text-wrap no-padding">'
	+'<div class="list">'
	+'<div class="item item-input-inset">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload Foto" disabled ng-model="var.foto[3]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'foto\', 3)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<div class="item item-input-inset animate-if" ng-if="var.selisih[3] >= 17">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload KTP" disabled ng-model="var.ktp[3]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'ktp\', 3)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<div class="item item-input-inset animate-if" ng-if="var.selisih[3] >= 3">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload Passport" disabled ng-model="var.passport[3]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'passport\', 3)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'<div class="item item-input-inset">'
	+'<label class="item-input-wrapper">'
	+'<input type="text" placeholder="Upload Kartu Keluarga" disabled ng-model="var.kk[3]">'
	+'</label>'

	+'<button class="button button-small bg-medium-orange" ng-click="loadFile(\'kk\', 3)">'
	+'<i class="icon ion-search txt-putih"></i>'
	+'</button>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'</div>'
	+'<div class="btn-wrapper padding">'
	+'<button class="button button-block txt-putih bg-medium-orange" ng-click="confirmDataJamaah1(3)">'
	+'Simpan'
	+'</button>'
	+'<button class="button button-block txt-putih bg-medium-orange"  ng-click="modal4.hide()">'
	+'Cancel'
	+'</button>'
	+'</div>'
	+'</ion-content>'
	+'</ion-modal-view>'

	, {
		scope: $scope,
		animation: 'slide-in-up'
	});




$scope.confirmDataJamaah1 = function(i) {
	var province, regency, district, village;

	if ($scope.var.selisih[i] >= 0 && $scope.var.selisih[i] < 3)
	{
		if($scope.var.namaLengkap[i] && $scope.var.namaAyahKandung[i] && $scope.var.tempatLahir[i] && 
			$scope.var.rt[i] && $scope.var.rw[i] && $scope.var.alamat[i] && $scope.var.nomorTelpRumah[i] &&
			$scope.var.nomorAkta[i] && $scope.var.nomorHPOrtu[i] && $scope.var.foto[i] && $scope.var.kk[i] && 
			$scope.var.provinsi[i] && $scope.var.kotaKabupaten[i] && $scope.var.kecamatan[i] && $scope.var.kelurahan[i])
		{
			province = $scope.var.provinsi[i].split(';');
			regency = $scope.var.kotaKabupaten[i].split(';');
			district = $scope.var.kecamatan[i].split(';');
			village = $scope.var.kelurahan[i].split(';');

			$scope.var.passengger[i] = {
				'full_name' : $scope.var.namaLengkap[i],
				'father_name' : $scope.var.namaAyahKandung[i],
				'pob' : $scope.var.tempatLahir[i],
				'bnis_relation' : $scope.var.hubunganDenganBNI[i],
				"cloth_size": $scope.var.ukuranPakaian[i],
				"province": province[1],
				"regency": regency[1],
				"district": district[1],
				"village": village[1],
				"rt": $scope.var.rt[i],
				"rw": $scope.var.rw[i],
				"address": $scope.var.alamat[i],
				"phone": $scope.var.nomorTelpRumah[i],
				"dob": $scope.var.tglLahir[i],
				"gender": $scope.var.jenisKelamin[i],
				"ktp_no": null,
				"mahram_status": null,
				"mahram_name": null,
				"acta_no": $scope.var.nomorAkta[i],
				"hp": null,
				"parent_hp": $scope.var.nomorHPOrtu[i],
				"bnis_rek": null,
				"img":$scope.var.foto[i],
				"ktp_img": null,
				"passport_img": null,
				"kk_img":$scope.var.kk[i],
				"selisih_date" : $scope.var.selisih[i]
			}
			if(i == 0) {
				$scope.modal1.hide();
			} else if (i == 1) {
				$scope.modal2.hide();
			} else if (i == 2) {
				$scope.modal3.hide();
			} else if (i == 3) {
				$scope.modal4.hide();
			}
		}
		else
		{   
			$scope.showAlert2('Error', 'Data Masih Belum Lengkap');
		}
	}
	else if ($scope.var.selisih[i] >= 3 && $scope.var.selisih[i] < 17) {
		if($scope.var.namaLengkap[i] && $scope.var.namaAyahKandung[i] && $scope.var.tempatLahir && $scope.var.rt[i] && $scope.var.rw[i] && $scope.var.rw[i] && $scope.var.alamat[i] && $scope.var.nomorTelpRumah[i] && $scope.var.nomorAkta[i] && $scope.var.nomorHP[i] && $scope.var.nomorHPOrtu && $scope.var.foto[i] && $scope.var.passport[i] && $scope.var.kk[i] && $scope.var.provinsi[i] && $scope.var.kotaKabupaten[i] && $scope.var.kecamatan[i] && $scope.var.kelurahan[i]) {
			if ($scope.var.statusMahram[i] != 'sendiri' && !$scope.var.namaMahram[i]) {
				$scope.showAlert2('Error', 'Data Masih Belum Lengkap');
			} else {
				province = $scope.var.provinsi[i].split(';');
				regency = $scope.var.kotaKabupaten[i].split(';');
				district = $scope.var.kecamatan[i].split(';');
				village = $scope.var.kelurahan[i].split(';');

				$scope.var.passengger[i] = {
					'full_name' : $scope.var.namaLengkap[i],
					'father_name' : $scope.var.namaAyahKandung[i],
					'pob' : $scope.var.tempatLahir[i],
					'bnis_relation' : $scope.var.hubunganDenganBNI[i],
					"cloth_size": $scope.var.ukuranPakaian[i],
					"province": province[1],
					"regency": regency[1],
					"district": district[1],
					"village": village[1],
					"rt": $scope.var.rt[i],
					"rw": $scope.var.rw[i],
					"address": $scope.var.alamat[i],
					"phone": $scope.var.nomorTelpRumah[i],
					"dob": $scope.var.tglLahir[i],
					"gender": $scope.var.jenisKelamin[i],
					"ktp_no": null,
					"mahram_status": $scope.var.statusMahram[i],
					"mahram_name":$scope.var.namaMahram[i],
					"acta_no": $scope.var.nomorAkta[i],
					"hp": $scope.var.nomorHP[i],
					"parent_hp": $scope.var.nomorHPOrtu[i],
					"bnis_rek":$scope.var.RekeningBNISyariah[i],
					"img":$scope.var.foto[i],
					"ktp_img": null,
					"passport_img": null,
					"kk_img":$scope.var.kk[i],
					"selisih_date" : $scope.var.selisih[i]
				}
				if(i == 0) {
					$scope.modal1.hide();
				} else if (i == 1) {
					$scope.modal2.hide();
				} else if (i == 2) {
					$scope.modal3.hide();
				} else if (i == 3) {
					$scope.modal4.hide();
				}
			}
		} else {
			$scope.showAlert2('Error', 'Data Masih Belum Lengkap');
		}
	} else if (($scope.var.selisih[i] >= 17 && $scope.var.selisih[i] < 45) && $scope.var.jenisKelamin[i] == 'wanita') {
		if($scope.var.namaLengkap[i] && $scope.var.namaAyahKandung[i] && $scope.var.tempatLahir && $scope.var.rt[i] && $scope.var.rw[i] && $scope.var.rw[i] && $scope.var.alamat[i] && $scope.var.nomorTelpRumah[i] && $scope.var.nomorKTP[i] && $scope.var.nomorHP[i]  && $scope.var.foto[i] && $scope.var.passport[i] && $scope.var.kk[i] && $scope.var.ktp[i] && $scope.var.provinsi[i] && $scope.var.kotaKabupaten[i] && $scope.var.kecamatan[i] && $scope.var.kelurahan[i]) {
			if ($scope.var.statusMahram[i] != 'sendiri' && !$scope.var.namaMahram[i]) {
				$scope.showAlert2('Error', 'Data Masih Belum Lengkap');
			} else {
				province = $scope.var.provinsi[i].split(';');
				regency = $scope.var.kotaKabupaten[i].split(';');
				district = $scope.var.kecamatan[i].split(';');
				village = $scope.var.kelurahan[i].split(';');

				$scope.var.passengger[i] = {
					'full_name' : $scope.var.namaLengkap[i],
					'father_name' : $scope.var.namaAyahKandung[i],
					'pob' : $scope.var.tempatLahir[i],
					'bnis_relation' : $scope.var.hubunganDenganBNI[i],
					"cloth_size": $scope.var.ukuranPakaian[i],
					"province": province[1],
					"regency": regency[1],
					"district": district[1],
					"village": village[1],
					"rt": $scope.var.rt[i],
					"rw": $scope.var.rw[i],
					"address": $scope.var.alamat[i],
					"phone": $scope.var.nomorTelpRumah[i],
					"dob": $scope.var.tglLahir[i],
					"gender": $scope.var.jenisKelamin[i],
					"ktp_no": $scope.var.nomorKTP[i],
					"mahram_status": $scope.var.statusMahram[i],
					"mahram_name":$scope.var.namaMahram[i],
					"acta_no": null,
					"hp": $scope.var.nomorHP[i],
					"parent_hp": null,
					"bnis_rek":$scope.var.RekeningBNISyariah[i],
					"img":$scope.var.foto[i],
					"ktp_img": $scope.var.ktp[i],
					"passport_img": $scope.var.passport[i],
					"kk_img":$scope.var.kk[i],
					"selisih_date" : $scope.var.selisih[i]
				}
				if(i == 0) {
					$scope.modal1.hide();
				} else if (i == 1) {
					$scope.modal2.hide();
				} else if (i == 2) {
					$scope.modal3.hide();
				} else if (i == 3) {
					$scope.modal4.hide();
				}
			}
		} else {
			$scope.showAlert2('Error', 'Data Masih Belum Lengkap');
		}
	} else if (($scope.var.selisih[i] >= 17 && $scope.var.jenisKelamin[i] == 'pria') ||  ($scope.var.selisih[i] >= 45 && $scope.var.jenisKelamin[i] == 'wanita')) {
		if($scope.var.namaLengkap[i] && $scope.var.namaAyahKandung[i] && $scope.var.tempatLahir && $scope.var.rt[i] && $scope.var.rw[i] && $scope.var.rw[i] && $scope.var.alamat[i] && $scope.var.nomorTelpRumah[i] && $scope.var.nomorKTP[i] && $scope.var.nomorHP[i]  && $scope.var.foto[i] && $scope.var.passport[i] && $scope.var.kk[i] && $scope.var.ktp[i] && $scope.var.provinsi[i] && $scope.var.kotaKabupaten[i] && $scope.var.kecamatan[i] && $scope.var.kelurahan[i]) {
			province = $scope.var.provinsi[i].split(';');
			regency = $scope.var.kotaKabupaten[i].split(';');
			district = $scope.var.kecamatan[i].split(';');
			village = $scope.var.kelurahan[i].split(';');

			$scope.var.passengger[i] = {
				'full_name' : $scope.var.namaLengkap[i],
				'father_name' : $scope.var.namaAyahKandung[i],
				'pob' : $scope.var.tempatLahir[i],
				'bnis_relation' : $scope.var.hubunganDenganBNI[i],
				"cloth_size": $scope.var.ukuranPakaian[i],
				"province": province[1],
				"regency": regency[1],
				"district": district[1],
				"village": village[1],
				"rt": $scope.var.rt[i],
				"rw": $scope.var.rw[i],
				"address": $scope.var.alamat[i],
				"phone": $scope.var.nomorTelpRumah[i],
				"dob": $scope.var.tglLahir[i],
				"gender": $scope.var.jenisKelamin[i],
				"ktp_no": $scope.var.nomorKTP[i],
				"mahram_status": null,
				"mahram_name":null,
				"acta_no": null,
				"hp": $scope.var.nomorHP[i],
				"parent_hp": null,
				"bnis_rek":$scope.var.RekeningBNISyariah[i],
				"img":$scope.var.foto[i],
				"ktp_img": $scope.var.ktp[i],
				"passport_img": $scope.var.passport[i],
				"kk_img":$scope.var.kk[i],
				"selisih_date" : $scope.var.selisih[i]
			}
			if(i == 0) {
				$scope.modal1.hide();
			} else if (i == 1) {
				$scope.modal2.hide();
			} else if (i == 2) {
				$scope.modal3.hide();
			} else if (i == 3) {
				$scope.modal4.hide();
			}
		} else {
			$scope.showAlert2('Error', 'Data Masih Belum Lengkap');
		}
	} 

    console.log(JSON.stringify($scope.var.passengger[i]));
}


$scope.openDatePickerTanggalLahir = function(i){
	var selisih;
	var a, b;
	var dpObj = {
		callback: function (val) {
			var date = new Date(val);
			$scope.var.tglLahir[i] = moment(val).format('DD MMMM YYYY');
			a = moment($scope.var.tglKeberangkatan);
			b = moment(val);
			$scope.var.selisih[i] = a.diff(b, 'years', true);
                //$scope.filing.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
            },
            from: new Date(1920, 01, 01),
            to: new Date(2020, 01, 01),
            mondayFirst: true,
            closeOnSelect: false,
            templateType: 'popup'
        };

        ionicDatePicker.openDatePicker(dpObj);
    };

    $scope.loadFile = function(jenis, i) {
    	var titleText;
    	if(jenis == 'foto') {

    	} else if (jenis == 'ktp') {

    	} else if (jenis == 'passport') {

    	} else {

    	}

    	var options = {
    		title: 'Pilih Sumber File',
    		buttonLabels: ['Ambil Dari Gallery', 'Menggunakan Kamera'],
    		addCancelButtonWithLabel: 'Cancel',
    		androidEnableCancelButton : true,
    	};
    	$cordovaActionSheet.show(options).then(function(btnIndex) {
    		var type = null;
    		if (btnIndex === 1) {
                type = Camera.PictureSourceType.PHOTOLIBRARY;
            } else if (btnIndex === 2) {
                type = Camera.PictureSourceType.CAMERA;
            }
            if (type !== null) {
                $scope.selectPicture(type, jenis, i);
            }
        });
    };


    $scope.selectPicture = function(sourceType, jenis, i) {
        var options = {
          quality: 100,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: sourceType,
          saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imagePath) {
            // Grab the file name of the photo in the temporary directory
            var currentName = imagePath.replace(/^.*[\\\/]/, '');
            
            //Create a new name for the photo
            var d = new Date(),
            n = d.getTime(),
            // newFileName =  n + "tes.jpg";
            newFileName = $scope.user.id + '_' + jenis +'.jpg';

            // If you are trying to load image from the gallery on Android we need special treatment!
            if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
                window.FilePath.resolveNativePath(imagePath, function(entry) {
                  window.resolveLocalFileSystemURL(entry, success, fail);
                  function fail(e) {
                   console.error('Error: ', e);
               }

               function success(fileEntry) {
                   var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                    // Only copy because of access rights
                    $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                    	$scope.image = newFileName;
                        setTimeout(function(){
                          $scope.uploadImage(jenis, i);
                      }, 800);
                    }, function(error){
                    	$scope.showAlert2('Error', error.exception);
                    });
                };
            }
            );
            } else {
                var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                // Move the file to permanent storage
                $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
                	$scope.image = newFileName;
                    setTimeout(function(){
                      $scope.uploadImage(jenis, i);
                  }, 800);
                }, function(error){
                	$scope.showAlert2('Error', error.exception);
                });
            }
        },
        function(err){
            // Not always an error, maybe cancel was pressed...
        })
  };

  $scope.pathForImage = function(image) {
     if (image === null) {
      return '';
  } else {
      return cordova.file.dataDirectory + image;
  }
};


$scope.uploadImage = function(jenis, i) {
        // Destination URL
        // File for Upload
        var file = '';
        var targetPath = $scope.pathForImage($scope.image);
        // File name only
        var filename = $scope.image;
        
        var options = {
        	fileKey: "file",
        	fileName: filename,
        	chunkedMode: false,
        	mimeType: "multipart/form-data",
        	params : {'fileName': filename}
        };

        if(jenis == 'foto') {
        	file = $scope.var.foto[i];
        } else if (jenis == 'ktp') {
        	file = $scope.var.ktp[i];
        } else if (jenis == 'passport') {
        	file = $scope.var.passport[i];
        } else {
        	file = $scope.var.kk[i];
        }

        var data = {
        	'name':file
        }
        $ionicLoading.show({
        	template: 'Uploading ...'
        });
        
        RequestService.deleteImage(data).then(function(response) {
        	$cordovaFileTransfer.upload($scope.var.uri + 'upload/uploadimg/', targetPath, options).then(function(result) {
        		$ionicLoading.hide();

        		if(jenis == 'foto') {
        			$scope.var.foto[i] = result.response;
        		} else if (jenis == 'ktp') {
        			$scope.var.ktp[i] = result.response;
        		} else if (jenis == 'passport') {
        			$scope.var.passport[i] = result.response;
        		} else {
        			$scope.var.kk[i] = result.response;
        		}


        	}, function(error){
        		$ionicLoading.hide();
        		$scope.showAlert2('Error', error.exception);
        	});
        });
    }

    

    
    
    

    $scope.submitRegister = function() {
        // if ($scope.register.name && $scope.register.address && $scope.register.gender && $scope.register.phone && $scope.register.nearest_branch) {

        	var date = new Date();


        	var data = {

        	}




        	if($scope.var.hargaPilihan == 0) {
        		$scope.showAlert2('Error', 'Anda Belum Memilih Jenis Kamar');
        	}else { 

                //$scope.navigateTo('app.umroh-pay', $scope.var.passengger);
                if(parseInt($scope.var.jmljamaah) > $scope.var.passengger.length)
                {
                	$scope.showAlert2('Error', 'Masih Ada Data Inputan Jamaah Yang Belum Tersimpan');
                }
                else {
                	var i = 0;
                	$scope.var.jamaahDewasa = 0;
                	$scope.var.jamaahChild = 0;
                	$scope.var.jamaahInfant = 0;
                	$scope.var.mahram = 0;
                	while (i < $scope.var.passengger.length)
                	{
                		if($scope.var.passengger[i].selisih_date >= 0 && $scope.var.passengger[i].selisih_date < 3){
                			$scope.var.jamaahInfant++;
                		} else if($scope.var.passengger[i].selisih_date >= 3 && $scope.var.passengger[i].selisih_date < 17) {
                			$scope.var.jamaahChild++;
                		} else if ($scope.var.passengger[i].selisih_date >= 17 ) {
                			$scope.var.jamaahDewasa++;
                		}

                		if($scope.var.passengger[i].mahram_status == 'sendiri') {
                			$scope.var.mahram++;
                		}



                		i++;
                	}
                	var tempDewasa, tempInfant, tempAnak, tempMahram, tempHandling;
                	tempAnak = $scope.var.jamaahChild * $scope.umroh.child_price;
                	tempInfant = $scope.var.jamaahInfant * $scope.umroh.infant_price;
                	tempDewasa = $scope.var.jamaahDewasa * $scope.var.hargaPilihan;
                	tempMahram = $scope.var.mahram * $scope.umroh.mahram_price;
                	tempHandling = $scope.var.passengger.length * $scope.umroh.handling_price;

                	$scope.var.totalPrice = tempAnak + tempInfant + tempDewasa + tempMahram + tempHandling;


                	$scope.var.basket = $scope.umroh.name + ' - ' + 'dewasa,' + $scope.var.hargaPilihan + '.00,' + $scope.var.jamaahDewasa + ',' + tempDewasa + '.00;'
                	+ 'anak (3th - 16th),'+ $scope.umroh.infant_price + '.00,'+$scope.var.jamaahInfant+','+ tempInfant + '.00;'
                	+ 'infant (0th - 2th),' + $scope.umroh.child_price + '.00,'+ $scope.var.jamaahChild + ',' +tempAnak + '.00;'
                	+ 'biaya surat mahram,' + $scope.umroh.mahram_price + '.00,' + $scope.var.mahram + ',' +tempMahram + '.00;'
                	+ 'shipping + handling,' + $scope.umroh.handling_price + '.00,' + $scope.var.passengger.length + ',' +tempHandling + '.00';



                	$scope.umroh.basket = $scope.var.basket;
                	$scope.umroh.amount = $scope.var.totalPrice;
                	$scope.umroh.passengger = $scope.var.passengger;
                	$scope.umroh.jmlJamaahDewasa = $scope.var.jamaahDewasa;
                	$scope.umroh.hargaJamaahDewasa = $scope.var.hargaPilihan;
                	$scope.umroh.totalBiayaJamaahDewasa = tempDewasa;
                	$scope.umroh.jmlJamaahInfant = $scope.var.jamaahInfant;
                	$scope.umroh.hargaJamaahInfant = $scope.umroh.infant_price;
                	$scope.umroh.totalBiayaJamaahInfant = tempInfant;
                	$scope.umroh.jmlJamaahChild = $scope.var.jamaahChild;
                	$scope.umroh.hargaJamaahChild = $scope.umroh.child_price;
                	$scope.umroh.totalBiayaJamaahChild = tempAnak;

                	$scope.umroh.jmlJamaahMahram = $scope.var.mahram;
                	$scope.umroh.hargaJamaaMahram = $scope.umroh.mahram_price;
                	$scope.umroh.totalBiayaJamaahMahram = tempMahram;

                	$scope.umroh.jmlJamaahHandling = $scope.var.passengger.length;
                	$scope.umroh.hargaJamaahHandling = $scope.umroh.handling_price;
                	$scope.umroh.totalBiayaJamaahHandling = tempHandling;
                	$scope.umroh.nearest_branch_id = $scope.var.nearest_branch;
                	$scope.umroh.order_type = $scope.var.order_type;

                	$scope.umroh.totalJamaah = $scope.var.passengger.length;
                	$scope.umroh.harga_dp = 5000000;
                	var tempDP = $scope.umroh.harga_dp * $scope.var.passengger.length;
                	$scope.umroh.amount_dp = tempDP;


                	$scope.navigateTo('app.umroh-pay', $scope.umroh);
                }
            }
            
            


            // RequestService.save(data, 'umrohregister').then(function(response) {
            //     $scope.showAlert('success');
            // });
            // if($scope.var.jmljamaah > ($scope.var.totaljamaah + $scope.var.jmlanak + $scope.var.jmllaki17 + $scope.var.jmlperempuan1745 + $scope.var.jmlperempuan45)) {
            //     $scope.showAlert('Error', 'Jumlah Total Jamaah Masih Kurang dari '+$scope.var.jmljamaah+' Yang anda pilih');
            // }
            // else {
            //     $scope.navigateTo('app.umroh-input', data);
            // }

            
        // } else {
        //     $scope.showAlert('warning');
        // }
    };

    $scope.navigateTo = function (targetPage, objectData) {
    	$state.go(targetPage, {
    		umroh: objectData
    	});
    };

    $scope.showAlert2 = function(title, msg) {
    	var alertPopup = $ionicPopup.alert({
    		title: title,
    		template: msg
    	});
    };

    $scope.submitListPenumpang = function() {
    	console.log($scope.var.jmljamaah);
    };

    $scope.showAlert = function(type, msg) {
    	if (type == 'warning') {
    		title = 'Pesan';
    		template = 'Harap lengkapi data.';
    	} else if (type == 'success') {
    		title = 'Terima Kasih';
    		template = 'Pendaftaran Anda akan segera kami proses.';
    	} else if (type == 'Error') {
    		title = type;
    		template = msg;
    	}

    	var alertPopup = $ionicPopup.alert({
    		title: title,
    		template: template
    	});

    	if (type == 'success') {
    		alertPopup.then(function(res) {
    			$scope.filing = [];

    			$ionicHistory.goBack(-2);
    		});
    	}
    }

    

    $scope.toModal = function(){
    	$scope.modal1.show()
    };

    $scope.toModal2 = function(){
    	$scope.modal2.show()
    };

    $scope.toModal3 = function(){
    	$scope.modal3.show()
    };

    $scope.toModal4 = function(){
    	$scope.modal4.show()
    };
    $scope.init();
});


appCtrl.controller('umrohInputCtrl', function($scope, $stateParams, $ionicPopup, $ionicHistory, RequestService, TimeService, ionicDatePicker, $state) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh Input"); }

    $scope.init = function() {
		var bintangHotel = 0;
		$scope.umroh = $stateParams.umroh;
    //   var bintangHotel = $scope.umroh.hotel;
    //   $scope.bintang = [];
    //   if(bintangHotel != null)
    //   {
    //           var i = 0;
    //           while (i < $scope.umroh.hotel)
    //           {
    //               $scope.bintang.push(i);
    //               i++;
    //           }
    //   }

    $scope.openDatePickerTanggalLahirSuami = function(){
    	var dpObj = {
    		callback: function (val) {
    			var date = new Date(val);
                    //$scope.filing.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
                },
                from: new Date(1920, 01, 01),
                to: new Date(2017, 10, 30),
                inputDate: new Date(),
                mondayFirst: true,
                closeOnSelect: false,
                templateType: 'popup'
            };
            
            ionicDatePicker.openDatePicker(dpObj);
        };

        $scope.openDatePickerTanggalLahirIstri = function(){
        	var dpObj = {
        		callback: function (val) {
        			var date = new Date(val);
                    //$scope.filing.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
                },
                from: new Date(1920, 01, 01),
                to: new Date(2017, 10, 30),
                inputDate: new Date(),
                mondayFirst: true,
                closeOnSelect: false,
                templateType: 'popup'
            };
            
            ionicDatePicker.openDatePicker(dpObj);
        };

        $scope.openDatePickerStart = function() {
        	ionicDatePicker.openDatePicker(dpObjStart);
        };

        $scope.openDatePickerEnd = function() {
        	if ($scope.search.date_start) {
        		ionicDatePicker.openDatePicker($scope.dpObjEnd);
        	}
        } 

        $scope.submitInput = function() {
        	var date = new Date();

            // var data = {
            //     date: date,
            //     type_id: $scope.umroh.type_id,
            //     city: $scope.umroh.city,
            //     content: $scope.umroh.content,
            //     departure: $scope.umroh.departure,
            //     departure_month: $scope.umroh.departure_month,
            //     hotel: $scope.umroh.hotel,
            //     img: $scope.umroh.img,
            //     name_packet: $scope.umroh.name,
            //     packet: $scope.umroh.packet,
            //     phone_travel: $scope.umroh.phone,
            //     plane: $scope.umroh.plane,
            //     price: $scope.umroh.price,
            //     price_million: $scope.umroh.price_million,
            //     quota: $scope.umroh.quota,
            //     suamiistri: $scope.umroh.cbsuamiistri,
            //     jmlsuamiistri: $scope.umroh.totaljamaah,
            //     jmlanak: $scope.umroh.jmlanak,
            //     jmllaki17: $scope.umroh.jmllaki17,
            //     jmlperempuan1945: $scope.umroh.jmlperempuan1745,
            //     jmlperempuan45: $scope.umroh.jmlperempuan45
            // }
            
            $scope.navigateTo('app.umroh-pay', data);
        }

        $scope.navigateTo = function (targetPage, objectData) {
        	$state.go(targetPage, {
        		umroh: objectData
        	});
        };

    };



    $scope.init();
});


appCtrl.controller('umrohReviewCtrl', function($ionicHistory, $window, $ionicLoading, $scope, $state, $stateParams, $ionicPopup, $ionicHistory, RequestService, TimeService, ionicDatePicker, sha1) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh Review"); }

    $scope.init = function() {
		var bintangHotel = 0;
		$scope.umroh = $stateParams.umroh;
		var bintangHotel = $stateParams.umroh.hotel;
		$scope.bintang = [];
		if(bintangHotel != null) {
			var i = 0;
			while (i < bintangHotel )
			{
				$scope.bintang.push(i);
				i++;
			}
		}

		$scope.var = [];
		$scope.var.pembayaran = 'lunas';
		$scope.varpg = [];
		$scope.user = JSON.parse(localStorage.user);
	};




	$scope.submitLunas = function() {
		var date = new Date();

		var data = { 
			"basket": 'LUNAS - '+$scope.umroh.basket,
			"channel": $scope.umroh.channel.toString(),
			"type": "umrohregister",
			"userdevice_id": $scope.user.id,
			"email":$scope.user.email,
			"name": $scope.user.name,
			"address": $scope.user.address,
			"gender": null,
			"phone": $scope.user.phone,
			"nearest_branch_id": $scope.umroh.nearest_branch,
			"type_id": $scope.umroh.id,
			"price": $scope.umroh.amount,
			"order_type":$scope.umroh.order_type,
			"travelagent_id" : $scope.umroh.travelagent_id,
			"status":1,
			"payment_status":false,
			'payment_type':'lunas',
			"passenger": $scope.umroh.passengger,
			'pic_email': $scope.umroh.pic_email
		}

		$ionicLoading.show({
			template: 'Loading..'
		});

        /*
        var mallId = '2654';
        var sharedKey =  'e9Bg1WvpYD27';
        var dokuhost = 'https://pay.doku.com/Suite/Receive';
        */
        if ($scope.umroh.channel == '15' || $scope.umroh.channel == '02')
        {
        	RequestService.saveorder(data).then(function(response) {
        		var temp, tglTrans;
        		$ionicLoading.hide();
        		if(response.status == 'error') {
        			$scope.showAlert2('Error', response.message);
        		}
        		else
        		{
        			$scope.varpg.transactionid = response.json.order_id;
        			$scope.varpg.mallid = '2654';
        			$scope.varpg.sharedkey = 'e9Bg1WvpYD27';
        			if($scope.umroh.channel == '15' || $scope.umroh.channel == '02') {
        				$scope.varpg.amount = response.json.price.toString() + '.00';
        			} else {
        				$scope.varpg.amount = response.json.price;
        			}

        			tglTrans = moment(response.json.date).format('YYYYMMDDHHmmSS');
        			temp = sha1.hash($scope.varpg.amount + $scope.varpg.mallid + $scope.varpg.sharedkey + $scope.varpg.transactionid);
        			$scope.varpg.words = temp;
        			$scope.varpg.basket = response.json.basket;
        			$scope.varpg.chainmerchant = 'NA';
        			$scope.varpg.purchaseamount = $scope.varpg.amount;
        			$scope.varpg.currency = '360';
        			$scope.varpg.purchasecurrency = '360';
        			$scope.varpg.country = 'ID';
        			$scope.varpg.sessionid = '234asd234';
        			$scope.varpg.requestdatetime = tglTrans;
        			$scope.varpg.name = response.json.name;
        			$scope.varpg.email = response.json.email;
        			$scope.varpg.paymentchannel = $scope.umroh.channel.toString();

        			var datapg = {
        				'MALLID' : $scope.varpg.mallid,
        				'BASKET' : $scope.varpg.basket,
        				'CHAINMERCHANT' : $scope.varpg.chainmerchant,
        				'AMOUNT' : $scope.varpg.amount,
        				'PURCHASEAMOUNT' : $scope.varpg.purchaseamount,
        				'TRANSIDMERCHANT' : $scope.varpg.transactionid,
        				'WORDS' : $scope.varpg.words,
        				'CURRENCY' : $scope.varpg.currency,
        				'PURCHASECURRENCY' : $scope.varpg.purchasecurrency,
        				'COUNTRY' : $scope.varpg.country,
        				'SESSIONID' : $scope.varpg.sessionid,
        				'REQUESTDATETIME' : $scope.varpg.requestdatetime,
        				'NAME' : $scope.varpg.name,
        				'EMAIL' : $scope.varpg.email,
        				'PAYMENTCHANNEL' : $scope.varpg.paymentchannel
        			}
        			localStorage.trans = JSON.stringify(datapg);
                    // $scope.navigateTo('app.umroh-paymentgateway', datapg);
                    
                    // var datapg = 'MALLID='+$scope.varpg.mallid+'&BASKET='+$scope.varpg.basket+'&CHAINMERCHANT='+$scope.varpg.chainmerchant+'&AMOUNT='+$scope.varpg.amount+'&PURCHASEAMOUNT='+$scope.varpg.purchaseamount+'&TRANSIDMERCHANT=' + $scope.varpg.transactionid+'&WORDS='+ $scope.varpg.words+'&CURRENCY='+$scope.varpg.currency+'&PURCHASECURRENCY='+$scope.varpg.purchasecurrency+'&COUNTRY='+$scope.varpg.country+'&SESSIONID=' +$scope.varpg.sessionid+'&REQUESTDATETIME='+$scope.varpg.requestdatetime+'&NAME=' +$scope.varpg.name+'&EMAIL='+$scope.varpg.email+'&PAYMENTCHANNEL='+$scope.varpg.paymentchannel;
                    // setTimeout(function() {
                    //     $('#form1').submit();
                    // } , 800)
                    $ionicHistory.nextViewOptions({
                    	disableBack: true
                    });
                    $window.location.href = '#/app/umroh-paymentgateway';
                    // RequestService.topaymentgateway(url, datapg).then(function(response) {
                    //         console.log(response);
                    // });
                }
                
                
                
            });
        } else {
        	RequestService.saveorderecoll(data).then(function(response) {
        		var temp, tglTrans;
        		$ionicLoading.hide();
        		if(response.status == 'error') {
        			$scope.showAlert2('Error', response.message);
        		}
        		else
        		{   
        			$scope.varpg.transactionid = response.json.order_id;
        			$scope.varpg.mallid = '2654';
        			$scope.varpg.sharedkey = 'e9Bg1WvpYD27';
        			if($scope.umroh.channel == '15' || $scope.umroh.channel == '02') {
        				$scope.varpg.amount = response.json.price.toString() + '.00';
        			} else {
        				$scope.varpg.amount = response.json.price;
        			}

        			tglTrans = moment(response.json.date).format('YYYYMMDDHHmmSS');
        			temp = sha1.hash($scope.varpg.amount + $scope.varpg.mallid + $scope.varpg.sharedkey + $scope.varpg.transactionid);
        			$scope.varpg.words = temp;
        			$scope.varpg.basket = response.json.basket;
        			$scope.varpg.chainmerchant = 'NA';
        			$scope.varpg.purchaseamount = $scope.varpg.amount;
        			$scope.varpg.currency = '360';
        			$scope.varpg.purchasecurrency = '360';
        			$scope.varpg.country = 'ID';
        			$scope.varpg.sessionid = '234asd234';
        			$scope.varpg.requestdatetime = tglTrans;
        			$scope.varpg.name = response.json.name;
        			$scope.varpg.email = response.json.email;
        			$scope.varpg.paymentchannel = $scope.umroh.channel.toString();

        			var datapg = {
        				'MALLID' : $scope.varpg.mallid,
        				'BASKET' : $scope.varpg.basket,
        				'CHAINMERCHANT' : $scope.varpg.chainmerchant,
        				'AMOUNT' : $scope.varpg.amount,
        				'PURCHASEAMOUNT' : $scope.varpg.purchaseamount,
        				'TRANSIDMERCHANT' : $scope.varpg.transactionid,
        				'WORDS' : $scope.varpg.words,
        				'CURRENCY' : $scope.varpg.currency,
        				'PURCHASECURRENCY' : $scope.varpg.purchasecurrency,
        				'COUNTRY' : $scope.varpg.country,
        				'SESSIONID' : $scope.varpg.sessionid,
        				'REQUESTDATETIME' : $scope.varpg.requestdatetime,
        				'NAME' : $scope.varpg.name,
        				'EMAIL' : $scope.varpg.email,
        				'PAYMENTCHANNEL' : $scope.varpg.paymentchannel,
        				'va' : response.json.va,
        				'date' : response.json.date
        			}
                    // localStorage.trans = JSON.stringify(datapg);
                    // $scope.navigateTo('app.umroh-paymentgateway', datapg);
                    
                    // var datapg = 'MALLID='+$scope.varpg.mallid+'&BASKET='+$scope.varpg.basket+'&CHAINMERCHANT='+$scope.varpg.chainmerchant+'&AMOUNT='+$scope.varpg.amount+'&PURCHASEAMOUNT='+$scope.varpg.purchaseamount+'&TRANSIDMERCHANT=' + $scope.varpg.transactionid+'&WORDS='+ $scope.varpg.words+'&CURRENCY='+$scope.varpg.currency+'&PURCHASECURRENCY='+$scope.varpg.purchasecurrency+'&COUNTRY='+$scope.varpg.country+'&SESSIONID=' +$scope.varpg.sessionid+'&REQUESTDATETIME='+$scope.varpg.requestdatetime+'&NAME=' +$scope.varpg.name+'&EMAIL='+$scope.varpg.email+'&PAYMENTCHANNEL='+$scope.varpg.paymentchannel;
                    // setTimeout(function() {
                    //     $('#form1').submit();
                    // } , 800)
                    // $ionicHistory.nextViewOptions({
                    //     disableBack: true
                    // });
                    // $window.location.href = '#/app/umroh-paymentgateway';
                    // RequestService.topaymentgateway(url, datapg).then(function(response) {
                    //         console.log(response);
                    // });
                    $ionicHistory.nextViewOptions({
                    	disableBack: true
                    });
                    $scope.navigateTo('app.umroh-va', datapg);
                }
                
                
            });
        }


        // $scope.navigateTo('app.umroh-pay', data);
    }
    
    $scope.submitDP = function() {
    	var date = new Date();

    	var data = { 
    		"basket": 'DP - '+$scope.umroh.basket,
    		"channel": $scope.umroh.channel.toString(),
    		"type": "umrohregister",
    		"userdevice_id": $scope.user.id,
    		"email":$scope.user.email,
    		"name": $scope.user.name,
    		"address": $scope.user.address,
    		"gender": null,
    		"phone": $scope.user.phone,
    		"nearest_branch_id": $scope.umroh.nearest_branch,
    		"type_id": $scope.umroh.id,
    		"price": $scope.umroh.amount_dp,
    		"order_type":$scope.umroh.order_type,
    		"travelagent_id" : $scope.umroh.travelagent_id,
    		"status":1,
    		"payment_status":false,
    		'payment_type':'dp',
    		"passenger": $scope.umroh.passengger,
    		'pic_email': $scope.umroh.pic_email
    	}

    	$ionicLoading.show({
    		template: 'Loading..'
    	});

        console.log('--------data--------');
        console.log(JSON.stringify(data));
        if ($scope.umroh.channel == '15' || $scope.umroh.channel == '02')
        {
            console.log('--------15/02--------');
            RequestService.saveorder(data).then(function(response) {
               if(response.status == 'error') {
                $scope.showAlert2('Error', response.message);
            }
            else
            {
                var temp, tglTrans;
                $ionicLoading.hide();

                $scope.varpg.transactionid = response.json.order_id;
                $scope.varpg.mallid = '2654';
                $scope.varpg.sharedkey = 'e9Bg1WvpYD27';
                if($scope.umroh.channel == '15' || $scope.umroh.channel == '02') {
                 $scope.varpg.amount = response.json.price.toString() + '.00';
             } else {
                 $scope.varpg.amount = response.json.price;
             }

             tglTrans = moment(response.json.date).format('YYYYMMDDHHmmSS');
             temp = sha1.hash($scope.varpg.amount + $scope.varpg.mallid + $scope.varpg.sharedkey + $scope.varpg.transactionid);
             $scope.varpg.words = temp;
             $scope.varpg.basket = response.json.basket;
             $scope.varpg.chainmerchant = 'NA';
             $scope.varpg.purchaseamount = $scope.varpg.amount;
             $scope.varpg.currency = '360';
             $scope.varpg.purchasecurrency = '360';
             $scope.varpg.country = 'ID';
             $scope.varpg.sessionid = '234asd234';
             $scope.varpg.requestdatetime = tglTrans;
             $scope.varpg.name = response.json.name;
             $scope.varpg.email = response.json.email;
             $scope.varpg.paymentchannel = $scope.umroh.channel.toString();

             var datapg = {
                 'MALLID' : $scope.varpg.mallid,
                 'BASKET' : $scope.varpg.basket,
                 'CHAINMERCHANT' : $scope.varpg.chainmerchant,
                 'AMOUNT' : $scope.varpg.amount,
                 'PURCHASEAMOUNT' : $scope.varpg.purchaseamount,
                 'TRANSIDMERCHANT' : $scope.varpg.transactionid,
                 'WORDS' : $scope.varpg.words,
                 'CURRENCY' : $scope.varpg.currency,
                 'PURCHASECURRENCY' : $scope.varpg.purchasecurrency,
                 'COUNTRY' : $scope.varpg.country,
                 'SESSIONID' : $scope.varpg.sessionid,
                 'REQUESTDATETIME' : $scope.varpg.requestdatetime,
                 'NAME' : $scope.varpg.name,
                 'EMAIL' : $scope.varpg.email,
                 'PAYMENTCHANNEL' : $scope.varpg.paymentchannel
             }
             localStorage.trans = JSON.stringify(datapg);
                            // $scope.navigateTo('app.umroh-paymentgateway', datapg);
                            
                            // var datapg = 'MALLID='+$scope.varpg.mallid+'&BASKET='+$scope.varpg.basket+'&CHAINMERCHANT='+$scope.varpg.chainmerchant+'&AMOUNT='+$scope.varpg.amount+'&PURCHASEAMOUNT='+$scope.varpg.purchaseamount+'&TRANSIDMERCHANT=' + $scope.varpg.transactionid+'&WORDS='+ $scope.varpg.words+'&CURRENCY='+$scope.varpg.currency+'&PURCHASECURRENCY='+$scope.varpg.purchasecurrency+'&COUNTRY='+$scope.varpg.country+'&SESSIONID=' +$scope.varpg.sessionid+'&REQUESTDATETIME='+$scope.varpg.requestdatetime+'&NAME=' +$scope.varpg.name+'&EMAIL='+$scope.varpg.email+'&PAYMENTCHANNEL='+$scope.varpg.paymentchannel;
                            // setTimeout(function() {
                            //     $('#form1').submit();
                            // } , 800)
                            // $ionicHistory.nextViewOptions({
                            //     disableBack: true
                            // });
                            $window.location.href = '#/app/umroh-paymentgateway';
                            // RequestService.topaymentgateway(url, datapg).then(function(response) {
                            //         console.log(response);
                            // });
                            
                        }
                        
                        
                    });
        } else {    		
            console.log('--------ecoll--------');
            RequestService.saveorderecoll(data).then(function(response) {
               var temp, tglTrans;
               $ionicLoading.hide();
               if(response.status == 'error') {
                console.log('-------error------');
                console.log(JSON.stringify(response));
                $scope.showAlert2('Error', response.message);
            }
            else
            {
                console.log('-------success------');
                $scope.varpg.transactionid = response.json.order_id;
                $scope.varpg.mallid = '2654';
                $scope.varpg.sharedkey = 'e9Bg1WvpYD27';
                if($scope.umroh.channel == '15' || $scope.umroh.channel == '02') {
                 $scope.varpg.amount = response.json.price.toString() + '.00';
             } else {
                 $scope.varpg.amount = response.json.price;
             }

             tglTrans = moment(response.json.date).format('YYYYMMDDHHmmSS');
             temp = sha1.hash($scope.varpg.amount + $scope.varpg.mallid + $scope.varpg.sharedkey + $scope.varpg.transactionid);
             $scope.varpg.words = temp;
             $scope.varpg.basket = response.json.basket;
             $scope.varpg.chainmerchant = 'NA';
             $scope.varpg.purchaseamount = $scope.varpg.amount;
             $scope.varpg.currency = '360';
             $scope.varpg.purchasecurrency = '360';
             $scope.varpg.country = 'ID';
             $scope.varpg.sessionid = '234asd234';
             $scope.varpg.requestdatetime = tglTrans;
             $scope.varpg.name = response.json.name;
             $scope.varpg.email = response.json.email;
             $scope.varpg.paymentchannel = $scope.umroh.channel.toString();

             var datapg = {
                 'MALLID' : $scope.varpg.mallid,
                 'BASKET' : $scope.varpg.basket,
                 'CHAINMERCHANT' : $scope.varpg.chainmerchant,
                 'AMOUNT' : $scope.varpg.amount,
                 'PURCHASEAMOUNT' : $scope.varpg.purchaseamount,
                 'TRANSIDMERCHANT' : $scope.varpg.transactionid,
                 'WORDS' : $scope.varpg.words,
                 'CURRENCY' : $scope.varpg.currency,
                 'PURCHASECURRENCY' : $scope.varpg.purchasecurrency,
                 'COUNTRY' : $scope.varpg.country,
                 'SESSIONID' : $scope.varpg.sessionid,
                 'REQUESTDATETIME' : $scope.varpg.requestdatetime,
                 'NAME' : $scope.varpg.name,
                 'EMAIL' : $scope.varpg.email,
                 'PAYMENTCHANNEL' : $scope.varpg.paymentchannel,
                 'va' : response.json.va,
                 'date' : response.json.date
             }
                            // localStorage.trans = JSON.stringify(datapg);
                            // $scope.navigateTo('app.umroh-paymentgateway', datapg);
                            
                            // var datapg = 'MALLID='+$scope.varpg.mallid+'&BASKET='+$scope.varpg.basket+'&CHAINMERCHANT='+$scope.varpg.chainmerchant+'&AMOUNT='+$scope.varpg.amount+'&PURCHASEAMOUNT='+$scope.varpg.purchaseamount+'&TRANSIDMERCHANT=' + $scope.varpg.transactionid+'&WORDS='+ $scope.varpg.words+'&CURRENCY='+$scope.varpg.currency+'&PURCHASECURRENCY='+$scope.varpg.purchasecurrency+'&COUNTRY='+$scope.varpg.country+'&SESSIONID=' +$scope.varpg.sessionid+'&REQUESTDATETIME='+$scope.varpg.requestdatetime+'&NAME=' +$scope.varpg.name+'&EMAIL='+$scope.varpg.email+'&PAYMENTCHANNEL='+$scope.varpg.paymentchannel;
                            // setTimeout(function() {
                            //     $('#form1').submit();
                            // } , 800)
                            // $ionicHistory.nextViewOptions({
                            //     disableBack: true
                            // });
                            // $window.location.href = '#/app/umroh-paymentgateway';
                            // RequestService.topaymentgateway(url, datapg).then(function(response) {
                            //         console.log(response);
                            // });
                            $scope.navigateTo('app.umroh-va', datapg);
                        }
                        
                        
                    });
}
        // $scope.navigateTo('app.umroh-pay', data);
    }

    $scope.showAlert2 = function(title, msg) {
    	var alertPopup = $ionicPopup.alert({
    		title: title,
    		template: msg
    	});
    };


    $scope.newMessage = function(){
    	$ionicHistory.nextViewOptions({
    		disableBack: true
    	});
    	$window.location.href = '#/app/';
    }

    $scope.navigateTo = function (targetPage, objectData) {
    	$state.transitionTo(targetPage, {
    		umroh: objectData
    	});
    };

    $scope.init();
});


//   appCtrl.controller('umrohPaymentGatewayCtrl', function($ionicLoading, $scope, $stateParams, $ionicPopup, $ionicHistory, RequestService, TimeService, ionicDatePicker, sha1) {
//     $scope.init = function() {
//         var bintangHotel = 0;
//         $scope.varpg = $stateParams.umroh;
//         $('#form1').submit();


//     };



//     $scope.init();


//   });

appCtrl.controller('umrohVACtrl', function($interval, $scope, $stateParams, $ionicPopup, $ionicHistory, RequestService, $state) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh VA"); }

    var promise;
	$scope.init = function() {
		$scope.umroh = $stateParams.umroh;
		$scope.var = [];



	};


    //submitReview
    
    $scope.navigateTo = function (targetPage, objectData) {
    	$state.go(targetPage, {
    		umroh: objectData
    	});
    };

    $scope.newMessage = function(){
    	$ionicHistory.nextViewOptions({
    		disableBack: true
    	});
    	$state.go('app.home');
    }
    
    $scope.init();
    promise =  $interval(function(){
    	var countDownDate = new Date($scope.umroh.date);
    	countDownDate.setDate(countDownDate.getDate() + 2);
    	countDownDate = countDownDate.getTime();
    	var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        $scope.var.days = Math.floor(distance / (1000 * 60 * 60 * 24));
        $scope.var.hours = Math.floor((distance / (1000 * 60 * 60)));
        $scope.var.minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        $scope.var.seconds = Math.floor((distance % (1000 * 60)) / 1000);
        if(distance < 0)
        {
        	$interval.cancel(promise);
        }
    }, 1000);
});


appCtrl.controller('umrohPayCtrl', function($scope, $stateParams, $ionicPopup, $ionicHistory, RequestService, $state) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh Pay"); }

    $scope.init = function() {
		$scope.umroh = $stateParams.umroh;
		$scope.var = [];
		$scope.var.pembayaran = "lunas";
		$scope.pay = [];

		$scope.pay.channel = "15";
	};

    //submitReview

    $scope.submitPay = function() {
    	$scope.umroh.channel = $scope.pay.channel
    	$scope.navigateTo('app.umroh-review', $scope.umroh);
    }
    
    $scope.navigateTo = function (targetPage, objectData) {
    	$state.go(targetPage, {
    		umroh: objectData
    	});
    };

    
    
    $scope.init();
});

appCtrl.controller('umrohFilingCtrl', function($scope, $stateParams, $ionicPopup, $ionicHistory, $ionicLoading, ionicDatePicker, TimeService, RequestService) {
	//if(typeof analytics !== undefined) { analytics.trackView("Umroh Filing"); }

    $scope.init = function() {
		$scope.umroh = $stateParams.umroh;

		$scope.filing = [];
		$scope.branchs = [];
		$scope.profile = [];
		$scope.login = [];

		$scope.userCheck();
	}

	$scope.openDatePicker = function(){
		var dpObj = {
			callback: function (val) {
				var date = new Date(val);
				$scope.filing.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
			},
			from: new Date(1920, 01, 01),
			to: new Date(2017, 10, 30),
			inputDate: new Date(),
			mondayFirst: true,
			closeOnSelect: false,
			templateType: 'popup'
		};

		ionicDatePicker.openDatePicker(dpObj);
	};

	$scope.submitFiling = function() {
		if ($scope.filing.facility && $scope.filing.name && $scope.filing.birthday && $scope.filing.gender && $scope.filing.city && $scope.filing.address && $scope.filing.kelurahan && $scope.filing.kecamatan && $scope.filing.rt && $scope.filing.rw && $scope.filing.postal_code && $scope.filing.phone && $scope.filing.email && $scope.filing.nearest_branch && $scope.filing.job && $scope.filing.company_type && $scope.filing.job_position && $scope.filing.business_field && $scope.filing.year_start_work && $scope.filing.service_length && $scope.filing.office_name && $scope.filing.office_phone && $scope.filing.income && $scope.filing.contact_to) {
			if ($scope.filing.term1 && $scope.filing.term2) {
				var data = {
					address: $scope.filing.address,
					birthday: $scope.filing.birthday,
					business_field: $scope.filing.business_field,
					city: $scope.filing.city,
					company_type: $scope.filing.company_type,
					contact_to: $scope.filing.contact_to,
					email: $scope.filing.email,
					facility: $scope.filing.facility,
					gender: $scope.filing.gender,
					income: $scope.filing.income,
					job: $scope.filing.job,
					job_position: $scope.filing.job_position,
					kecamatan: $scope.filing.kecamatan,
					kelurahan: $scope.filing.kelurahan,
					name: $scope.filing.name,
					nearest_branch_id: $scope.filing.nearest_branch,
					office_name: $scope.filing.office_name,
					office_phone: $scope.filing.office_phone,
					phone: $scope.filing.phone,
					postal_code: $scope.filing.postal_code,
					rt: $scope.filing.rt,
					rw: $scope.filing.rw,
					service_length: $scope.filing.service_length,
					type_id: $scope.umroh.id,
					year_start_work: $scope.filing.year_start_work
				}

				RequestService.save(data, 'umrohfiling').then(function(response) {
					$scope.showAlert('success');
				});
			} else {
				$scope.showAlert('warning2');
			}
		} else {
			$scope.showAlert('warning');
		}
	}

	$scope.showAlert = function(type, msg, user) {
		if (type == 'warning') {
			title = 'Pesan';
			template = 'Harap lengkapi data.';
		} else if (type == 'warning2') {
			title = 'Pesan';
			template = 'Harap menyetujui Syarat dan Ketentuan.';
		} else if (type == 'warning3') {
			title = 'Pesan';
			template = 'Password tidak sama';
		} else if (type == 'warning4') {
			title = 'Pesan';
			template = msg;
		} else if (type == 'warning5') {
			title = 'Pesan';
			template = 'Username tidak boleh mengandung spasi dan karakter simbol';
		} else if (type == 'success') {
			title = 'Terima Kasih';
			template = 'Pengajuan Anda akan segera kami proses.';
		} else {
			title = 'Terima Kasih';
			template = msg;
		}

		var alertPopup = $ionicPopup.alert({
			title: title,
			template: template
		});

		if (type == 'success') {
			alertPopup.then(function(res) {
				$scope.filing = [];

				$ionicHistory.goBack(-2);
			});
		} else if (type == 'success2') {
			$scope.profile = [];

			$('#umroh-profile-loading-progress').show();

			$('#filing-register').hide();

			var data = {
				username: user.username,
				pass: user.password
			};

			RequestService.login(data).then(function(response) {
				$scope.user = response;

				$('#umroh-profile-loading-progress').hide();

				$scope.showForm();

				localStorage.user = JSON.stringify(response);
			});
		} else if (type == 'success3') {
			alertPopup.then(function(res) {
				$scope.login = [];

				$('#umroh-profile-loading-progress').show();

				$('#filing-login').hide();

				$scope.user = user;

				$('#umroh-loading-progress').hide();

				$scope.showForm();

				localStorage.user = JSON.stringify(user);
			});
		}
	}

	$scope.userCheck = function() {
		$('#umroh-profile-loading-progress').show();

		if (localStorage.user) {
			$('#umroh-profile-loading-progress').hide();

			$scope.showForm();
		} else {
			$('#umroh-profile-loading-progress').hide();

			var dpObj = {
                callback: function (val) {  //Mandatory
                	var date = new Date(val);
                	$scope.profile.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
                },
                from: new Date(1920, 01, 01), //Optional
                inputDate: new Date(2017, 10, 30),      //Optional
                mondayFirst: true,          //Optional
                closeOnSelect: false,       //Optional
                templateType: 'popup'       //Optional
            };

            $scope.openDatePickerRegister = function() {
            	ionicDatePicker.openDatePicker(dpObj);
            };

            $('#filing-form').hide();
            $('#filing-register').show();
        }
    }

    $scope.submitProfile = function() {
    	if ($scope.profile.username && $scope.profile.password && $scope.profile.repassword && $scope.profile.name && $scope.profile.phone && $scope.profile.email && $scope.profile.address && $scope.profile.birthday) {
    		if ($scope.profile.password != $scope.profile.repassword) {
    			$scope.showAlert('warning3');
    		} else {
    			if ($scope.profile.username.match(/^[a-zA-Z0-9.\-_]+$/)) {
    				$ionicLoading.show({
    					template: 'Registering..'
    				});

    				var deviceInfo = ionic.Platform.device();

    				var data = {
    					username: $scope.profile.username,
    					pass: $scope.profile.password,
    					name: $scope.profile.name,
    					phone: $scope.profile.phone,
    					email: $scope.profile.email,
    					city: $scope.profile.city,
    					address: $scope.profile.address,
    					uuid: deviceInfo.uuid,
    					device: deviceInfo.model
                        //uuid: '5b030556347f72ed',
                        //device: 'SM-A520F'
                    }

                    RequestService.save(data, 'userdevice').then(function(response) {
                    	$ionicLoading.hide();

                    	$scope.showAlert('success2', 'Terima Kasih telah melakukan registrasi', $scope.profile);
                    });
                } else {
                	$scope.showAlert('warning5');
                }
            }
        } else {
        	$scope.showAlert('warning');
        }
    }

    $scope.doLogin = function() {
    	if ($scope.login.username && $scope.login.password) {
    		var data = {
    			username: $scope.login.username,
    			pass: $scope.login.password
    		}

    		$ionicLoading.show({
    			template: 'Logging in..'
    		});

    		RequestService.login(data).then(function(response) {
    			$ionicLoading.hide();

    			if (response.status == 'error') {
    				$scope.showAlert('warning4', response.message);
    			} else {
    				$scope.showAlert('success3', 'Login berhasil.', response);
    			}
    		});
    	} else {
    		$scope.showAlert('warning');
    	}
    }

    $scope.showForm = function() {
    	RequestService.getallnopaging('branch').then(function(branchs) {
    		for (var ii = 0; ii < branchs.length; ii++) {
    			$scope.branchs.push({ 'value' : branchs[ii].id, 'label': branchs[ii].name });
    		}
    	});

    	$scope.filing.facility = 'Umroh';

        // $scope.facilities = [{
        //     value: 'Umroh',
        //     label: 'Umroh'
        //     value: 'Pembelian',
        //     label: 'Pembelian'
        // }, {
        //     value: 'Pembangunan',
        //     label: 'Pembangunan'
        // }, {
        //     value: 'Renovasi',
        //     label: 'Renovasi'
        // }, {
        //     value: 'Take Over',
        //     label: 'Take Over'
        // }, {
        //     value: 'Pembelian Kavling',
        //     label: 'Pembelian Kavling'
        // }, {
        //     value: 'Roda 4 Baru',
        //     label: 'Roda 4 Baru'
        // }, {
        //     value: 'Roda 4 Second',
        //     label: 'Roda 4 Second'
        // }];

        $scope.genders = [{
        	value: 'Laki-laki',
        	label: 'Laki-laki'
        }, {
        	value: 'Perempuan',
        	label: 'Perempuan'
        }];

        $scope.jobs = [{
        	value: 'Karyawan',
        	label: 'Karyawan'
        }, {
        	value: 'Wiraswasta',
        	label: 'Wiraswasta'
        }, {
        	value: 'Professional',
        	label: 'Professional'
        }];

        $scope.company_types = [{
        	value: 'Perusahaan Swasta Nasional',
        	label: 'Perusahaan Swasta Nasional'
        }, {
        	value: 'Perusahaan Swasta Asing',
        	label: 'Perusahaan Swasta Asing'
        }, {
        	value: 'Pemerintahan',
        	label: 'Pemerintahan'
        }, {
        	value: 'BUMN',
        	label: 'BUMN'
        }, {
        	value: 'BUMD',
        	label: 'BUMD'
        }, {
        	value: 'TNI/Polri',
        	label: 'TNI/Polri'
        }, {
        	value: 'Perusahaan Perorangan',
        	label: 'Perusahaan Perorangan'
        }];

        $scope.job_positions = [{
        	value: 'Di Bawah Supervisor',
        	label: 'Di Bawah Supervisor'
        }, {
        	value: 'Supervisor Ke Atas',
        	label: 'Supervisor Ke Atas'
        }, {
        	value: 'Dosen/Guru',
        	label: 'Dosen/Guru'
        }, {
        	value: 'Wiraswasta',
        	label: 'Wiraswasta'
        }, {
        	value: 'Profesional',
        	label: 'Profesional'
        }, {
        	value: 'Pensiunan',
        	label: 'Pensiunan'
        }, {
        	value: 'Tidak Bekerja',
        	label: 'Tidak Bekerja'
        }];

        $scope.business_fields = [{
        	value: 'Konstruksi',
        	label: 'Konstruksi'
        }, {
        	value: 'Industri Pengolahan',
        	label: 'Industri Pengolahan'
        }, {
        	value: 'Jasa-jasa Dunia Usaha',
        	label: 'Jasa-jasa Dunia Usaha'
        }, {
        	value: 'Jasa-jasa Sosial Masyarakat',
        	label: 'Jasa-jasa Sosial Masyarakat'
        }, {
        	value: 'Listrik, Gas dan Air',
        	label: 'Listrik, Gas dan Air'
        }, {
        	value: 'Pengangkutan, Pergudangan dan Komunikasi',
        	label: 'Pengangkutan, Pergudangan dan Komunikasi'
        }, {
        	value: 'Perdagangan, Restoran dan Hotel',
        	label: 'Perdagangan, Restoran dan Hotel'
        }, {
        	value: 'Pertambangan',
        	label: 'Pertambangan'
        }, {
        	value: 'Pertanian, Perburuan dan Sarana Pertanian',
        	label: 'Pertanian, Perburuan dan Sarana Pertanian'
        }];

        $scope.contact_options = [{
        	value: 'No. Handphone',
        	label: 'No. Handphone'
        }, {
        	value: 'Telp. Kantor',
        	label: 'Telp. Kantor'
        }];

        $('#filing-form').show();
    }

    $scope.doRefresh = function() {
    	$scope.userCheck();

    	$scope.$broadcast('scroll.refreshComplete');
    }

    $scope.showLogin = function() {
    	$('#filing-register').hide();
    	$('#filing-login').show();
    }

    $scope.showRegister = function() {
    	$('#filing-register').show();
    	$('#filing-login').hide();
    }

    // $scope.interested = function() {
    //     $scope.interest = {};

    //     var alertPopup = $ionicPopup.show({
    //         title: 'Permohonan',
    //         subTitle: 'Harap masukkan data Anda',
    //         template: '<input type="text" placeholder="Nama" ng-model="interest.name"><input type="tel" placeholder="No. Handphone" ng-model="interest.phone">',
    //         scope: $scope,
    //         buttons: [
    //             { text: 'Batal' },
    //             {
    //                 text: '<b>Submit</b>',
    //                 type: 'button-positive',
    //                 onTap: function(e) {
    //                     if ($scope.interest.name && $scope.interest.phone) {
    //                         return $scope.interest;
    //                     } else {
    //                         e.preventDefault();
    //                     }
    //                 }
    //             }
    //         ]
    //     });

    //     alertPopup.then(function(res) {
    //         if (res) {
    //             var alertPopup = $ionicPopup.alert({
    //                 title: 'Pesan',
    //                 template: 'Terima Kasih, Bpk/Ibu ' + res.name + '.<br>Permohonan Anda akan segera kami proses.'
    //             });
    //         }
    //     });
    // }

    $scope.init();
});

// appCtrl.controller('umrohSearchCtrl', function($scope, $state, RequestService, TimeService, ionicDatePicker) {
//     $scope.init = function() {
//         $scope.search = [];
//         $scope.search.rate = 3;
//         $scope.results = [];

//         var dpObj = {
//             callback: function (val) {  //Mandatory
//                 var date = new Date(val);
//                 $scope.search.departure = date;
//                 $scope.search.date = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
//             },
//             from: new Date(), //Optional
//             to: new Date(2017, 10, 30), //Optional
//             inputDate: new Date(),      //Optional
//             mondayFirst: true,          //Optional
//             closeOnSelect: false,       //Optional
//             templateType: 'popup'       //Optional
//         };

//         $scope.openDatePicker = function(){
//             ionicDatePicker.openDatePicker(dpObj);
//         };
//     }

//     $scope.searchUmroh = function() {
//         $scope.results = [];

//         $('#umroh-search-loading-progress').show();

//         var data = {
//             'city': $scope.search.city,
//             'departure': $scope.search.departure,
//             'min_price': $scope.search.min_price,
//             'max_price': $scope.search.max_price,
//             'rate': $scope.search.rate
//         };

//         RequestService.search(data, 'umroh').then(function(results) {
//             for (var ii = 0; ii < results.length; ii++) {
//                 var departure = new Date(results[ii].departure);
//                 results[ii].departure_month = TimeService.months[departure.getMonth()] + ' ' + departure.getFullYear();

//                 results[ii].price_million = results[ii].price / 1000000;

//                 $scope.results.push(results[ii]);
//             }

//             $('#umroh-search-loading-progress').hide();
//             $('#umroh-result').show();
//         });
//     }

//     $scope.navigateTo = function (targetPage, objectData) {
//         $state.go(targetPage, {
//             umroh: objectData
//         });
//     };

//     $scope.init();
// });
