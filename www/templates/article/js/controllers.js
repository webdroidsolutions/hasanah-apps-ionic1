appCtrl.controller('articleCtrl', function($scope, $state, RequestService) {
//if(typeof analytics !== undefined) { analytics.trackView("Article"); }

        $scope.baseUrl = RequestService.urlImageLink;
    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {
		$scope.categories = [];

		$scope.getData();
	}

	$scope.getData = function() {
		RequestService.getall('articlecategory').then(function(categories) {
            for (var ii = 0; ii < categories.length; ii++) {
                $scope.categories.push(categories[ii]);
            }

            $('#article-loading-progress').hide();
            $('#article-content').show();
        });
	}

	$scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            category: objectData
        });
    };

	$scope.init();
});

appCtrl.controller('subCategoryCtrl', function($scope, $state, $stateParams, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Article Subcategory"); }

    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {
		$scope.category = $stateParams.category;
		$scope.subCategories = [];

		$scope.getData();
	}

	$scope.getData = function() {
        var data = { id_article_category : $scope.category.id };        
        
		RequestService.getby(data, 'articlesubcategory', 'idarticlecategory').then(function(subCategories) {
            for (var ii = 0; ii < subCategories.length; ii++) {
                $scope.subCategories.push(subCategories[ii]);
            }

            $('#subcategory-loading-progress').hide();
            $('#subcategory-content').show();
        });
	}

	$scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            subcategory: objectData
        });
    };

	$scope.init();
});

appCtrl.controller('articleListCtrl', function($scope, $state, $stateParams, RequestService, TimeService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Article List"); }

    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {
		$scope.subCategory = $stateParams.subcategory;
		
		$scope.articles = [];

        $scope.getData();
	}

    $scope.getData = function() {
        var data = { id_article_sub_category: $scope.subCategory.id };

        RequestService.getby(data, 'article', 'idarticlesubcategory').then(function(articles) {
            for (var ii = 0; ii < articles.length; ii++) {
                $scope.articles.push(articles[ii]);
                var date = new Date(articles[ii].time);
                $scope.articles[ii].date = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
            }

            $('#article-list-loading-progress').hide();
            $('#article-wrapper').show();
        });
    }

	// $scope.loadMoreArticles = function() {
 //        RequestService.getall('article').then(function(articles) {
 //            for (var ii = 0; ii < articles.length; ii++) {
 //                var date = new Date(articles[ii].time);
 //                articles[ii].date = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();

 //                $scope.articles.push(articles[ii]);
 //            }

 //            $('#article-list-loading-progress').hide();
 //            $('#article-wrapper').show();

 //            $scope.$broadcast('scroll.infiniteScrollComplete');
 //        });
 //    };

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            article: objectData
        });
    };

	$scope.init();
});

appCtrl.controller('articleDetailCtrl', function($scope, $stateParams) {
    //if(typeof analytics !== undefined) { analytics.trackView("Article Detail"); }

    $scope.init = function() {
        $scope.article = $stateParams.article;
    }

    $scope.init();
});