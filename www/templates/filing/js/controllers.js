appCtrl.controller('filingCtrl', function($scope, $state, $ionicPopup, $ionicLoading, TimeService, ionicDatePicker, RequestService) {
	//if(typeof analytics !== undefined) { analytics.trackView("Filing"); }

    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {
		$scope.filing = [];
		$scope.branchs = [];
		$scope.types = [];
		$scope.profile = [];
		$scope.login = [];

		$scope.userCheck();
	}

	$scope.openDatePicker = function(){
		var dpObj = {
            callback: function (val) {
                var date = new Date(val);
                $scope.filing.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
            },
            from: new Date(1920, 01, 01),
            to: new Date(2017, 10, 30),
            inputDate: new Date(),
            mondayFirst: true,
            closeOnSelect: false,
            templateType: 'popup'
        };

        ionicDatePicker.openDatePicker(dpObj);
    };

    $scope.submitFiling = function() {
    	console.log($scope.filing.length);
    	console.log($scope.filing);
    	if ($scope.filing.type && $scope.filing.facility && $scope.filing.name && $scope.filing.birthday && $scope.filing.gender && $scope.filing.city && $scope.filing.address && $scope.filing.kelurahan && $scope.filing.kecamatan && $scope.filing.rt && $scope.filing.rw && $scope.filing.postal_code && $scope.filing.phone && $scope.filing.email && $scope.filing.nearest_branch && $scope.filing.job && $scope.filing.company_type && $scope.filing.job_position && $scope.filing.business_field && $scope.filing.year_start_work && $scope.filing.service_length && $scope.filing.office_name && $scope.filing.office_phone && $scope.filing.income && $scope.filing.contact_to) {
			if ($scope.filing.term1 && $scope.filing.term2) {
				var data = {
					address: $scope.filing.address,
					birthday: $scope.filing.birthday,
					business_field: $scope.filing.business_field,
					city: $scope.filing.city,
					company_type: $scope.filing.company_type,
					contact_to: $scope.filing.contact_to,
					email: $scope.filing.email,
					facility: $scope.filing.facility,
					gender: $scope.filing.gender,
					income: $scope.filing.income,
					job: $scope.filing.job,
					job_position: $scope.filing.job_position,
					kecamatan: $scope.filing.kecamatan,
					kelurahan: $scope.filing.kelurahan,
					name: $scope.filing.name,
					nearest_branch_id: $scope.filing.nearest_branch,
					office_name: $scope.filing.office_name,
					office_phone: $scope.filing.office_phone,
					phone: $scope.filing.phone,
					postal_code: $scope.filing.postal_code,
					rt: $scope.filing.rt,
					rw: $scope.filing.rw,
					service_length: $scope.filing.service_length,
					type_id: $scope.filing.type,
					year_start_work: $scope.filing.year_start_work
				}

				RequestService.save(data, 'financefiling').then(function(response) {
		            $scope.showAlert('success');
				});
			} else {
				$scope.showAlert('warning2');
			}
        } else {
            $scope.showAlert('warning');
        }
    }

    $scope.showAlert = function(type, msg, user) {
        if (type == 'warning') {
            title = 'Pesan';
            template = 'Harap lengkapi data.';
        } else if (type == 'warning2') {
        	title = 'Pesan';
        	template = 'Harap menyetujui Syarat dan Ketentuan.';
        } else if (type == 'warning3') {
        	title = 'Pesan';
        	template = 'Password tidak sama';
        } else if (type == 'warning4') {
        	title = 'Pesan';
        	template = msg;
        } else if (type == 'warning5') {
        	title = 'Pesan';
        	template = 'Username tidak boleh mengandung spasi dan karakter simbol';
        } else if (type == 'success') {
            title = 'Terima Kasih';
            template = 'Pengajuan Anda akan segera kami proses.';
        } else {
            title = 'Terima Kasih';
            template = msg;
        }

        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template
        });

        if (type == 'success') {
            alertPopup.then(function(res) {
                $scope.filing = [];
            });
        } else if (type == 'success2') {
        	$scope.profile = [];

            $('#filing-profile-loading-progress').show();

            $('#filing-register').hide();

            var data = {
                username: user.username,
                pass: user.password
            };

            RequestService.login(data).then(function(response) {
                $scope.user = response;

                $('#filing-profile-loading-progress').hide();

                $scope.showForm();

                localStorage.user = JSON.stringify(response);
            });
        } else if (type == 'success3') {
        	alertPopup.then(function(res) {
                $scope.login = [];

                $('#filing-profile-loading-progress').show();

                $('#filing-login').hide();

                $scope.user = user;

                $('#filing-loading-progress').hide();

                $scope.showForm();

                localStorage.user = JSON.stringify(user);
            });
        }
    }

    $scope.navigateTo = function (targetPage) {
        $state.go(targetPage);
    };

    $scope.userCheck = function() {
    	$('#filing-profile-loading-progress').show();

        if (localStorage.user) {
	        $('#filing-profile-loading-progress').hide();
	        
            $scope.showForm();
        } else {
	        $('#filing-profile-loading-progress').hide();

        	var dpObj = {
	            callback: function (val) {  //Mandatory
	                var date = new Date(val);
	                $scope.profile.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
	            },
	            from: new Date(1920, 01, 01), //Optional
	            inputDate: new Date(2017, 10, 30),      //Optional
	            mondayFirst: true,          //Optional
	            closeOnSelect: false,       //Optional
	            templateType: 'popup'       //Optional
	        };

	        $scope.openDatePickerRegister = function() {
	            ionicDatePicker.openDatePicker(dpObj);
	        };

            $('#filing-form').hide();
            $('#filing-register').show();
        }
    }

    $scope.submitProfile = function() {
        if ($scope.profile.username && $scope.profile.password && $scope.profile.repassword && $scope.profile.name && $scope.profile.phone && $scope.profile.email && $scope.profile.address && $scope.profile.birthday) {
        	if ($scope.profile.password != $scope.profile.repassword) {
        		$scope.showAlert('warning3');
        	} else {
	        	if ($scope.profile.username.match(/^[a-zA-Z0-9.\-_]+$/)) {
	        		$ionicLoading.show({
	                    template: 'Registering..'
	                });

		        	var deviceInfo = ionic.Platform.device();

		            var data = {
		            	username: $scope.profile.username,
		            	pass: $scope.profile.password,
		                name: $scope.profile.name,
		                phone: $scope.profile.phone,
		                email: $scope.profile.email,
		                city: $scope.profile.city,
		                address: $scope.profile.address,
                        uuid: deviceInfo.uuid,
                        device: deviceInfo.model
                        //uuid: '5b030556347f72ed',
                        //device: 'SM-A520F'
		            }

		            RequestService.save(data, 'userdevice').then(function(response) {
		            	$ionicLoading.hide();

	                    $scope.showAlert('success2', 'Terima Kasih telah melakukan registrasi', $scope.profile);
		            });
		        } else {
		        	$scope.showAlert('warning5');
		        }
        	}
        } else {
            $scope.showAlert('warning');
        }
    }

    $scope.doLogin = function() {
        if ($scope.login.username && $scope.login.password) {
            var data = {
                username: $scope.login.username,
                pass: $scope.login.password
            }

            $ionicLoading.show({
                template: 'Logging in..'
            });
            
            RequestService.login(data).then(function(response) {
                $ionicLoading.hide();

                if (response.status == 'error') {
                    $scope.showAlert('warning4', response.message);
                } else {
                    $scope.showAlert('success3', 'Login berhasil.', response);
                }
            });
        } else {
            $scope.showAlert('warning');
        }
    }

    $scope.showForm = function() {
    	RequestService.getall('product').then(function(types) {
            for (var ii = 0; ii < types.length; ii++) {
                $scope.types.push({value: types[ii].id, label: types[ii].name});
            }
        });

        RequestService.getallnopaging('branch').then(function(branchs) {
            for (var ii = 0; ii < branchs.length; ii++) {
                $scope.branchs.push({ 'value' : branchs[ii].id, 'label': branchs[ii].name });
            }
        });

		// $scope.types = [{
		// 	value: 'Griya iB Hasanah (Rumah, Tanah, Renovasi)', 
		// 	label: 'Griya iB Hasanah (Rumah, Tanah, Renovasi)'
		// }, {
		// 	value: 'OTO iB Hasanah', 
		// 	label: 'OTO iB Hasanah'
		// }];

		$scope.facilities = [{
			value: 'Pembelian',
			label: 'Pembelian'
		}, {
			value: 'Pembangunan',
			label: 'Pembangunan'
		}, {
			value: 'Renovasi',
			label: 'Renovasi'
		}, {
			value: 'Take Over',
			label: 'Take Over'
		}, {
			value: 'Pembelian Kavling',
			label: 'Pembelian Kavling'
		// }, {
		// 	value: 'Roda 4 Baru',
		// 	label: 'Roda 4 Baru'
		// }, {
		// 	value: 'Roda 4 Second', 
		// 	label: 'Roda 4 Second'
		}];

		$scope.genders = [{
			value: 'Laki-laki',
			label: 'Laki-laki'
		}, {
			value: 'Perempuan',
			label: 'Perempuan'
		}];

		$scope.jobs = [{
			value: 'Karyawan',
			label: 'Karyawan'
		}, {
			value: 'Wiraswasta',
			label: 'Wiraswasta'
		}, {
			value: 'Professional',
			label: 'Professional'
		}];

		$scope.company_types = [{
			value: 'Perusahaan Swasta Nasional',
			label: 'Perusahaan Swasta Nasional'
		}, {
			value: 'Perusahaan Swasta Asing',
			label: 'Perusahaan Swasta Asing'
		}, {
			value: 'Pemerintahan',
			label: 'Pemerintahan'
		}, {
			value: 'BUMN',
			label: 'BUMN'
		}, {
			value: 'BUMD',
			label: 'BUMD'
		}, {
			value: 'TNI/Polri',
			label: 'TNI/Polri'
		}, {
			value: 'Perusahaan Perorangan',
			label: 'Perusahaan Perorangan'
		}];

		$scope.job_positions = [{
			value: 'Di Bawah Supervisor',
			label: 'Di Bawah Supervisor'
		}, {
			value: 'Supervisor Ke Atas',
			label: 'Supervisor Ke Atas'
		}, {
			value: 'Dosen/Guru',
			label: 'Dosen/Guru'
		}, {
			value: 'Wiraswasta',
			label: 'Wiraswasta'
		}, {
			value: 'Profesional',
			label: 'Profesional'
		}, {
			value: 'Pensiunan',
			label: 'Pensiunan'
		}, {
			value: 'Tidak Bekerja',
			label: 'Tidak Bekerja'
		}];

		$scope.business_fields = [{
			value: 'Konstruksi',
			label: 'Konstruksi'
		}, {
			value: 'Industri Pengolahan',
			label: 'Industri Pengolahan'
		}, {
			value: 'Jasa-jasa Dunia Usaha',
			label: 'Jasa-jasa Dunia Usaha'
		}, {
			value: 'Jasa-jasa Sosial Masyarakat',
			label: 'Jasa-jasa Sosial Masyarakat'
		}, {
			value: 'Listrik, Gas dan Air',
			label: 'Listrik, Gas dan Air'
		}, {
			value: 'Pengangkutan, Pergudangan dan Komunikasi',
			label: 'Pengangkutan, Pergudangan dan Komunikasi'
		}, {
			value: 'Perdagangan, Restoran dan Hotel',
			label: 'Perdagangan, Restoran dan Hotel'
		}, {
			value: 'Pertambangan',
			label: 'Pertambangan'
		}, {
			value: 'Pertanian, Perburuan dan Sarana Pertanian',
			label: 'Pertanian, Perburuan dan Sarana Pertanian'
		}];

		$scope.contact_options = [{
			value: 'No. Handphone',
			label: 'No. Handphone'
		}, {
			value: 'Telp. Kantor',
			label: 'Telp. Kantor'
		}];

        $('#filing-form').show();
    }

    $scope.doRefresh = function() {
    	$scope.userCheck();

    	$scope.$broadcast('scroll.refreshComplete');
    }

    $scope.showLogin = function() {
        $('#filing-register').hide();
        $('#filing-login').show();
    }

    $scope.showRegister = function() {
        $('#filing-register').show();
        $('#filing-login').hide();
    }

	$scope.init();
});