appCtrl.controller('homeCtrl', function($ionicPopup, $scope, $ionicSlideBoxDelegate, RequestService, TimeService) {
	//if(typeof analytics !== undefined) { analytics.trackView("Home"); }

    $scope.baseUrl = RequestService.urlImageLink;
	
	$scope.init = function() {
		$scope.slides = [];
		$scope.quick_menus = [];

		$scope.getData();
		// $scope.showPopup();
	}

	$scope.showPopup = function() {
		$scope.data = {};
	  
		// An elaborate, custom popup
		var myPopup = $ionicPopup.show({
			template: '<input type="text" ng-model="data.ip">',
			title: 'Enter IP',
			subTitle: 'Please insert IP',
			scope: $scope,
			buttons: [
			{
				text: '<b>Save</b>',
				type: 'button-positive',
				onTap: function(e) {
				if (!$scope.data.ip) {
					//don't allow the user to close unless he enters wifi password
					e.preventDefault();
				} else {
					$scope.data.ip;
					RequestService.seturl($scope.data.ip);
					$scope.getData();
				}
				}
			}
			]
		});
	}
	
	$scope.getData = function() {
		RequestService.getall('home').then(function(slides) {
			for (var ii = 0; ii < slides.length; ii++) {
				var date = new Date(slides[ii].time);
				slides[ii].date = date.getDate() + ' ' + TimeService.months[date.getMonth()];

				$scope.slides.push(slides[ii]);
			}

			$('#home-loading-progress').hide();
			$('#home-slide-box').show();

			$ionicSlideBoxDelegate.update();
		});
		
		RequestService.getall('quickmenu').then(function(quick_menus) {
			for (var ii = 0; ii < quick_menus.length; ii++) {
				$scope.quick_menus.push(quick_menus[ii]);
			}
		});
	}

	$scope.init();
});