appCtrl.controller('merchantCtrl', function($scope, $state, $sce, $filter, RequestService) {
	//if(typeof analytics !== undefined) { analytics.trackView("Merchant"); }

    $scope.baseUrl = RequestService.urlImageLink;
    
    $scope.init = function() {
      $scope.merchants = [];

      $scope.getData();
  }

  $scope.getData = function() {
    RequestService.getall('merchant').then(function(merchants) {
        for (var ii = 0; ii < merchants.length; ii++) {
            $scope.merchants.push(merchants[ii]);
        }

        $('#merchant-loading-progress').hide();
        $('#merchant-content').show();
    });
}

$scope.doRefresh = function() {
    $scope.merchants = [];
    
    RequestService.getall('merchant').then(function(merchants) {
        for (var ii = 0; ii < merchants.length; ii++) {
            $scope.merchants.push(merchants[ii]);
        }
    })

    .finally(function() {
        $scope.$broadcast('scroll.refreshComplete');
    });
}

$scope.navigateTo = function (targetPage, objectData) {
    $state.go(targetPage, {
        merchant: objectData
    });
};

$scope.init();
});

appCtrl.controller('merchantDetailCtrl', function($scope, $stateParams, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Merchant Detail"); }

    $scope.baseUrl = RequestService.urlImageLink;
    $scope.init = function() {
      $scope.merchant = $stateParams.merchant;
  }

  $scope.init();
});