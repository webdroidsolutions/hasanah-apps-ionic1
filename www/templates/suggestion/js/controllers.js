appCtrl.controller('suggestionCtrl', function($scope, $state, RequestService, TimeService, $ionicPopup, $ionicHistory) {
	//if(typeof analytics !== undefined) { analytics.trackView("Suggestion"); }

    $scope.init = function() {
		$scope.suggestion = [];
	}

    $scope.submitSuggestion = function() {
        if ($scope.suggestion.name && $scope.suggestion.phone && $scope.suggestion.email && $scope.suggestion.suggestion) {
            var data = {
                name: $scope.suggestion.name,
                phone: $scope.suggestion.phone,
                email: $scope.suggestion.email,
                suggestion: $scope.suggestion.suggestion
            }

            RequestService.save(data, 'suggestion').then(function(response) {
                $scope.showAlert('success');
            });
        } else {
            $scope.showAlert('warning');
        }
    }

    $scope.showAlert = function(type) {
        if (type == 'warning') {
            title = 'Pesan';
            template = 'Harap lengkapi data.';
        } else if (type == 'success') {
            title = 'Terima Kasih';
            template = 'Terima kasih atas saran Anda.';
        }

        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template
        });

        if (type == 'success') {
            alertPopup.then(function(res) {
                $scope.suggestion = [];
            });
        }
    }

	$scope.init();
});
