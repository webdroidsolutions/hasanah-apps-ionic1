appCtrl.controller('profiledetailCtrl', function($scope, $ionicPopup, $ionicActionSheet, $ionicLoading, TimeService, RequestService, ionicDatePicker) {
    //if(typeof analytics !== undefined) { analytics.trackView("Profile Detail"); }

    $scope.init = function() {
          $scope.profile = [];
          $scope.user = [];
          $scope.login = [];

          $scope.userCheck();

          var dpObj = {
              callback: function (val) {  //Mandatory
                  var date = new Date(val);
                  $scope.profile.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
              },
              from: new Date(1920, 01, 01), //Optional
              inputDate: new Date(2017, 10, 30),      //Optional
              mondayFirst: true,          //Optional
              closeOnSelect: false,       //Optional
              templateType: 'popup'       //Optional
          };


          $scope.openDatePicker = function() {
              ionicDatePicker.openDatePicker(dpObj);
          };
    }

    $scope.userCheck = function() {
        if (localStorage.user) {
            $scope.user = JSON.parse(localStorage.user);
        } else {
            $scope.user = [];
        }
    }







	$scope.init();
});
