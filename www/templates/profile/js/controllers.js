appCtrl.controller('profileCtrl', function( $ionicPlatform,$ionicHistory, $state, $stateParams, $scope, $ionicPopup, $ionicActionSheet, $ionicLoading, TimeService, RequestService, ionicDatePicker, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $cordovaActionSheet) {
    //if(typeof analytics !== undefined) { analytics.trackView("Profile"); }

    $scope.init = function() { 
        console.log($stateParams.umroh);
        $scope.profile = [];
        $scope.user = [];
        $scope.login = [];
        $scope.var = [];
        $scope.userCheck();
        $scope.var = [];
        $scope.var.uri = '';
        var dpObj = {
            callback: function (val) {  //Mandatory
                var date = new Date(val);
                $scope.profile.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
            },
            from: new Date(1920, 01, 01), //Optional
            inputDate: new Date(2017, 10, 30),      //Optional
            mondayFirst: true,          //Optional
            closeOnSelect: false,       //Optional
            templateType: 'popup'       //Optional
        };

        $scope.openDatePicker = function() {
            ionicDatePicker.openDatePicker(dpObj);
        };

    }



    $scope.userCheck = function() {
        // var deviceInfo = ionic.Platform.device();

        // // data = { uuid: deviceInfo.uuid };
        // data = { uuid: '123123' };

        // RequestService.getby(data, 'userdevice', 'uuid').then(function(response) {
        //     $('#profile-loading-progress').hide();

        //     if (response.length > 0) {
        //         $scope.user = response[0];

        //         $('#profile-info').show();
        //     } else {
        //         $('#profile-register').show();
        //     }
        // });

        if (localStorage.user) {
            // $scope.user.img = 'img/man-avatar.png';
            $scope.user = JSON.parse(localStorage.user);
            if($scope.user.img == null || $scope.user.img == undefined) {
                $scope.user.img = 'img/man-avatar.png';
            }
            $('#profile-info').show();
            //profile-login
            $('#profile-login').hide();
            
        } else {
            $scope.user = [];

            $('#profile-info').hide();
            $('#lupa-password-container').hide();
            $('#profile-login').show();
        }
    }

    $scope.submitProfile = function() {

       if ($scope.profile.username && $scope.profile.password && $scope.profile.repassword && $scope.profile.name && $scope.profile.phone && $scope.profile.email && $scope.profile.address && $scope.profile.birthday) {

        if ($scope.profile.password.length < 5 || $scope.profile.repassword.length < 5)
        {
            $scope.showAlert2('Error', 'Password Minimal 5 Karakter');
        }
        else if ($scope.profile.password != $scope.profile.repassword) {
            $scope.showAlert('warning2');
        } else {
            if ($scope.profile.username.match(/^[a-zA-Z0-9.\-_]+$/)) {
                $ionicLoading.show({
                    template: 'Registering..'
                });

                var deviceInfo = ionic.Platform.device();

                var data = {
                    'username': $scope.profile.username,
                    'pass': $scope.profile.password,
                    'name': $scope.profile.name,
                    'phone': $scope.profile.phone,
                    'email': $scope.profile.email,
                    'city': $scope.profile.city,
                    'address': $scope.profile.address,
                        uuid: deviceInfo.uuid,
                        device: deviceInfo.model
                        //uuid: '5b030556347f72ed',
                        //device: 'SM-A520F'
                    }
                    RequestService.save(data, 'userdevice').then(function(response) {

                        // $scope.showAlert('success', 'Terima Kasih telah melakukan registrasi', $scope.profile);
                        if(response.data.status == 'error') {
                            $ionicLoading.hide();
                            $scope.showAlert2(response.data.status, response.data.message);
                        } else {
                            //$scope.showAlert('success', 'Terima Kasih telah melakukan registrasi', $scope.profile);
                            var data = {
                                'username': $scope.profile.username,
                                'pass': $scope.profile.password
                            }
                            RequestService.login(data).then(function(response) {
                                $ionicLoading.hide();
                                // $('#profile-register').hide();
                                
                                $scope.showAlert('success3', 'Terimakasih Telah Melakukan Registrasi', response);
                                
                                
                            });
                        }
                    });
                } else {
                    $scope.showAlert('warning4');
                }
            }
        } else {
            $scope.showAlert('warning');
        }
    }

    $scope.showAlert2 = function(title, msg) {
        var alertPopup = $ionicPopup.alert({
          title: title,
          template: msg
      });
    };

    $scope.showAlert = function(type, msg, user) {
        if (type == 'warning') {
            title = 'Pesan';
            template = 'Harap lengkapi data.';
        } else if (type == 'warning2') {
            title = 'Pesan';
            template = 'Password tidak sama.';
        } else if (type == 'warning3') {
            title = 'Pesan';
            template = msg;
        } else if (type == 'warning4') {
            title = 'Pesan';
            template = 'Username tidak boleh mengandung spasi dan karakter simbol';
        } else {
            title = 'Terima Kasih';
            template = msg;
        }

        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template
        });

        if (type == 'success') {
            alertPopup.then(function(res) {
                $scope.profile = [];

                $('#profile-loading-progress').show();

                $('#profile-register').hide();

                var data = {
                    username: user.username,
                    pass: user.password
                };

                RequestService.login(data).then(function(response) {

                    $scope.user = response;

                    $('#profile-loading-progress').hide();

                    $('#profile-info').show();

                    localStorage.user = JSON.stringify(response);
                    if($stateParams.umroh != null)
                    {
                        $ionicHistory.goBack();
                    }
                });
            });
        } else if (type == 'success2') {
            alertPopup.then(function(res) {
                $scope.login = [];

                $('#profile-loading-progress').show();

                $('#profile-login').hide();

                $scope.user = user;
                if($scope.user.img == null || $scope.user.img == undefined) {
                    $scope.user.img = 'img/man-avatar.png';
                }
                $('#profile-loading-progress').hide();

                $('#profile-info').show();

                localStorage.user = JSON.stringify(user);
                if($stateParams.umroh != null)
                {
                    $ionicHistory.goBack();   
                }
            });
        }
        else if (type == 'success3') {
            alertPopup.then(function(res) {
                $scope.login = [];

                $('#profile-loading-progress').show();

                $('#profile-register').hide();

                $scope.user = user;
                if($scope.user.img == null || $scope.user.img == undefined) {
                    $scope.user.img = 'img/man-avatar.png';
                }
                $('#profile-loading-progress').hide();

                $('#profile-info').show();

                localStorage.user = JSON.stringify(user);
                if($stateParams.umroh != null)
                {
                    $ionicHistory.goBack();   
                }
            });
        }
    }

    $scope.showLogin = function() {
        //

        $('#profile-login').removeClass('animated fadeOutLeft');
        $('#profile-register').removeClass('animated fadeInRight');
        $('#profile-register').addClass('animated fadeOutLeft');
        setTimeout(function(){
          $('#profile-register').hide();
          $('#profile-login').addClass('animated fadeInRight');
          setTimeout(function(){
              $('#profile-login').show();
          }, 100);
      }, 600);
    }

    $scope.showRegister = function() {

        $('#profile-login').removeClass('animated fadeInRight');
        $('#profile-register').removeClass('animated fadeOutLeft');
        $('#profile-login').addClass('animated fadeOutLeft');
        setTimeout(function(){
          $('#profile-login').hide();
          $('#profile-register').addClass('animated fadeInRight');
          setTimeout(function(){
              $('#profile-register').show();
          }, 100);
      }, 600);
    }

    $scope.showLupaPassword = function() {

        $('#profile-login').removeClass('animated fadeInRight');
        $('#lupa-password-container').removeClass('animated fadeOutLeft');
        $('#profile-login').addClass('animated fadeOutLeft');
        setTimeout(function(){
          $('#profile-login').hide();
          $('#lupa-password-container').addClass('animated fadeInRight');
          setTimeout(function(){
              $('#lupa-password-container').show();
          }, 100);
      }, 600);
    }

    $scope.showProfileDetail = function() {


    }

    $scope.resetPassword = function() {
      if ($scope.login.username) {
          var data = {
              'username': $scope.login.username
          }


          $ionicLoading.show({
              template: 'Sending Password Ke Email'
          });

          RequestService.forgotpassword(data).then(function(response) {
              $ionicLoading.hide();
              if (response.status == 'error') {
                $scope.showAlert('warning3', response.message);
                $('#profile-login').removeClass('animated fadeOutLeft');
                $('#lupa-password-container').removeClass('animated fadeInRight');
                $('#lupa-password-container').addClass('animated fadeOutLeft');
                setTimeout(function(){
                  $('#lupa-password-container').hide();
                  $('#profile-login').addClass('animated fadeInRight');
                  setTimeout(function(){
                      $('#profile-login').show();
                  }, 100);
              }, 600);
            }

        });

      } else {
          $scope.showAlert('warning');
      }
  }

  $scope.cancelResetPassword = function() {

    $('#profile-login').removeClass('animated fadeOutLeft');
    $('#lupa-password-container').removeClass('animated fadeInRight');
    $('#lupa-password-container').addClass('animated fadeOutLeft');
    setTimeout(function(){
      $('#lupa-password-container').hide();
      $('#profile-login').addClass('animated fadeInRight');
      setTimeout(function(){
          $('#profile-login').show();
      }, 100);
  }, 600);
}

$scope.doLogin = function() {
    if ($scope.login.username && $scope.login.password) {
        var data = {
            username: $scope.login.username,
            pass: $scope.login.password
        }

        $ionicLoading.show({
            template: 'Logging in..'
        });

        RequestService.login(data).then(function(response) {
            $ionicLoading.hide();

            if (response.status == 'error') {
                $scope.showAlert2(response.status, response.message);
            } else {
                $scope.showAlert('success2', 'Login berhasil.', response);
            }
        });
    } else {
        $ionicLoading.hide();
        $scope.showAlert('warning');
    }
}

$scope.showLogoutMenu = function() {
    var hideSheet = $ionicActionSheet.show({
        destructiveText: 'Logout',
        titleText: 'Anda yakin ingin logout?',
        cancelText: 'Cancel',
        cancel: function() {},
        buttonClicked: function(index) {
            return true;
        },
        destructiveButtonClicked: function(){
                // $ionicLoading.show({
                //     template: 'Logging out..'
                // });

                // // Google logout
                // window.plugins.googleplus.logout(
                //     function (msg) {
                //         console.log(msg);
                //         $ionicLoading.hide();
                //         $state.go('login');
                //     },
                //     function(fail){
                //         console.log(fail);
                //     }
                // );

                localStorage.user = '';
                $scope.user = [];
                $('#profile-info').hide();
                $('#profile-login').removeClass('animated fadeOutLeft');
                setTimeout(function(){
                    $('#profile-login').show();
                }, 800);
                // $scope.userCheck();

                // $ionicLoading.hide();

                return true;
            }
        });
};

$scope.browseFile = function(){

}

$scope.loadImage = function() {
    var options = {
        title: 'Select Image Source',
        buttonLabels: ['Load from Library', 'Use Camera'],
        addCancelButtonWithLabel: 'Cancel',
        androidEnableCancelButton : true,
    };
    $cordovaActionSheet.show(options).then(function(btnIndex) {
        var type = null;
        if (btnIndex === 1) {
            type = Camera.PictureSourceType.PHOTOLIBRARY;
        } else if (btnIndex === 2) {
            type = Camera.PictureSourceType.CAMERA;
        }
        if (type !== null) {
            $scope.selectPicture(type);
        }
    });
};

$scope.selectPicture = function(sourceType) {
    var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        saveToPhotoAlbum: false
    };

    $cordovaCamera.getPicture(options).then(function(imagePath) {
            // Grab the file name of the photo in the temporary directory
            var currentName = imagePath.replace(/^.*[\\\/]/, '');

            //Create a new name for the photo
            var d = new Date(),
            n = d.getTime(),
            // newFileName =  n + "tes.jpg";
            newFileName = "tes.jpg";

            // If you are trying to load image from the gallery on Android we need special treatment!
            if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
                window.FilePath.resolveNativePath(imagePath, function(entry) {
                    window.resolveLocalFileSystemURL(entry, success, fail);
                    function fail(e) {
                        console.error('Error: ', e);
                    }

                    function success(fileEntry) {
                        var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                    // Only copy because of access rights
                    $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                        $scope.image = newFileName;
                        setTimeout(function(){
                            $scope.uploadImage();
                        }, 800);
                    }, function(error){
                        $scope.showAlert2('Error', error.exception);
                    });
                };
            }
            );
            } else {
                var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                // Move the file to permanent storage
                $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
                    $scope.image = newFileName;
                    setTimeout(function(){
                        $scope.uploadImage();
                    }, 800);
                }, function(error){
                    $scope.showAlert2('Error', error.exception);
                });
            }
        },
        function(err){
            // Not always an error, maybe cancel was pressed...
        })
};

$scope.pathForImage = function(image) {
    if (image === null) {
      return '';
  } else {
      return cordova.file.dataDirectory + image;
  }
};


$scope.uploadImage = function() {
        // Destination URL
        
        
            // File for Upload
            var targetPath = $scope.pathForImage($scope.image);
            
            // File name only
            var filename = $scope.image;;
            
            var options = {
                fileKey: "file",
                fileName: filename,
                chunkedMode: false,
                mimeType: "multipart/form-data",
                params : {'fileName': filename}
            };
            $ionicLoading.show({
                template: 'Uploading ...'
            });
            $scope.var.uri = RequestService.getUrlUpload();
            $cordovaFileTransfer.upload($scope.var.uri + 'upload/uploadimg/', targetPath, options).then(function(result) {
                $ionicLoading.hide();
                //$scope.var.image = result.response;
                //http://YOUR_DOMAIN_NAME:8180/images/YOUR_IMAGE_NAME
                //editprofil
                $scope.var.image = result.response;
                // var data = {
                //     "id": $scope.user.id,
                //     'img' : result.response
                // }

                // RequestService.editprofil(data).then(function(response) {
                //     $ionicLoading.hide();
                // });
            }, function(error){
                $ionicLoading.hide();
                $scope.showAlert('Error', error.exception);
            });


        }
        $scope.toHistoryPage = function () {
            var data = {

            };

            $scope.navigateTo('app.history', data);
        }
        $scope.navigateTo = function (targetPage, objectData) {
            $state.go(targetPage, {
                umroh: objectData
            });
        };
        $scope.init();
    });

appCtrl.controller('profiledetailCtrl', function($scope, $ionicPopup, $ionicActionSheet, $ionicLoading, TimeService, RequestService, ionicDatePicker, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $cordovaActionSheet) {
  //if(typeof analytics !== undefined) { analytics.trackView("Profile Detail"); }

    $scope.init = function() {
    $scope.profile = [];
    $scope.user = [];
    $scope.login = [];
    $scope.var = [];


    $scope.userCheck();

    var dpObj = {
            callback: function (val) {  //Mandatory
                var date = new Date(val);
                $scope.profile.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
            },
            from: new Date(1920, 01, 01), //Optional
            inputDate: new Date(2017, 10, 30),      //Optional
            mondayFirst: true,          //Optional
            closeOnSelect: false,       //Optional
            templateType: 'popup'       //Optional
        };


        $scope.openDatePicker = function() {
            ionicDatePicker.openDatePicker(dpObj);
        };
        
    }

    $scope.userCheck = function() {
      if (localStorage.user) {
          $scope.user = JSON.parse(localStorage.user);
          if($scope.user.img == null || !$scope.user.img) {
            $scope.user.img = 'img/man-avatar.png';
        } 
    } else {
      $scope.user = [];
  }
}

$scope.showProfileDetail = function() {
    $('#profiledetail-info').removeClass('animated fadeInRight');
    $('#profiledetail-update').removeClass('animated fadeOutLeft');
    $('#profiledetail-info').addClass('animated fadeOutLeft');
    setTimeout(function(){
      $('#profiledetail-info').hide();
      $('#profiledetail-update').addClass('animated fadeInRight');
      setTimeout(function(){
          $('#profiledetail-update').show();
      }, 100);
  }, 600);
}

$scope.cancelProfileDetail = function() {

  $('#profiledetail-info').removeClass('animated fadeOutLeft');
  $('#profiledetail-update').removeClass('animated fadeInRight');
  $('#profiledetail-update').addClass('animated fadeOutLeft');
  setTimeout(function(){
    $('#profiledetail-update').hide();
    $('#profiledetail-info').addClass('animated fadeInRight');
    setTimeout(function(){
        $('#profiledetail-info').show();
    }, 100);
}, 600);
}

$scope.showUbahPassword = function() {
    $('#profiledetail-info').removeClass('animated fadeInRight');
    $('#profiledetail-password').removeClass('animated fadeOutLeft');
    $('#profiledetail-info').addClass('animated fadeOutLeft');
    setTimeout(function(){
      $('#profiledetail-info').hide();
      $('#profiledetail-password').addClass('animated fadeInRight');
      setTimeout(function(){
          $('#profiledetail-password').show();
      }, 100);
  }, 600);
}


$scope.cancelUbahPassword = function() {
    $('#profiledetail-info').removeClass('animated fadeOutLeft');
    $('#profiledetail-password').removeClass('animated fadeInRight');
    $('#profiledetail-password').addClass('animated fadeOutLeft');
    setTimeout(function(){
      $('#profiledetail-password').hide();
      $('#profiledetail-info').addClass('animated fadeInRight');
      setTimeout(function(){
          $('#profiledetail-info').show();
      }, 100);
  }, 600);
}


$scope.ubahProfileDetail = function() {
    var data = {
      "id": $scope.user.id,
      "name":$scope.user.name,
      "email": $scope.user.email,
      "phone": $scope.user.phone,
      "address": $scope.user.address
  }


  $ionicLoading.show({
      template: 'Simpan Update Data'
  });

  RequestService.editprofil(data).then(function(response) {
      $ionicLoading.hide();
      if (response.status == 'success') {
        localStorage.user = JSON.stringify($scope.user);
        $scope.showAlert('warning3', response.message);
        $('#profiledetail-info').removeClass('animated fadeOutLeft');
        $('#profiledetail-update').removeClass('animated fadeInRight');
        $('#profiledetail-update').addClass('animated fadeOutLeft');
        setTimeout(function(){
          $('#profiledetail-update').hide();
          $('#profiledetail-info').addClass('animated fadeInRight');
          setTimeout(function(){
              $('#profiledetail-info').show();
          }, 100);
      }, 600);
    }
    else
    {
        $scope.showAlert('warning3', response.message);
    }

});
}


$scope.showAlert = function(type, msg, user) {
  if (type == 'warning') {
      title = 'Pesan';
      template = 'Harap lengkapi data.';
  } else if (type == 'warning2') {
      title = 'Pesan';
      template = 'Password tidak sama.';
  } else if (type == 'warning3') {
      title = 'Pesan';
      template = msg;
  } else if (type == 'warning4') {
      title = 'Pesan';
      template = 'Username tidak boleh mengandung spasi dan karakter simbol';
  } else {
      title = 'Terima Kasih';
      template = msg;
  }

  var alertPopup = $ionicPopup.alert({
      title: title,
      template: template
  });

  if (type == 'success') {
      alertPopup.then(function(res) {
          $scope.profile = [];

          $('#profile-loading-progress').show();

          $('#profile-register').hide();

          var data = {
              username: user.username,
              pass: user.password
          };

          RequestService.login(data).then(function(response) {

              $scope.user = response;

              $('#profile-loading-progress').hide();

              $('#profile-info').show();

              localStorage.user = JSON.stringify(response);
                  //$scope.var.image = 'img/man-avatar.png';
              });
      });
  } else if (type == 'success2') {
      alertPopup.then(function(res) {
          $scope.login = [];

          $('#profile-loading-progress').show();

          $('#profile-login').hide();

          $scope.user = user;

          $('#profile-loading-progress').hide();

          $('#profile-info').show();

          localStorage.user = JSON.stringify(user);
      });
  }
}

$scope.showAlert2 = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
        title: title,
        template: msg
    });
};
$scope.ubahPassword = function() {
    if ($scope.var.userpassnew.length < 5 || $scope.var.userpassconfirm.length < 5 || $scope.var.userpassold.length < 5)
    {
        $scope.showAlert2('Error', 'Password Minimal 5 Karakter');
    }
    else if ($scope.var.userpassnew != $scope.var.userpassconfirm) {
        $scope.showAlert2('Error', 'Password Dan Konfirmasi Password Tidak Sama');
    }
    else if ($scope.var.userpassnew == $scope.var.userpassconfirm) {
        var data = {
            'id': $scope.user.id,
            'pass': $scope.var.userpassnew,
            'old_pass' : $scope.var.userpassold
        }


        $ionicLoading.show({
            template: 'Updating Password'
        });

        RequestService.changepassword(data).then(function(response) {
            $ionicLoading.hide();
            if(response.status == 'error') {
                $scope.showAlert(response.status, 'Gagal Merubah Password');
            } else {
                $scope.showAlert(response.status, response.message);
                $('#profiledetail-info').removeClass('animated fadeOutLeft');
                $('#profiledetail-password').removeClass('animated fadeInRight');
                $('#profiledetail-password').addClass('animated fadeOutLeft');
                setTimeout(function(){
                  $('#profiledetail-password').hide();
                  $('#profiledetail-info').addClass('animated fadeInRight');
                  setTimeout(function(){
                      $('#profiledetail-info').show();
                  }, 100);
              }, 600);
            }
            
            

        });

    } else {
        $scope.showAlert('warning3', 'Password Baru Dan Konfirmasi Password Tidak Sama');
    }
}


$scope.loadImage = function() {
    var options = {
        title: 'Select Image Source',
        buttonLabels: ['Load from Library', 'Use Camera'],
        addCancelButtonWithLabel: 'Cancel',
        androidEnableCancelButton : true,
    };
    $cordovaActionSheet.show(options).then(function(btnIndex) {
        var type = null;
        if (btnIndex === 1) {
            type = Camera.PictureSourceType.PHOTOLIBRARY;
        } else if (btnIndex === 2) {
            type = Camera.PictureSourceType.CAMERA;
        }
        if (type !== null) {
            $scope.selectPicture(type);
        }
    });
};

$scope.selectPicture = function(sourceType) {
    var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        saveToPhotoAlbum: false
    };
    
    $cordovaCamera.getPicture(options).then(function(imagePath) {
        // Grab the file name of the photo in the temporary directory
        var currentName = imagePath.replace(/^.*[\\\/]/, '');

        //Create a new name for the photo
        var d = new Date(),
        n = d.getTime(),
        // newFileName =  n + "tes.jpg";
        newFileName = "tes.jpg";

        // If you are trying to load image from the gallery on Android we need special treatment!
        if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
            window.FilePath.resolveNativePath(imagePath, function(entry) {
                window.resolveLocalFileSystemURL(entry, success, fail);
                function fail(e) {
                    console.error('Error: ', e);
                }

                function success(fileEntry) {
                    var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                // Only copy because of access rights
                $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                    $scope.image = newFileName;
                    setTimeout(function(){
                        $scope.uploadImage();
                    }, 800);
                }, function(error){
                    $scope.showAlert2('Error', error.exception);
                });
            };
        }
        );
        } else {
            var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            // Move the file to permanent storage
            $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
                $scope.image = newFileName;
                setTimeout(function(){
                    $scope.uploadImage();
                }, 800);
            }, function(error){
                $scope.showAlert2('Error', error.exception);
            });
        }
    },
    function(err){
        // Not always an error, maybe cancel was pressed...
    })
};

$scope.pathForImage = function(image) {
    if (image === null) {
      return '';
  } else {
      return cordova.file.dataDirectory + image;
  }
};


$scope.uploadImage = function() {
    // Destination URL
    
    
        // File for Upload
        var targetPath = $scope.pathForImage($scope.image);
        
        // File name only
        var filename = $scope.image;;
        
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {'fileName': filename}
        };
        $ionicLoading.show({
            template: 'Uploading ...'
        });
        $scope.var.uri = RequestService.getUrlUpload();
        $cordovaFileTransfer.upload($scope.var.uri + 'upload/uploadimg/', targetPath, options).then(function(result) {

            //$scope.var.image = result.response;
            //http://YOUR_DOMAIN_NAME:8180/images/YOUR_IMAGE_NAME
            //editprofil
            $scope.var.image = result.response;
            var data = {
                "id": $scope.user.id,
                'img' : result.response,
                'address' : $scope.user.address,
                'email' : $scope.user.email,
                'name' : $scope.user.name,
                'phone' : $scope.user.phone
            }

            RequestService.editprofil(data).then(function(response) {
                $scope.var.image = RequestService.getUrlImg()+$scope.var.image;
                $scope.user.img = $scope.var.image;
                localStorage.user = JSON.stringify($scope.user);
                $ionicLoading.hide();
            });
        }, function(error){
            $ionicLoading.hide();
            $scope.showAlert('Error', error.exception);
        });


    }


    
    $scope.init();
});





appCtrl.controller('inboxCtrl', function($state, $scope, $ionicPopup, $ionicActionSheet, $ionicLoading, TimeService, RequestService, ionicDatePicker) {
//if(typeof analytics !== undefined) { analytics.trackView("Inbox"); }

      $scope.init = function() {
    $scope.profile = [];
    $scope.user = [];
    $scope.login = [];
    $scope.var = [];
    $scope.userCheck();

    var dpObj = {
            callback: function (val) {  //Mandatory
                var date = new Date(val);
                $scope.profile.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
            },
            from: new Date(1920, 01, 01), //Optional
            inputDate: new Date(2017, 10, 30),      //Optional
            mondayFirst: true,          //Optional
            closeOnSelect: false,       //Optional
            templateType: 'popup'       //Optional
        };

        $scope.sentItem = [];
        
        var data = {
            "userdevice_id": $scope.user.id
        }

        RequestService.getMessage(data).then(function(response) {
            console.log(response);
            $scope.sentItem = response;
            var i = 0;
            $scope.var.jmlInbox = 0;
            $scope.var.jmSent = 0;
            while (i < $scope.sentItem.length) {
                $scope.sentItem[i].create_date = moment($scope.sentItem.create_date).format('DD MMM YYYY - HH:mm');
                if($scope.sentItem[i].reply_date != null) {
                    $scope.sentItem[i].reply_date = moment($scope.sentItem[i].reply_date).format('DD MMM YYYY - HH:mm');
                }

                if($scope.sentItem[i].reply == null){
                    $scope.var.jmlSent++;
                } else {
                    $scope.var.jmlInbox++;
                }
                i++;
            }
            
        });

        $scope.openDatePicker = function() {
            ionicDatePicker.openDatePicker(dpObj);
        };



    }

    $scope.newMessage = function() {
        $scope.navigateTo('app.message-send');
    }

    $scope.navigateTo = function (targetPage) {
        $state.go(targetPage);
    };

    $scope.userCheck = function() {
      if (localStorage.user) {
          $scope.user = JSON.parse(localStorage.user);

      } else {
          $scope.user = [];
      }
  }

  $scope.init();
});



appCtrl.controller('messageSendCtrl', function($stateParams, $ionicHistory, $state, $scope, $ionicPopup, $ionicActionSheet, $ionicLoading, TimeService, RequestService, ionicDatePicker) {
    //if(typeof analytics !== undefined) { analytics.trackView("Message Send"); }

    $scope.showAlert2 = function(title, msg) {
        var alertPopup = $ionicPopup.alert({
          title: title,
          template: msg
      });
    };

    $scope.init = function() {

      $scope.profile = [];
      $scope.user = [];
      $scope.var = [];
      $scope.login = [];
      $scope.umroh = $stateParams.umroh;

      $scope.var.to = null;
      if($scope.umroh == null) {
        $scope.var.to = 'Administrator';
    } else {
        $scope.var.to = $scope.umroh.travelagent;
    }
    $scope.userCheck();






    $scope.openDatePicker = function() {
      ionicDatePicker.openDatePicker(dpObj);
  };


  

}

    // RequestService.saveorder(data).then(function(response) {
    //     var temp;
    //     $ionicLoading.hide();

    // });

    $scope.kirimPesan = function() {

        if($scope.umroh == null)
        {
            var data = {
                "userdevice_id": $scope.user.id,
                "customer_name": $scope.user.name,
                "description": $scope.var.message,
                "subject": $scope.var.subject,
                'travelagent_id' : null
            }
        } else if($scope.umroh.umroh_id !== undefined) {
            var data = {
                "userdevice_id": $scope.user.id,
                "customer_name": $scope.user.name,
                "description": $scope.var.message,
                "subject": $scope.var.subject,
                "umroh_id" : $scope.umroh.umroh_id
            }
        } else {

            var data = {
                "userdevice_id": $scope.user.id,
                "customer_name": $scope.user.name,
                "description": $scope.var.message,
                "subject": $scope.var.subject,
                "travelagent_id" : $scope.umroh.travelagent_id
            }
        }
        
        
        if($scope.var.subject == undefined || $scope.var.message == undefined) {
            $scope.showAlert('Subject Atau Pesan Tidak Boleh Kosong');
            
        } else {
            $ionicLoading.show({
                template: 'Loading..'
            });

            RequestService.sendMessage(data).then(function(response) {
                var temp;
                $ionicLoading.hide();
                $scope.showAlert2(response.status, response.message);
                $ionicHistory.goBack();
                
            });  
        }  
    }
    
    $scope.navigateTo = function (targetPage) {
        $state.go(targetPage);
    };
    
    $scope.userCheck = function() {
        if (localStorage.user) {
            $scope.user = JSON.parse(localStorage.user);

        } else {
            $scope.user = [];
        }
    }

    $scope.showAlert = function(pesan) {
        var alertPopup = $ionicPopup.alert({
          title: 'Error',
          template: pesan
      });
    };

    $scope.init();
});


appCtrl.controller('historyCtrl', function($state, $scope, $ionicPopup, $ionicActionSheet, $ionicLoading, TimeService, RequestService, ionicDatePicker) {
  //if(typeof analytics !== undefined) { analytics.trackView("History"); }

    $scope.init = function() {
    $scope.profile = [];
    $scope.user = [];
    $scope.login = [];
    $scope.history = [];
    $scope.userCheck();
    $scope.user = JSON.parse(localStorage.user);
    $scope.var = [];
    var dpObj = {
            callback: function (val) {  //Mandatory
                var date = new Date(val);
                $scope.profile.birthday = date.getDate() + ' ' + TimeService.months[date.getMonth()] + ' ' + date.getFullYear();
            },
            from: new Date(1920, 01, 01), //Optional
            inputDate: new Date(2017, 10, 30),      //Optional
            mondayFirst: true,          //Optional
            closeOnSelect: false,       //Optional
            templateType: 'popup'       //Optional
        };


        $scope.openDatePicker = function() {
            ionicDatePicker.openDatePicker(dpObj);
        };

        var data = {
            "userdevice_id": $scope.user.id
        }

        $ionicLoading.show({
            template: 'Loading..'
        });
        RequestService.getHistoryTransaksi(data).then(function(response) {
            $ionicLoading.hide();
            $scope.history = response;
            
            var i = 0;
            $scope.var.jmlTab1 = 0;
            $scope.var.jmlTab2 = 0;
            $scope.var.jmlTab3 = 0;
            $scope.var.jmlTab4 = 0;
            while (i < $scope.history.length) {
                $scope.history[i].date = moment($scope.history[i].date).format('YYYY MMMM DD');
                if($scope.history[i].status == 1 || $scope.history[i].status == 2) {
                    $scope.var.jmlTab1++;
                } else if ($scope.history[i].status == 3 || $scope.history[i].status == 4) {
                    $scope.var.jmlTab2++;
                } else if ($scope.history[i].status == 5) {
                    $scope.var.jmlTab3++;
                } else {
                    $scope.var.jmlTab4++;
                }
                i++;
            }
        });

        $scope.navigateTo = function (targetPage, objectData) {
            $state.go(targetPage, {
                umroh: objectData
            });
        };
    }

    $scope.userCheck = function() {
      if (localStorage.user) {
          $scope.user = JSON.parse(localStorage.user);

      } else {
          $scope.user = [];
      }
  }


  
  $scope.init();
});

appCtrl.controller('orderDetailCtrl', function($ionicModal, $state, $rootScope, $ionicHistory, $stateParams, $scope, $ionicPopup, $ionicActionSheet, $ionicLoading, TimeService, RequestService, ionicDatePicker) {
    //if(typeof analytics !== undefined) { analytics.trackView("Order Detail"); }

    $scope.init = function() {
      $scope.umroh = $stateParams.umroh;
      $scope.profile = [];
      $scope.user = [];
      $scope.login = [];
      $scope.history = [];
      $scope.user = JSON.parse(localStorage.user);
      $scope.var = [];
      $scope.detail = [];
      var data = {
        "id": $scope.umroh.id
    }
    $ionicLoading.show({
        template: 'Loading..'
    });

    RequestService.getOrderDetail(data).then(function(response) {
        $ionicLoading.hide();
        $scope.detail = response;
        var bintangHotel = $scope.detail.umroh.hotel;
        $scope.bintang = [];
        if(bintangHotel != null) {
            var i = 0;
            while (i < bintangHotel )
            {
                $scope.bintang.push(i);
                i++;
            }
        }
        $scope.var.jmlChild = 0;
        $scope.var.jmlDewasa = 0;
        $scope.var.jmlInfant = 0;
        $scope.var.jmlMahram = 0;

            // $scope.var.totalJamaah = $scope.detail.passenger.length;
            // i = 0;
            // while (i < $scope.detail.passenger.length) {
            //     if($scope.detail.passenger[i].selisih_date >=0 && $scope.detail.passenger[i].selisih_date < 3) {
            //         $scope.var.jmlChild++;
            //     } else if ($scope.detail.passenger[i].selisih_date >= 3 && $scope.detail.passenger[i].selisih_date < 17) {
            //         $scope.var.jmlInfant++;
            //     } else if ($scope.detail.passenger[i].selisih_date >= 17) {
            //         $scope.var.jmlDewasa++;
            //     }

            //     if($scope.detail.passenger[i].status == 'sendiri') {
            //         $scope.var.jmlMahram++;
            //     }
            //     i++;
            // }
            //$scope.var.jmlShipping = $scope.detail.passenger.length;
            var spl = $scope.detail.basket.split(';');
            var tmp = spl[0].split(',');
            var hargaDewasa = parseInt(tmp[1].split('.'));
            $scope.var.jmlDewasa = parseInt(tmp[2].split('.'));
            $scope.var.totalBiayaDewasa = parseInt(tmp[3].split('.'));
            
            
            var tmp = spl[1].split(',');
            $scope.var.totalBiayaChild = parseInt(tmp[3].split('.'));
            $scope.var.jmlChild = parseInt(tmp[2].split('.'));

            var tmp = spl[3].split(',');
            $scope.var.totalBiayaInfant = parseInt(tmp[3].split('.'));
            $scope.var.jmlInfant = parseInt(tmp[2].split('.'));

            var tmp = spl[3].split(',');
            $scope.var.totalBiayaMahram = parseInt(tmp[3].split('.'));
            $scope.var.jmlMahram = parseInt(tmp[2].split('.'));

            var tmp = spl[4].split(',');
            $scope.var.totalBiayaShipping = parseInt(tmp[3].split('.'));
            $scope.var.jmlShipping = parseInt(tmp[2].split('.'));

            $scope.var.jmlShipping = $scope.detail.passenger.length;
            // $scope.var.totalBiayaDewasa = hargaDewasa * $scope.var.jmlDewasa;
            // $scope.var.totalBiayaChild = $scope.var.jmlChild * $scope.detail.umroh.child_price;
            // $scope.var.totalBiayaInfant = $scope.var.jmlInfant * $scope.detail.umroh.infant_price;
            // $scope.var.totalBiayaMahram = $scope.var.jmlMahram * $scope.detail.umroh.mahram_price;
            // $scope.var.totalBiayaShipping = $scope.detail.passenger.length * $scope.detail.umroh.handling_price;

            $scope.var.totalBiaya = $scope.var.totalBiayaDewasa + $scope.var.totalBiayaChild + $scope.var.totalBiayaInfant + $scope.var.totalBiayaMahram + $scope.var.totalBiayaShipping;
            $scope.userCheck();
            
        });
}

$scope.userCheck = function() {
    if (localStorage.user) {
        $scope.user = JSON.parse(localStorage.user);
        if($scope.user.img == null || !$scope.user.img) {
            $scope.user.img = 'img/man-avatar.png';
        }
    } else {
        $scope.user = [];
    }
}

$scope.showStar = function(det) {
    var myPopup = $ionicPopup.show({
        template: 
        '<div class="row">'
        +'<div class="col">'
        +'<label>Bintang Pelayanan</label>'
        +'</div>'
        +'</div>'
        +'<div class="row">'
        +'<div class="col text-center txt-medium-orange" ng-click="changeStar(\'star1\')">'
        +'<i id="star1" class="fa fa-star-o fa-lg" aria-hidden="true"></i>'
        +'</div>'
        +'<div class="col text-center txt-medium-orange" ng-click="changeStar(\'star2\')">'
        +'<i id="star2" class="fa fa-star-o fa-lg" aria-hidden="true"></i>'
        +'</div>'
        +'<div class="col text-center txt-medium-orange" ng-click="changeStar(\'star3\')">'
        +'<i id="star3" class="fa fa-star-o fa-lg" aria-hidden="true"></i>'
        +'</div>'
        +'<div class="col text-center txt-medium-orange" ng-click="changeStar(\'star4\')">'
        +'<i id="star4" class="fa fa-star-o fa-lg" aria-hidden="true"></i>'
        +'</div>'
        +'<div class="col text-center txt-medium-orange" ng-click="changeStar(\'star5\')">'
        +'<i id="star5" class="fa fa-star-o fa-lg" aria-hidden="true"></i>'
        +'</div>'
        +'</div>'
        +'<div class="row">'
        +'<div class="col">'
        +'<label>Komentar</label>'
        +'</div>'
        +'</div>'
        +'<div class="row">'
        +'<div class="col">'
        +'<input type="text" ng-model="var.komentar">'
        +'</div>'
        +'</div>'
        ,
        title: 'Masukkan Penilaian Anda',
        subTitle: '',
        scope: $scope,
        buttons: [
        { text: 'Cancel' },
        {
            text: '<b>Simpan</b>',
            type: 'button-positive',
            onTap: function(e) {
                if(!$scope.var.komentar) {
                    e.preventDefault();
                    $scope.showAlert('Warning', 'Kolom Komentar Harus Terisi');
                } else if (!$scope.var.star) {
                    e.preventDefault();
                    $scope.showAlert('Warning', 'Pilih Bintang Pelayanan');
                } else if($scope.var.komentar.length < 15) {
                    e.preventDefault();
                    $scope.showAlert('Warning', 'Kolom komentar minimal 15 karakter');
                }
                else {
                        //Jalankan API
                        //saveRatingReview
                        var data = {
                            "userdevice_id": $scope.user.id,
                            "customer_name": $scope.user.name,
                            "rating": $scope.var.star,
                            "review": $scope.var.komentar,
                            "umrohregister_id": $scope.umroh.id,
                            "umroh_id": $scope.detail.type_id,
                            "travelagent_id": $scope.detail.travelagent_id
                        }

                        RequestService.saveRatingReview(data).then(function(response) {
                            $scope.showAlert(response.status, response.message);
                            setTimeout(function(){
                                window.history.back();
                            });
                        });
                        
                    }
                }
            }
            ]
        });


}

$scope.changeStar = function(st) {
    if(st == 'star1') {
        $('#star1').removeClass('fa-star-o');
        $('#star2').removeClass('fa-star-o');
        $('#star3').removeClass('fa-star-o');
        $('#star4').removeClass('fa-star-o');
        $('#star5').removeClass('fa-star-o');
        $('#star1').addClass('fa-star');
        $('#star2').addClass('fa-star-o');
        $('#star3').addClass('fa-star-o');
        $('#star4').addClass('fa-star-o');
        $('#star5').addClass('fa-star-o');
        $scope.var.star = 1;
    } else if (st == 'star2') {
        $('#star1').removeClass('fa-star-o');
        $('#star2').removeClass('fa-star-o');
        $('#star3').removeClass('fa-star-o');
        $('#star4').removeClass('fa-star-o');
        $('#star5').removeClass('fa-star-o');
        $('#star1').addClass('fa-star');
        $('#star2').addClass('fa-star');
        $('#star3').addClass('fa-star-o');
        $('#star4').addClass('fa-star-o');
        $('#star5').addClass('fa-star-o');
        $scope.var.star = 2;
    } else if (st == 'star3') {
        $('#star1').removeClass('fa-star-o');
        $('#star2').removeClass('fa-star-o');
        $('#star3').removeClass('fa-star-o');
        $('#star4').removeClass('fa-star-o');
        $('#star5').removeClass('fa-star-o');
        $('#star1').addClass('fa-star');
        $('#star2').addClass('fa-star');
        $('#star3').addClass('fa-star');
        $('#star4').addClass('fa-star-o');
        $('#star5').addClass('fa-star-o');
        $scope.var.star = 3;
    } else if (st == 'star4') {
        $('#star1').removeClass('fa-star-o');
        $('#star2').removeClass('fa-star-o');
        $('#star3').removeClass('fa-star-o');
        $('#star4').removeClass('fa-star-o');
        $('#star5').removeClass('fa-star-o');
        $('#star1').addClass('fa-star');
        $('#star2').addClass('fa-star');
        $('#star3').addClass('fa-star');
        $('#star4').addClass('fa-star');
        $('#star5').addClass('fa-star-o');
        $scope.var.star = 4;
    } else {
        $('#star1').removeClass('fa-star-o');
        $('#star2').removeClass('fa-star-o');
        $('#star3').removeClass('fa-star-o');
        $('#star4').removeClass('fa-star-o');
        $('#star5').removeClass('fa-star-o');
        $('#star1').addClass('fa-star');
        $('#star2').addClass('fa-star');
        $('#star3').addClass('fa-star');
        $('#star4').addClass('fa-star');
        $('#star5').addClass('fa-star');
        $scope.var.star = 5;
    }
}


$scope.updateStatus = function() {
    var status;
    if ($scope.detail.status == 1) {
        status = 6;         
    } else if ($scope.detail.status == 2) {
        status = 7; 
    } else if ($scope.detail.status == 3) {
        status = 5;
    } else {
        status = 5;
    }   

    var data = {
        'order_id' : $scope.umroh.order_id,
        'status' : status
    }

    RequestService.updateStatusOrder(data).then(function(response) {
        $scope.showAlert(response.status, response.message);
    });
}

$rootScope.$ionicGoBack = function() {
        // Default -1, -2 goes back 2 views
        $ionicHistory.goBack(-2); 
    };
    


    $scope.showAlert = function(judul, pesan) {
        var alertPopup = $ionicPopup.alert({
          title: judul,
          template: pesan
      });
    };

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            umroh: objectData
        });
    };

    $scope.sendPesan = function() {
        var data = {
          'travelagent_id' : $scope.detail.travelagent_id,
          'travelagent': $scope.detail.travelagent
      };
      $scope.navigateTo('app.message-send', data);
  }
  $scope.init();
});
