appCtrl.controller('promoCtrl', function($scope, $state, $ionicSlideBoxDelegate, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Promo"); }

    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {
		$scope.promos = [];

        $scope.page = 1;
        $scope.limit = 3;

        $scope.noMorePromo = false;

		// $scope.getData();
	}

	// $scope.getData = function() {
	// 	RequestService.getall('promo').then(function(promos) {
 //            for (var ii = 0; ii < promos.length; ii++) {
 //                $scope.promos.push(promos[ii]);
 //            }

 //            $('#promo-loading-progress').hide();
 //            $('#promo-content').show();

 //            $ionicSlideBoxDelegate.update();
 //        });
	// }

	// $scope.doRefresh = function() {
 //        $scope.promos = [];
        
 //        RequestService.getall('promo').then(function(promos) {
 //            for (var ii = 0; ii < promos.length; ii++) {
 //                $scope.promos.push(promos[ii]);
 //            }
 //        })

 //        .finally(function() {
 //            $scope.$broadcast('scroll.refreshComplete');
 //        });
 //    }

    $scope.navigateTo = function (targetPage, objectData) {
        $state.go(targetPage, {
            promo: objectData
        });
    };

    $scope.loadMorePromos = function() {
        RequestService.getlimit('promo', $scope.page, $scope.limit).then(function(promos) {
            for (var ii = 0; ii < promos.length; ii++) {
                $scope.promos.push(promos[ii]);
            }

            $scope.$broadcast('scroll.infiniteScrollComplete');

            $scope.page += 1;

            // if ($scope.promos.length == $scope.total) {
            //     $scope.noMorePromo = true;
            // }

            if (promos.length != $scope.limit) {
                $scope.noMorePromo = true;
            }
        });
    };

	$scope.init();
});

appCtrl.controller('promoDetailCtrl', function($scope, $stateParams, RequestService) {
    //if(typeof analytics !== undefined) { analytics.trackView("Promo Detail"); }

    $scope.baseUrl = RequestService.urlImageLink;
	$scope.init = function() {
		$scope.promo = $stateParams.promo;
	}

	$scope.init();
});