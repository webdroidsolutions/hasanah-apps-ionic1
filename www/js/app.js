// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js


angular.module('starter', ['ionic', 'starter.controllers', 'ion-floating-menu', 'tabSlideBox', 'ui.router', 'ngCordova', 'angular-sha1'])

.filter('cut', function () {
	return function (value, wordwise, max, tail) {
		if (!value) return '';

		max = parseInt(max, 10);

		if (!max) return value;

		if (value.length <= max) return value;

		value = value.substr(0, max);

		if (wordwise) {
			var lastspace = value.lastIndexOf(' ');

			if (lastspace !== -1) {
				//Also remove . and , so its gives a cleaner result.
				if (value.charAt(lastspace-1) === '.' || value.charAt(lastspace-1) === ',') {
					lastspace = lastspace - 1;
				}

				value = value.substr(0, lastspace);
			}
		}

		return value + (tail || ' ...');
	};
})

.run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);

		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
	});
})

.run(function($rootScope, $ionicPlatform, $ionicHistory, $location, $state){
	$ionicPlatform.registerBackButtonAction(function(e){
		if ($rootScope.backButtonPressedOnceToExit) {
			ionic.Platform.exitApp();
		} else if ($ionicHistory.backView()) {
			$ionicHistory.goBack();
		} else if ($location.path().indexOf('/app/home') !== -1) {
			$rootScope.backButtonPressedOnceToExit = true;
			window.plugins.toast.showShortCenter(
				"Tekan Tombol Back Sekali Lagi Untuk Keluar",function(a){},function(b){}
				);

			setTimeout(function(){
				$rootScope.backButtonPressedOnceToExit = false;
			},2000);
		} else {
			$ionicHistory.nextViewOptions({
				disableBack: true
			});

			$state.go('app.home');
		}

		e.preventDefault();

		return false;
	},101);
})

.run(function($rootScope, $ionicPlatform, $cordovaGoogleAnalytics, $state) {
	$ionicPlatform.ready(function() {
		if(typeof analytics !== "undefined") {
			$cordovaGoogleAnalytics.startTrackerWithId('UA-124568189-1');
			$cordovaGoogleAnalytics.trackView($state.current.name);
		} else {
			console.log("Google Analytics Unavailable");
		}

		$rootScope.$on('$stateChangeSuccess', function () {
			if(typeof analytics !== undefined) {
				$cordovaGoogleAnalytics.startTrackerWithId('UA-124568189-1');
				$cordovaGoogleAnalytics.trackView($state.current.name);
			} else {
				console.log("Google Analytics Unavailable");
			}
		});
	});
})

.service('UserService', function() {
	// For the purpose of this example I will store user data on ionic local storage but you should save it on a database

	var setUser = function(user_data) {
		window.localStorage.starter_google_user = JSON.stringify(user_data);
	};

	var getUser = function(){
		return JSON.parse(window.localStorage.starter_google_user || '{}');
	};

	return {
		getUser: getUser,
		setUser: setUser
	};
})

.filter('rupiah', function() {
	return function(jumlah) {
		var titik = ",";
		var nilai = new String(jumlah);
		var pecah = [];
		while(nilai.length > 3)
		{
			var asd = nilai.substr(nilai.length-3);
			pecah.unshift(asd);
			nilai = nilai.substr(0, nilai.length-3);
		}

		if(nilai.length > 0) { pecah.unshift(nilai); }
		nilai = pecah.join(titik);
		return nilai+" IDR";
	};
})

.factory('TimeService', function() {
	return {
		days: ["Ahad", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
		months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
	};
})

.factory('RequestService', function($http, $q, $timeout) {
	return {
    // url : 'http://demo.proapptive.in:8180/srv/',
  	//url : 'http://localhost:8180/srv/',
	// urlImageLink : 'http://localhost:8180/',
	url : 'http://192.168.1.3:8180/srv/',
	urlImageLink : 'http://192.168.1.3:8180/',
	//url : 'http://hasanahpersonal.co.id:8180/srv/',
	urlecoll : 'http://hasanahpersonal.co.id:8180/srv/',
	urlImage : 'http://hasanahpersonal.co.id:8180/adm/upload/uploadimg/',
	urlImageRemove : 'http://hasanahpersonal.co.id:8180/adm/upload/remove/',
	urlImg : 'http://hasanahpersonal.co.id:8180/images/',
	getall: function(url) {
		var q = $q.defer();

		if (url != 'city') {
			var data = { status: 1 };
		}

		$http({
			method: 'POST',
			url: this.url+url+'/getall',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response.data);
		});

		return q.promise;
	},
	getUrlUpload : function(){
		return this.url;
	},
	getUrlImg : function () {
		return this.urlImg;
	}, 
	seturl: function(url){
		this.url = 'http://'+url+':8180/srv/';
	},
	getallnopaging: function(url) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+url+'/getall'+url,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response.data);
		});

		return q.promise;
	},
	getlimit: function(url, page, limit) {
		var q = $q.defer();

		if (url != 'city') {
			var data = { status: 1, page: page, limit: limit };
		}

		$http({
			method: 'POST',
			url: this.url+url+'/getall',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response.data);
		});

		return q.promise;
	},
	getby: function(data, url, by) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+url+'/getby'+by,
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response.data);
		});

		return q.promise;
	},
	getactive: function(url) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+url+'/getactive'+url,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response.data);
		});

		return q.promise;
	},
	getvar: function(url, by) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+url+'/get'+by,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response.data);
		});

		return q.promise;
	},
	getcount: function(data, url) {
		var q = $q.defer();

		$http({
			method: 'POST',
			data: data,
			url: this.url+url+'/getcount',
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response.data[0]['total']);
		});

		return q.promise;
	},
	getsearchcount: function(data, url) {
		var q = $q.defer();

		$http({
			method: 'POST',
			data: data,
			url: this.url+url+'/getcount',
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response.data);
		});

		return q.promise;
	},
	save: function(data, url) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+url+'/save',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	delete: function(data, url) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+url+'/delete',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	search: function(data, url) {
		var q = $q.defer();

		$http({
			method: 'POST',
			data: data,
			url: this.url+url+'/search',
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).then(function(response) {
			q.resolve(response.data);
		});

		return q.promise;
	},
	login: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+'userdevice/login',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	forgotpassword: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+'userdevice/forgotPassword',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	changepassword: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+'userdevice/changePassword',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	editprofil: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+'userdevice/edit',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getHistory: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+'userdevice/forgotPassword',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	saveorder: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+'umrohregister/save/',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	saveorderecoll: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url+'umrohregister/save/',
			data: data,
			contentType: 'application/json',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	topaymentgateway: function(uri, data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: uri,
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getHistoryTransaksi: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url + 'umrohregister/getByCustomer',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	sendMessage: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url + 'message/save',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getMessage: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url + 'message/getByCustomer',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	deleteImage: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url + 'upload/remove/',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getOrderDetail: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url + 'umrohregister/getbyid',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	updateStatusOrder: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url + 'umrohregister/edit',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	//http://dev.farizdotid.com/api/daerahindonesia/provinsi
	getProvinsi: function() {
		var q = $q.defer();

		$http({
			method: 'GET',
			url: 'http://dev.farizdotid.com/api/daerahindonesia/provinsi',
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getKabupaten: function(idProv) {
		var q = $q.defer();

		$http({
			method: 'GET',
			url: 'http://dev.farizdotid.com/api/daerahindonesia/provinsi/'+idProv+'/kabupaten',
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getKecamatan: function(idKab) {
		var q = $q.defer();

		$http({
			method: 'GET',
			url: 'http://dev.farizdotid.com/api/daerahindonesia/provinsi/kabupaten/'+idKab+'/kecamatan',
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getKelurahan: function(idKec) {
		var q = $q.defer();

		$http({
			method: 'GET',
			url: 'http://dev.farizdotid.com/api/daerahindonesia/provinsi/kabupaten/kecamatan/'+idKec+'/desa',
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	saveRatingReview: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url + 'ratingreview/save',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getRatingReview: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url + 'ratingreview/getbyumroh',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getCountRatingReview: function(data) {
		var q = $q.defer();

		$http({
			method: 'POST',
			url: this.url + 'ratingreview/getcount',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getDepartureCity: function(data) {
		var q = $q.defer();
		///umroh/getdeparturecity
		$http({
			method: 'POST',
			url: this.url + 'umroh/getdeparturecity',
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getMessageUmroh: function(data) {
		var q = $q.defer();
		///umroh/getdeparturecity
		$http({
			method: 'POST',
			url: this.url + 'message/getbyumroh',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	},
	getCountByUmroh: function(data) {
		var q = $q.defer();
		///umroh/getdeparturecity
		$http({
			method: 'POST',
			url: this.url + 'message/getcountbyumroh',
			data: data,
			contentType: 'application/html',
			headers: {
				'Content-Type':'application/json'
			}
		}).success(function(response) {
			q.resolve(response);
		}).error(function(response) {
			q.resolve(response);
		});

		return q.promise;
	}
};
})

.filter('trustAsResourceUrl', ['$sce', function($sce) {
	return function(val) {
		return $sce.trustAsResourceUrl(val);
	};
}])

.config(function (ionicDatePickerProvider) {
	var datePickerObj = {
		inputDate: new Date(),
		titleLabel: 'Select a Date',
		setLabel: 'Set',
		todayLabel: 'Today',
		closeLabel: 'Close',
		mondayFirst: false,
		weeksList: ["S", "M", "T", "W", "T", "F", "S"],
		monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
		templateType: 'popup',
		from: new Date(2012, 8, 1),
		to: new Date(2018, 8, 1),
		showTodayButton: true,
		dateFormat: 'dd MMMM yyyy',
		closeOnSelect: false,
		disableWeekdays: []
	};

	ionicDatePickerProvider.configDatePicker(datePickerObj);
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
	$ionicConfigProvider.navBar.alignTitle('center');

	$stateProvider

	// .state('login', {
	// 	url: '/login',
	// 	templateUrl: 'templates/login/html/login.html',
	// 	controller: 'loginCtrl'
	// })

	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/menu/html/menu.html',
		controller: 'menuCtrl'
	})

	.state('app.home', {
		url: '/home',
		views: {
			'menuContent': {
				templateUrl: 'templates/home/html/home.html',
				controller: 'homeCtrl'
			}
		}
	})



	.state('app.contact', {
		url: '/contact',
		views: {
			'menuContent': {
				templateUrl: 'templates/contact/html/contact.html',
			}
		}
	})

	.state('app.product', {
		url: '/product',
		views: {
			'menuContent': {
				templateUrl: 'templates/product/html/product.html',
				controller: 'productCtrl'
			}
		}
	})

	.state('app.product-list', {
		url: '/product-list',
		params: {
			category: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/product/html/product-list.html',
				controller: 'productListCtrl'
			}
		}
	})

	.state('app.product-detail', {
		url: '/product-detail',
		params: {
			product: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/product/html/product-detail.html',
				controller: 'productDetailCtrl'
			}
		}
	})

	.state('app.branch', {
		url: '/branch',
		views: {
			'menuContent': {
				templateUrl: 'templates/branch/html/branch.html',
				controller: 'branchCtrl'
			}
		}
	})

	.state('app.branch-search', {
		url: '/branch-search',
		views: {
			'menuContent': {
				templateUrl: 'templates/branch/html/branch-search.html',
				controller: 'branchSearchCtrl'
			}
		}
	})

	.state('app.branch-detail', {
		url: '/branch-detail',
		params: {
			branch: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/branch/html/branch-detail.html',
				controller: 'branchDetailCtrl'
			}
		}
	})

	.state('app.merchant', {
		url: '/merchant',
		views: {
			'menuContent': {
				templateUrl: 'templates/merchant/html/merchant.html',
				controller: 'merchantCtrl'
			}
		}
	})

	.state('app.merchant-detail', {
		url: '/merchant-detail',
		params: {
			merchant: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/merchant/html/merchant-detail.html',
				controller: 'merchantDetailCtrl'
			}
		}
	})

	.state('app.umroh', {
		cache: false,
		url: '/umroh',
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh.html',
				controller: 'umrohCtrl'
			}
		}
	})

	.state('app.umroh-results', {
		cache: false,
		url: '/umroh-results',
		params: {
			search: null,
			total: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh-results.html',
				controller: 'umrohResultsCtrl'
			}
		}
	})

	.state('app.umroh-detail', {
		cache: false,
		url: '/umroh-detail',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh-detail.html',
				controller: 'umrohDetailCtrl'
			}
		}
	})


	

	.state('app.umroh-register', {
		cache: false,
		url: '/umroh-register',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh-register.html',
				controller: 'umrohRegisterCtrl'
			}
		}
	})

	.state('app.umroh-filing', {
		cache: false,
		url: '/umroh-filing',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh-filing.html',
				controller: 'umrohFilingCtrl'
			}
		}
	})

	.state('app.umroh-review', {
		cache: false,
		url: '/umroh-review',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh-review.html',
				controller: 'umrohReviewCtrl'
			}
		}
	})

	.state('app.umroh-input', {
		cache: false,
		url: '/umroh-input',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh-input.html',
				controller: 'umrohInputCtrl'
			}
		}
	})


	.state('app.umroh-pay', {
		cache: false,
		url: '/umroh-pay',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh-pay.html',
				controller: 'umrohPayCtrl'
			}
		}
	})


	.state('app.umroh-paymentgateway', {
		cache: false,
		url: '/umroh-paymentgateway',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/paymentgateway/html/umroh-paymentgateway.html',
				controller: 'umrohPaymentGatewayCtrl'
			}
		}
	})

	.state('app.umroh-va', {
		cache: false,
		url: '/umroh-va',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh-va.html',
				controller: 'umrohVACtrl'
			}
		}
	})

	.state('app.modal-peserta', {
		url: '/modal-peserta',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/modal-peserta.html',
				controller: 'modalPesertaCtrl'
			}
		}
	})

	// .state('app.umroh-search', {
	// 	url: '/umroh-search',
	// 	params: {
	// 		umroh: null
	// 	},
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/umroh/html/umroh-search.html',
	// 			controller: 'umrohSearchCtrl'
	// 		}
	// 	}
	// })

	.state('app.property', {
		url: '/property',
		views: {
			'menuContent': {
				templateUrl: 'templates/property/html/property.html',
				controller: 'propertyCtrl'
			}
		}
	})

	.state('app.property-results', {
		url: '/property-results',
		params: {
			search: null,
			total: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/property/html/property-results.html',
				controller: 'propertyResultsCtrl'
			}
		}
	})

	.state('app.property-detail', {
		url: '/property-detail',
		params: {
			property: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/property/html/property-detail.html',
				controller: 'propertyDetailCtrl'
			}
		}
	})

	.state('app.property-register', {
		url: '/property-register',
		params: {
			property: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/property/html/property-register.html',
				controller: 'propertyRegisterCtrl'
			}
		}
	})

	.state('app.property-filing', {
		url: '/property-filing',
		params: {
			property: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/property/html/property-filing.html',
				controller: 'propertyFilingCtrl'
			}
		}
	})

	.state('app.calculator', {
		url: '/calculator',
		views: {
			'menuContent': {
				templateUrl: 'templates/calculator/html/calculator.html',
				controller: 'calculatorCtrl'
			}
		}
	})

	.state('app.filing', {
		url: '/filing',
		views: {
			'menuContent': {
				templateUrl: 'templates/filing/html/filing.html',
				controller: 'filingCtrl'
			}
		}
	})

	.state('app.article', {
		url: '/article',
		views: {
			'menuContent': {
				templateUrl: 'templates/article/html/article.html',
				controller: 'articleCtrl'
			}
		}
	})

	.state('app.subcategory', {
		url: '/subcategory',
		params: {
			category: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/article/html/subcategory.html',
				controller: 'subCategoryCtrl'
			}
		}
	})

	.state('app.article-list', {
		url: '/article-list',
		params: {
			subcategory: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/article/html/article-list.html',
				controller: 'articleListCtrl'
			}
		}
	})

	.state('app.article-detail', {
		url: '/article-detail',
		params: {
			article: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/article/html/article-detail.html',
				controller: 'articleDetailCtrl'
			}
		}
	})

	.state('app.term', {
		url: '/term',
		views: {
			'menuContent': {
				templateUrl: 'templates/term/html/term.html',
				controller: 'termCtrl'
			}
		}
	})

	.state('app.term-detail', {
		url: '/term-detail',
		params: {
			term: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/term/html/term-detail.html',
				controller: 'termDetailCtrl'
			}
		}
	})

	.state('app.promo', {
		url: '/promo',
		views: {
			'menuContent': {
				templateUrl: 'templates/promo/html/promo.html',
				controller: 'promoCtrl'
			}
		}
	})

	.state('app.promo-detail', {
		url: '/promo-detail',
		params: {
			promo: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/promo/html/promo-detail.html',
				controller: 'promoDetailCtrl'
			}
		}
	})

	.state('app.survey', {
		cache: false,
		url: '/survey',
		views: {
			'menuContent': {
				templateUrl: 'templates/survey/html/survey.html',
				controller: 'surveyCtrl'
			}
		}
	})

	.state('app.survey-detail', {
		url: '/survey-detail',
		params: {
			survey: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/survey/html/survey-detail.html',
				controller: 'surveyDetailCtrl'
			}
		}
	})

	.state('app.suggestion', {
		url: '/suggestion',
		views: {
			'menuContent': {
				templateUrl: 'templates/suggestion/html/suggestion.html',
				controller: 'suggestionCtrl'
			}
		}
	})

	.state('app.profile', {
		cache: false,
		url: '/profile',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/profile/html/profile.html',
				controller: 'profileCtrl'
			}
		}
	})

	.state('app.profiledetail', {
		url: '/profiledetail',
		views: {
			'menuContent': {
				templateUrl: 'templates/profile/html/profiledetail.html',
				controller: 'profiledetailCtrl'
			}
		}
	})

	.state('app.inbox', {
		cache: false,
		url: '/inbox',
		views: {
			'menuContent': {
				templateUrl: 'templates/profile/html/inbox.html',
				controller: 'inboxCtrl'
			}
		}
	})

	.state('app.message-send', {
		url: '/message-send',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/profile/html/message-send.html',
				controller: 'messageSendCtrl'
			}
		}
	})

	.state('app.history', {
		cache: false,
		url: '/history',
		views: {
			'menuContent': {
				templateUrl: 'templates/profile/html/history.html',
				controller: 'historyCtrl'
			}
		}
	})


	.state('app.order-detail', {
		cache: false,
		url: '/order-detail',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/profile/html/order-detail.html',
				controller: 'orderDetailCtrl'
			}
		}
	})

	.state('app.umroh-rating', {
		url: '/umroh-rating',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh-rating.html',
				controller: 'umrohRatingCtrl'
			}
		}
	})

	.state('app.umroh-pesan', {
		url: '/umroh-pesan',
		params: {
			umroh: null
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/umroh/html/umroh-pesan.html',
				controller: 'umrohPesanCtrl'
			}
		}
	})
	





	// .state('app.profile', {
	// 	url: '/profile',
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/profile/html/profile.html',
	// 			controller: 'profileCtrl'
	// 		}
	// 	}
	// })

	// .state('app.network', {
	// 	url: '/network',
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/network/html/network.html',
	// 			controller: 'networkCtrl'
	// 		}
	// 	}
	// })

	// .state('app.network-detail', {
	// 	url: '/network-detail',
	// 	params: {
	// 		network: null
	// 	},
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/network/html/network-detail.html',
	// 			controller: 'networkDetailCtrl'
	// 		}
	// 	}
	// })

	// .state('app.calendar', {
	// 	url: '/calendar',
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/calendar/html/calendar.html',
	// 			controller: 'calendarCtrl'
	// 		}
	// 	}
	// })

	// .state('app.schedule', {
	// 	url: '/schedule',
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/schedule/html/schedule.html',
	// 			controller: 'scheduleCtrl'
	// 		}
	// 	}
	// })

	// .state('app.stream-video', {
	// 	url: '/stream-video',
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/stream/html/stream-video.html',
	// 			controller: 'streamVideoCtrl'
	// 		}
	// 	}
	// })

	// .state('app.stream-video-detail', {
	// 	url: '/stream-video-detail',
	// 	params: {
	// 		video: null
	// 	},
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/stream/html/stream-video-detail.html',
	// 			controller: 'streamVideoDetailCtrl'
	// 		}
	// 	}
	// })

	// .state('app.gallery', {
	// 	url: '/gallery',
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/gallery/html/gallery.html',
	// 			controller: 'galleryCtrl'
	// 		}
	// 	}
	// })

	// .state('app.gallery-image', {
	// 	url: '/gallery-image',
	// 	params: {
	// 		album: null
	// 	},
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/gallery/html/gallery-image.html',
	// 			controller: 'galleryImageCtrl'
	// 		}
	// 	}
	// })

	// .state('app.donation', {
	// 	url: '/donation',
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/donation/html/donation.html',
	// 			controller: 'donationCtrl'
	// 		}
	// 	}
	// })

	// $urlRouterProvider.otherwise('login');
	$urlRouterProvider.otherwise('app/home');
});
